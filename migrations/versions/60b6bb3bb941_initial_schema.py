"""Initial schema

Revision ID: 60b6bb3bb941
Revises:
Create Date: 2023-03-03 10:15:11.098008

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '60b6bb3bb941'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table('airmetitems',
    sa.Column('airmet', postgresql.JSON(astext_type=sa.Text()), nullable=True),
    sa.Column('uuid', sa.UUID(), nullable=False),
    sa.Column('lastUpdateDate', sa.DateTime(), server_default=sa.text('now()'), nullable=True),
    sa.Column('creationDate', sa.DateTime(), server_default=sa.text('now()'), nullable=True),
    sa.Column('author', sa.String(), nullable=True),
    sa.PrimaryKeyConstraint('uuid')
    )
    op.create_index(op.f('ix_airmetitems_creationDate'), 'airmetitems', ['creationDate'], unique=False)
    op.create_table('sigmetitems',
    sa.Column('sigmet', postgresql.JSON(astext_type=sa.Text()), nullable=True),
    sa.Column('uuid', sa.UUID(), nullable=False),
    sa.Column('lastUpdateDate', sa.DateTime(), server_default=sa.text('now()'), nullable=True),
    sa.Column('creationDate', sa.DateTime(), server_default=sa.text('now()'), nullable=True),
    sa.Column('author', sa.String(), nullable=True),
    sa.PrimaryKeyConstraint('uuid')
    )
    op.create_index(op.f('ix_sigmetitems_creationDate'), 'sigmetitems', ['creationDate'], unique=False)


def downgrade() -> None:
    op.drop_index(op.f('ix_sigmetitems_creationDate'), table_name='sigmetitems')
    op.drop_table('sigmetitems')
    op.drop_index(op.f('ix_airmetitems_creationDate'), table_name='airmetitems')
    op.drop_table('airmetitems')
