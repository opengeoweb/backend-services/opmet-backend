"""CLI utils for Alembic"""

import datetime
import os
from contextlib import contextmanager
from datetime import date, timedelta
from typing import Generator

import rich
import typer
from alembic.command import stamp
from alembic.config import Config
from sqlalchemy import inspect
from sqlalchemy.orm import Session

import opmet_backend.database as db
from opmet_backend.models.sigmet import Sigmet

app = typer.Typer()


@app.command()
def load(update_timestamps: bool = typer.Option(False)) -> None:
    "Load test data into the database"
    with get_db() as session:
        with os.scandir("./alembic_testdata/sigmet") as iterator:
            for entry in iterator:
                if entry.name.endswith(".json") and entry.is_file():
                    load_sigmet(session, entry, update_timestamps)


def load_sigmet(session: Session, entry: os.DirEntry[str],
                update_timestamps: bool) -> None:
    "Parse and load a SIGMET"
    try:
        sigmet = Sigmet.parse_file(entry)  # type: ignore
        if update_timestamps:
            sigmet = update_dates(sigmet)
        db_sigmet = db.create_sigmet(sigmet, session)
        rich.print(f"Added SIGMET with UUID {db_sigmet.uuid} to the database")
    # pylint: disable=broad-except
    except Exception as ex:
        rich.print(ex)


def update_dates(message: Sigmet) -> Sigmet:
    "Update timestamp dates relative to current date"
    now = date.today()
    if isinstance(message.issueDate, datetime.date):
        message.issueDate = message.issueDate.replace(year=now.year,
                                                      month=now.month,
                                                      day=now.day)
    if isinstance(message.validDateStart, datetime.date):
        message.validDateStart = message.validDateStart.replace(year=now.year,
                                                                month=now.month,
                                                                day=now.day)
        message.validDateEnd = message.validDateStart + timedelta(days=1)

    if isinstance(message.observationOrForecastTime, datetime.date):
        message.observationOrForecastTime = message.observationOrForecastTime.replace(
            year=now.year, month=now.month, day=now.day)
    return message


@contextmanager
def get_db() -> Generator:
    "Gets us the DB session for all the calls that read or write to DB"
    session = db.SessionLocal()
    try:
        yield session
    finally:
        session.close()


@app.command()
def enable_alembic() -> None:
    """Inspect the state of the database and make sure Alembic is enabled"""
    alembic_cfg = Config('alembic.ini')
    inspector = inspect(db.engine)
    if not inspector.has_table('alembic_version'):
        if inspector.has_table('airmetitems'):
            # pre-alembic schema seems to exist, so stamp current database with initial version
            stamp(alembic_cfg, revision='60b6bb3bb941')
        if inspector.has_table('sigmetitems'):
            # pre-alembic schema seems to exist, so stamp current database with initial version
            stamp(alembic_cfg, revision='60b6bb3bb941')


if __name__ == '__main__':
    app()
