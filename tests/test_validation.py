"""Tests for validation"""
from opmet_backend.models.shared import MetBase, ProductStatus


def test_metbase_field_validator():
    # Empty string converted to None by validator
    assert MetBase(status="").status is None

    # None remains None
    assert MetBase(status=None).status is None

    # Regular case unaffected
    assert MetBase(status=ProductStatus.DRAFT).status == ProductStatus.DRAFT
