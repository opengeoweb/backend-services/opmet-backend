# pylint: disable=redefined-outer-name, unspecified-encoding
"""Test for sigmet sequence numbering and related changes"""
import json
import logging

import httpx
import pytest
from freezegun import freeze_time

from opmet_backend.config import backend_config

logger = logging.getLogger(__name__)

@pytest.fixture
def short_test_publish():
    return create_test_sequence_fixture("short_test_publish")

@pytest.fixture
def test_publish():
    return create_test_sequence_fixture("test_publish")

@pytest.fixture
def short_va_test_publish():
    return create_test_sequence_fixture("short_va_test_publish")

@pytest.fixture
def va_test_publish():
    return create_test_sequence_fixture("va_test_publish")

def create_test_sequence_fixture(name: str):
    with open(f"./tests/testdata/from_frontend/sigmetexamples/test_sigmet_sequence_bodies/{name}.json") as file:
        data = json.load(file)
    return data


@pytest.fixture
def sample():
    """Fixture for testfing regular phenomena"""
    with open("./tests/testdata/from_frontend/sigmetexamples/sigmet-readGeoWe"
              "bSigmetAndCreateSigmet.json") as file:
        data = json.load(file)
    return data


@pytest.fixture
def va():
    """Fixture for testing VA_CLD"""
    with open("./tests/testdata/from_frontend/sigmetexamples/twolevels.json"
             ) as file:
        data = json.load(file)
    return data


@pytest.fixture
def thunder():
    """Fixture for sigmet with thunder as phenomenon"""
    with open("./tests/testdata/thunder.json") as file:
        data = json.load(file)
    return data


@freeze_time('2024-06-09T00:00:01Z', auto_tick_seconds=15, ignore=['asyncio'])
async def test_numbering_and_day_change(client: httpx.AsyncClient, thunder: dict):
    """
    * Publish a SIGMET for thunder valid 00.01UTC-02.01UTC
        * The number for it will be T01
    * Publish a SIGMET for thunder valid 01:55-03:55UTC
        * The number for it will be T02
    * Send a cancel for that SIGMET T01 at 01.56UTC
        * The number for it will be T03"""
    tohone = await client.post("/sigmet", json=thunder)
    assert tohone.json()['sigmet']['sequence'] == "T01"
    thunder2 = thunder.copy()
    thunder2['validDateStart'] = "2024-06-09T01:55:00Z"
    thunder2['validDateEnd'] = "2024-06-09T03:55:00Z"
    tohtwo = await client.post("/sigmet", json=thunder2)
    assert tohtwo.json()['sigmet']['sequence'] == "T02"
    await client.post("/sigmet",
                json={
                    "sigmet": tohone.json()['sigmet'],
                    "changeStatusTo": "CANCELLED"
                })
    r = await client.get("/sigmetlist")
    sigmetlist = [
        x for x in r.json()
        if x['sigmet']['status'] != 'DRAFT'
    ]
    assert sigmetlist[0]['sigmet']['sequence'] == "T03"


@freeze_time('2024-06-09T00:00:01Z', auto_tick_seconds=15, ignore=['asyncio'])
async def test_with_old_functionality(client: httpx.AsyncClient, thunder: dict):
    """
    * Publish a SIGMET for thunder valid 00.01UTC-02.01UTC
        * The number for it will be T01
    * Publish a SIGMET for thunder valid 01:55-03:55UTC
        * The number for it will be T02
    * Send a cancel for that SIGMET T01 at 01.56UTC
        * The number for it will be T03"""
    backend_config.sigmet_sequence_prefix = ""
    tohone = await client.post("/sigmet", json=thunder)
    assert tohone.json()['sigmet']['sequence'] == "1"
    thunder2 = thunder.copy()
    thunder2['validDateStart'] = "2025-06-09T01:55:00Z"
    thunder2['validDateEnd'] = "2025-06-09T03:55:00Z"
    tohtwo = await client.post("/sigmet", json=thunder2)
    assert tohtwo.json()['sigmet']['sequence'] == "2"
    await client.post("/sigmet",
                json={
                    "sigmet": tohone.json()['sigmet'],
                    "changeStatusTo": "CANCELLED"
                })
    r = await client.get("/sigmetlist")
    sigmetlist = [
        x for x in r.json()
        if x['sigmet']['status'] != 'DRAFT'
    ]
    assert sigmetlist[0]['sigmet']['sequence'] == "3"
    backend_config.sigmet_sequence_prefix = None


async def test_with_turbulence(client: httpx.AsyncClient, sample: dict):
    """
    * Publish a SIGMET for Turbulence today at 04.02 UTC
        * The number for it will be U01
    * Publish a SIGMET for Turbulence on the next day at 00.05 UTC
        *The number for it will be U01"""
    with freeze_time('2024-06-09T04:02:00Z', ignore=['asyncio']) as frozen_datetime:
        sample.pop("uuid")
        sample['type'] = "NORMAL"
        sample['phenomenon'] = "SEV_TURB"
        sample['validDateStart'] = "2024-06-09T04:02:00Z"
        sample['validDateEnd'] = "2024-06-09T06:02:00Z"
        sample["observationOrForecastTime"] = "2024-06-09T04:02:00Z"
        turbulence = await client.post('sigmet',
                                 json={
                                     "sigmet": sample,
                                     "changeStatusTo": "PUBLISHED"
                                 })
        assert turbulence.json()['sigmet']['sequence'] == "U01"
        # System date is now 2024-06-10 because every tick is worth 20 hours 3 minutes
        frozen_datetime.move_to('2024-06-10T00:05:00Z')
        sample['observationOrForecastTime'] = "2024-06-10T00:05:00Z"
        sample['validDateStart'] = "2024-06-10T00:05:00Z"
        sample['validDateEnd'] = "2024-06-10T02:05:00Z"
        turbulence2 = await client.post('sigmet',
                                  json={
                                      "sigmet": sample,
                                      "changeStatusTo": "PUBLISHED"
                                  })
        assert turbulence2.json()['sigmet']['sequence'] == "U01"


async def test_0000_vs_0001(client: httpx.AsyncClient, sample: dict):
    """Test that verifies we are resetting sequence numbering at 00:01, not
    at 00:00"""
    with freeze_time('2024-06-10T00:00:30Z', ignore=['asyncio']) as frozen_datetime:
        sample.pop("uuid")
        sample['type'] = "NORMAL"
        sample['validDateStart'] = "2024-06-10T00:00:30Z"
        sample['validDateEnd'] = "2024-06-10T03:00:00Z"
        sample["observationOrForecastTime"] = "2024-06-10T00:00:30Z"
        early = await client.post("sigmet",
                            json={
                                "sigmet": sample,
                                "changeStatusTo": "PUBLISHED"
                            })
        assert early.json()['sigmet']['sequence'] == "I01"
        frozen_datetime.move_to('2024-06-10T00:01:00Z')
        sample['validDateStart'] = "2024-06-10T00:01:00Z"
        sample['validDateEnd'] = "2024-06-10T03:01:00Z"
        sample['observationOrForecastTime'] = "2024-06-10T00:01:00Z"
        late = await client.post("sigmet",
                           json={
                               "sigmet": sample,
                               "changeStatusTo": "PUBLISHED"
                           })
        assert late.json()['sigmet']['sequence'] == "I01"


@pytest.mark.parametrize("phenomenon, letter",
                         [("EMBD_TS", "T"), ("HVY_DS", "D"), ("HVY_SS", "S"),
                          ("RDOACT_CLD", "R"), ("SEV_ICE", "I"),
                          ("SEV_ICE_FRZ_RN", "F"), ("SEV_MTW", "M"),
                          ("SEV_TURB", "U")])
async def test_types_to_letters(client: httpx.AsyncClient, sample: dict, phenomenon: str,
                          letter: str):
    """Test to iterate over phenomena other than VA and TC, to verify they get
    approppriate sequence prefixes"""
    sample['phenomenon'] = phenomenon
    sample['type'] = "NORMAL"
    sample.pop("uuid")
    post = await client.post("/sigmet",
                       json={
                           "sigmet": sample,
                           "changeStatusTo": "PUBLISHED"
                       })
    post.raise_for_status()
    assert post.json()["sigmet"]["sequence"][0] == letter


async def test_types_to_letters_va(client: httpx.AsyncClient, va: dict):
    """Test to verify VA_CLD gets approppriate sequence prefix"""
    va.pop("uuid")
    post = await client.post("/sigmet",
                       json={
                           "sigmet": va,
                           "changeStatusTo": "PUBLISHED"
                       })
    post.raise_for_status()
    assert post.json()["sigmet"]["sequence"][0] == 'A'


#def test_types_to_letters_tc(tc: dict):
#    """Test to verify TC gets approppriate sequence prefix"""
#    post = client.post("/sigmet",
#                       json={
#                           "sigmet": tc,
#                           "changeStatusTo": "PUBLISHED"
#                       })
#    post.raise_for_status()
#    assert post.json()["sigmet"]["sequence"][0] == 'C'


async def test_types_to_letters_ws_test(client: httpx.AsyncClient, sample: dict):
    """Test to verify WS test sigmet gets approppriate sequence prefix"""
    sample['type'] = "TEST"
    sample.pop("uuid")
    post = await client.post("/sigmet",
                       json={
                           "sigmet": sample,
                           "changeStatusTo": "PUBLISHED"
                       })
    post.raise_for_status()
    assert post.json()["sigmet"]["sequence"][0] == "Z"


async def test_types_to_letters_wv_test(client: httpx.AsyncClient, va: dict):
    """Test to verify WV test sigmet gets approppriate sequence prefix"""
    va['type'] = "TEST"
    va.pop("uuid")
    post = await client.post("/sigmet",
                       json={
                           "sigmet": va,
                           "changeStatusTo": "PUBLISHED"
                       })
    post.raise_for_status()
    assert post.json()["sigmet"]["sequence"][0] == "Y"


async def test_different_firs_have_separate_sequences(client: httpx.AsyncClient,
                                                sample: dict):
    """"
        Verify that if you publish two sigmets with different FIRs, they have the same sequence number,
        being the very first ones for those FIRs. Use a pre-prepared SIGMET with contrived FIR names.
    """
    fir_1_sigmet = prepare_sigmet_for_FIR(sample, "FIR 1")
    fir_1_sigmet_sequence = await get_json_response_sequence(client, fir_1_sigmet)
    fir_2_sigmet = prepare_sigmet_for_FIR(sample, "FIR 2")
    fir_2_sigmet_sequence = await get_json_response_sequence(client, fir_2_sigmet)
    assert fir_1_sigmet_sequence == fir_2_sigmet_sequence
    fir_1_sigmet_sequence_2 = await get_json_response_sequence(client, fir_1_sigmet)
    assert fir_1_sigmet_sequence_2 != fir_1_sigmet_sequence


async def get_json_response_sequence(client: httpx.AsyncClient, sigmet: dict):
    post = await client.post("/sigmet",
                       json={
                           "sigmet": sigmet,
                           "changeStatusTo": "PUBLISHED"
                       })
    post.raise_for_status()
    return post.json()["sigmet"]["sequence"]


def prepare_sigmet_for_FIR(sigmet: dict, fir: str):
    ret = dict(sigmet)
    ret.pop("uuid")
    ret['type'] = 'TEST'
    ret['firName'] = fir
    return ret

async def test_test_sigmet_sequences(client: httpx.AsyncClient,
        short_test_publish: dict,
        test_publish: dict,
        short_va_test_publish: dict,
        va_test_publish: dict):

    """
        We try to mimick a client interaction when the user is publishing
        and canceling various kinds of test SIGMETs. Thus the many asserts here.

        Also, we need to manipulate time a bit, just to make the SIGMETs have
        different issue dates in the database (seems that we only have a
        second's resolution there)
    """

    # SHORT_TEST: publish and cancel
    with freeze_time("2024-04-26 04:00:00Z", ignore=['asyncio']):
        short_test_publish_response = await send_body(client, short_test_publish)
        assert response_message_sigmet_sequence_is("Z01", short_test_publish_response)

    with freeze_time("2024-04-26 04:00:02Z", ignore=['asyncio']):
        short_test_cancel_message = prepare_cancel_message_from_publish_response(short_test_publish_response)
        short_test_cancel_response = await send_body(client, short_test_cancel_message)
        assert response_message_sigmet_status_is("CANCELLED", short_test_cancel_response)
        assert await last_sigmet_sequence_is("Z02", client)

    # TEST: publish and cancel
    with freeze_time("2024-04-26 04:00:04Z", ignore=['asyncio']):
        test_publish_response = await send_body(client, test_publish)
        assert response_message_sigmet_sequence_is("Z03", test_publish_response)
    with freeze_time("2024-04-26 04:00:06Z", ignore=['asyncio']):
        test_cancel_message = prepare_cancel_message_from_publish_response(test_publish_response)
        test_cancel_response = await send_body(client, test_cancel_message)
        assert response_message_sigmet_status_is("CANCELLED", test_cancel_response)
        assert await last_sigmet_sequence_is("Z04", client)

    # SHORT_VA_TEST: publish and cancel
    with freeze_time("2024-04-26 04:00:08Z", ignore=['asyncio']):
        short_va_test_publish_response = await send_body(client, short_va_test_publish)
        assert response_message_sigmet_sequence_is("Y01", short_va_test_publish_response)

    with freeze_time("2024-04-26 04:00:10Z", ignore=['asyncio']):
        short_va_test_cancel_message = prepare_cancel_message_from_publish_response(short_va_test_publish_response)
        short_va_test_cancel_response = await send_body(client, short_va_test_cancel_message)
        assert response_message_sigmet_status_is("CANCELLED", short_va_test_cancel_response)
        assert await last_sigmet_sequence_is("Y02", client)

    # VA_TEST: publish and cancel
    with freeze_time("2024-04-26 04:00:12Z", ignore=['asyncio']):
        va_test_publish_response = await send_body(client, va_test_publish)
        assert response_message_sigmet_sequence_is("Y03", va_test_publish_response)

    with freeze_time("2024-04-26 04:00:14Z", ignore=['asyncio']):
        va_test_cancel_message = prepare_cancel_message_from_publish_response(va_test_publish_response)
        va_test_cancel_response = await send_body(client, va_test_cancel_message)
        assert response_message_sigmet_status_is("CANCELLED", va_test_cancel_response)
        assert await last_sigmet_sequence_is("Y04", client)


async def send_body(client: httpx.AsyncClient, body: dict):
    post = await client.post("/sigmet",
                       json=body)
    post.raise_for_status()
    return post.json()

def prepare_cancel_message_from_publish_response(publish_response: dict):
    cancel_message = publish_response.copy()
    cancel_message.pop('uuid')
    cancel_message.pop('lastUpdateDate')
    cancel_message.pop('creationDate')
    cancel_message["changeStatusTo"] = "CANCELLED"
    cancel_message.pop('canbe')
    return cancel_message

async def get_last_sigmet(client: httpx.AsyncClient):
    r = await get_sigmet_list(client)
    return r[0]

async def get_sigmet_list(client: httpx.AsyncClient):
    get = await client.get("/sigmetlist")
    return get.json()

async def last_sigmet_sequence_is(sequence: str, client: httpx.AsyncClient):
    r = await get_last_sigmet(client)
    return r['sigmet']['sequence'] == sequence

def response_message_sigmet_sequence_is(sequence: str, message: dict):
    return message['sigmet']['sequence'] == sequence

def response_message_sigmet_status_is(status: str, message: dict):
    return message['sigmet']['status'] == status

#def test_types_to_letters_wc_test(tc: dict):
#    """Test to verify WC test sigmet gets approppriate sequence prefix"""
#    tc['type'] = "TEST"
#    tc.pop("uuid")
#    post = client.post("/sigmet",
#                       json={
#                           "sigmet": tc,
#                           "changeStatusTo": "PUBLISHED"
#                       })
#    post.raise_for_status()
#    assert post.json()["sigmet"]["sequence"][0] == "X"
