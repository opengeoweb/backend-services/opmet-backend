import os

# Determine host dynamically (to support both pipeline runners and local testing)
docker_host = os.getenv('DOCKER_HOST') if os.getenv(
    'GITLAB_CI') else 'localhost'

database_port = "5433"
messageconverter_port = "8080"
publisher_port = "8090"
database_name = "opmetbackenddb_tests"

# Set up everything needed in the environment for the services to run
os.environ[
    'SQLALCHEMY_DATABASE_URL'] = f'postgresql://geoweb:geoweb@{docker_host}:{database_port}/{database_name}'
os.environ[
    'MESSAGECONVERTER_URL'] = f'http://{docker_host}:{messageconverter_port}'
os.environ['PUBLISHER_URL'] = f'http://{docker_host}:{publisher_port}/publish'
os.environ['DATABASE_PORT'] = database_port
