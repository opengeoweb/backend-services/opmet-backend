""" Tests for publication """

from datetime import datetime

from opmet_backend.config import backend_config
from opmet_backend.publish import format_tac_filename


def test_format_tac_filename_without_valid_time():
    backend_config.tac_filename_template = "{location_indicator}_{header_designator}_{issue_time}_1{suffix}"
    backend_config.tac_filename_suffix = ".test"

    valid_start = datetime(2024, 2, 10, 12, 40, 10)
    header_designator = "WSNL31"
    location_indicator = "ABCD"
    issue_time = datetime(2024, 2, 10, 12, 30, 15)
    expected_filename = "ABCD_WSNL31_20240210123015_1.test"
    assert format_tac_filename(header_designator, location_indicator,
                               valid_start, issue_time) == expected_filename


def test_format_tac_filename():
    backend_config.tac_filename_template = "{header_designator}{location_indicator}_{valid_start}_{issue_time}{suffix}"
    backend_config.tac_filename_suffix = ".txt"

    valid_start = datetime(2024, 2, 10, 12, 40, 10)
    header_designator = "WSNL31"
    location_indicator = "ABCD"
    issue_time = datetime(2024, 2, 10, 12, 30, 15)
    expected_filename = "WSNL31ABCD_2024-02-10T1240_20240210123015.txt"
    assert format_tac_filename(header_designator, location_indicator,
                               valid_start, issue_time) == expected_filename
