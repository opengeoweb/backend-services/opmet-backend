# pylint: disable=redefined-outer-name, unspecified-encoding
"""Tests for AIRMET functionalities"""

import json
import logging
import os
import time
import uuid
from typing import Any

import httpx
import pytest
from freezegun import freeze_time

from opmet_backend.config import airmet_config, backend_config, env_config
from opmet_backend.models.airmet import Airmet, AirmetListItem
from opmet_backend.models.shared import DRAFT_SEQUENCE, ProductCanBe
from opmet_backend.utils import build_url

from .testdata.mock_airmets import (
    airmet3,
    airmet7,
    postdraft,
    postdrafttopublish,
)

logging.basicConfig()
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

if airmet_config is None:
    pytest.skip("airmetConfig.json not deployed", allow_module_level=True)

IWXXM_URL = build_url(env_config.messageconverter_url,
                      backend_config.airmet_iwxxm_endpoint)
TAC_URL = build_url(env_config.messageconverter_url,
                    backend_config.airmet_tac_endpoint)

docker_host = os.getenv('DOCKER_HOST') if os.getenv(
    'GITLAB_CI') else 'localhost'


@pytest.fixture
def non_mocked_hosts() -> list:
    """Hosts that do not require mocking"""
    return ["testserver", "publisher"]


@pytest.fixture
def published_airmet_dict() -> dict:
    """Fixture for published airmet json"""
    with open("./tests/testdata/from_frontend/airmetexamples/published.json"
             ) as file:
        data: dict = json.load(file)
    return data


@pytest.fixture
def post_draft() -> Any:
    """Mock POST to create draft"""
    return json.loads(postdraft)


@pytest.fixture
def post_draft_to_publish() -> Any:
    """Mock POST to publish a draft"""
    return json.loads(postdrafttopublish)


@pytest.fixture
def published() -> Any:
    """Mock published airmet"""
    return json.loads(airmet3)['airmet']


@pytest.fixture
def draft() -> Any:
    """Mock draft airmet"""
    return json.loads(airmet7)['airmet']


@pytest.fixture
def airmetconfig():
    """Fixture used in testing airmetconfig"""
    with open("./configuration_files/airmetConfig.json") as file:
        data = json.load(file)
    return data

def test_phenomenon_list_not_empty(airmetconfig: dict):
    """Test that the phenomenon list is not empty"""
    assert "fir_areas" in airmetconfig
    for fir_area in airmetconfig["fir_areas"].values():
        assert "phenomenon" in fir_area
        assert len(fir_area["phenomenon"]) > 0


@freeze_time('2020-11-18T11:31:00Z', ignore=['asyncio'])
async def test_airmetlist(client: httpx.AsyncClient, draft: dict,
                    post_draft_to_publish: dict):
    """A bit overly large test that populates database with published airmet, a
    draft and an airmet that cancels the first one. Also tests whether filtering
    and sorting works as specified."""
    del draft['uuid']
    post = await client.post("/airmet", json=post_draft_to_publish)
    print("### POST ")
    print(post.status_code)
    post.raise_for_status()
    await client.post("/airmet", json={"airmet": draft, "changeStatusTo": "DRAFT"})
    cancel = await client.post("/airmet",
                         json={
                             "airmet": post.json()["airmet"],
                             "changeStatusTo": "CANCELLED"
                         })

    cancel.raise_for_status()

    r = await client.get("/airmetlist")
    airmetlist = r.json()

    assert airmetlist[0]['canbe'] and airmetlist[0]['canbe'] is not None

    assert len(airmetlist) == 3
    draft['sequence'] = DRAFT_SEQUENCE
    draft['uuid'] = airmetlist[0]['airmet']['uuid']
    assert airmetlist[0]['airmet'] == draft
    assert airmetlist[1]['airmet']['status'] == 'CANCELLED'
    assert airmetlist[2]['airmet']['cancelsAirmetSequenceId'] == '1'
    assert airmetlist[2]['airmet']['sequence'] == '2'
    # A bit lazy, but we are testing here that CancelAirmet does not report as cancellable
    assert airmetlist[2]['canbe'] == []


async def test_get_missing(client: httpx.AsyncClient):
    """Test that verifies trying to fetch airmet that does not exists returns
    404"""
    get = await client.get(f'/airmet/{uuid.uuid1()}')
    assert get.status_code == 404


async def test_get_silly_input(client: httpx.AsyncClient):
    """Test that trying to get airmet by uuid that is not a valid uuid returns
    400"""
    get = await client.get('/airmet/notanuuid')
    assert get.status_code == 400


async def test_roundtrip(client: httpx.AsyncClient, post_draft: Any):
    """Test that verifies we can create and load a draft successfully"""

    post = await client.post("/airmet", json=post_draft)
    post.raise_for_status()
    get = await client.get(f"/airmet/{post.json()['uuid']}")
    assert get.json() == post.json()


async def test_edit(client: httpx.AsyncClient, post_draft: Any):
    """Test that verifies we can create, load and edit an airmet"""
    r = await client.post("/airmet", json=post_draft)
    post = r.json()

    airmet = post["airmet"]
    newstring = airmet["firName"] + "A"
    airmet["firName"] = newstring
    await client.post("/airmet", json={"airmet": airmet, "changeStatusTo": "DRAFT"})

    get = await client.get(f"/airmet/{post['uuid']}")
    assert get.json()["airmet"]["firName"] == newstring


def test_published_can_be_cancelled_or_renewed(published):
    """Test that the AirmetListItem validator fills the otherwise empty canBe
    field"""
    outputitem = AirmetListItem(uuid=uuid.uuid1(),
                                lastUpdateDate=time.time(),
                                creationDate=time.time(),
                                airmet=published)
    assert outputitem.canbe == [ProductCanBe.CANCELLED, ProductCanBe.RENEWED]


def test_draft_can_be_drafted(draft):
    """Test that the AirmetListItem validator fills the otherwise empty canBe
    field correctly"""
    outputitem = AirmetListItem(uuid=uuid.uuid1(),
                                lastUpdateDate=time.time(),
                                creationDate=time.time(),
                                airmet=draft)
    assert outputitem.canbe == [
        ProductCanBe.DRAFTED, ProductCanBe.PUBLISHED, ProductCanBe.DISCARDED
    ]


async def test_draft_becomes_published(client: httpx.AsyncClient, post_draft: Any):
    """Test that a draft can be published."""
    r = await client.post("/airmet", json=post_draft)
    post = r.json()
    r2 = await client.get(f"/airmet/{post['uuid']}")
    get = r2.json()
    r3 = await client.post("/airmet",
                       json={
                           "airmet": get["airmet"],
                           "changeStatusTo": "PUBLISHED"
                       })
    post = r3.json()
    assert post["airmet"]["status"] == 'PUBLISHED'


async def test_save_partial_airmet_draft(client: httpx.AsyncClient, post_draft: dict):
    """Test that a missing GeoJson field does not cause problems"""
    """This is a good example to check that all serializers and attributes
    implemented in the original classes have been kept when creating a partial
    type """
    del post_draft["airmet"]["startGeometry"]
    post = await client.post("/airmet", json=post_draft)
    assert post.json()["airmet"]["status"] == 'DRAFT'


async def test_cancellation(client: httpx.AsyncClient, post_draft_to_publish: Any):
    """Test that verifies an AIRMET can be first published and then cancelled"""
    r = await client.post("/airmet", json=post_draft_to_publish)
    post = r.json()
    r2 = await client.get(f"/airmet/{post['uuid']}")
    get = r2.json()
    r3 = await client.post("/airmet",
                       json={
                           "airmet": get["airmet"],
                           "changeStatusTo": "CANCELLED"
                       })
    post = r3.json()
    assert post["airmet"]["status"] == 'CANCELLED'


def test_cancel_airmet_type_with_original_type_config(
        published_airmet_dict: dict):
    """Test that cancelled AIRMET keeps the original type if configured to do so"""
    backend_config.cnl_airmet_original_type = True
    airmet = Airmet(**published_airmet_dict)
    cancel_airmet = airmet.make_cancelled()
    assert cancel_airmet.type == 'TEST'


def test_cancel_airmet_type_without_original_type_config(
        published_airmet_dict: dict):
    """Test that cancelled AIRMET is of NORMAL type if it's not configured to
       keep the original type"""
    backend_config.cnl_airmet_original_type = False
    airmet = Airmet(**published_airmet_dict)
    cancel_airmet = airmet.make_cancelled()
    assert cancel_airmet.type == 'NORMAL'


async def test_airmet2tac(client: httpx.AsyncClient, published_airmet_dict: dict):
    """Test that airmet2tac endpoint returns expected string"""
    r = await client.post("/airmet2tac", json=published_airmet_dict)
    tacmessage = r.text
    assert tacmessage == ('EHAA AIRMET 1 VALID 050908/051008 EHDB-\n'
                          'EHAA AMSTERDAM FIR TEST\n'
                          'BKN CLD SFC/ABV0500FT OBS N5220 E00556 STNR NC=')


async def test_airmet2tac_draft_gets_next_sequence(client: httpx.AsyncClient,
                                             published_airmet_dict: dict):
    """Test that airmet2tac endpoint returns expected
       string after publishing an airmet before drafting a new one"""
    published_airmet_dict.pop("uuid")
    r = await client.post("/airmet",
                       json={
                           "airmet": published_airmet_dict,
                           "changeStatusTo": "DRAFT"
                       })
    post = r.json()
    r2 = await client.get(f"/airmet/{post['uuid']}")
    get = r2.json()
    await client.post("/airmet",
                       json={
                           "airmet": get["airmet"],
                           "changeStatusTo": "PUBLISHED"
                       })
    published_airmet_dict['sequence'] = DRAFT_SEQUENCE
    r3 = await client.post("/airmet2tac", json=published_airmet_dict)
    tacmessage = r3.text
    assert tacmessage == ('EHAA AIRMET 2 VALID 050908/051008 EHDB-\n'
                          'EHAA AMSTERDAM FIR TEST\n'
                          'BKN CLD SFC/ABV0500FT OBS N5220 E00556 STNR NC=')


@freeze_time('2020-11-18T11:31:00Z', ignore=['asyncio'])
async def test_airmet_no_exception_on_iwxxm_publish_fail(client: httpx.AsyncClient,
                                                   httpx_mock,
                                                   post_draft_to_publish: Any,
                                                   output_dir):
    """Test that only TAC is published when IWXXM endpoint fails with a log"""
    backend_config.exception_on_iwxxm_publish_fail = False
    backend_config.exception_on_tac_publish_fail = True
    backend_config.airmet_header = "WANL31"

    # Mock the failure and success responses for the messageconverter endpoints
    httpx_mock.add_response(method='POST',
                            url=TAC_URL,
                            text='TAC CONTENT',
                            status_code=200)

    httpx_mock.add_response(method='POST',
                            url=IWXXM_URL,
                            json={'message': 'Server error'},
                            status_code=500)

    # This published URL needs to be requested
    httpx_mock.add_response(
        method='POST',
        url=
        f"http://{docker_host}:8090/publish/WANL31EHDB_2020-11-18T1131_20201118113100.tac",
        text="Publishing OK",
        status_code=200)

    # Output dir is empty previous to this
    r = await client.post("/airmet", json=post_draft_to_publish)
    post = r.json()
    # Check that it is not an error message but a real airmet
    assert 'airmet' in post


@freeze_time('2020-11-18T11:31:00Z', ignore=['asyncio'])
async def test_airmet_exception_on_iwxxm_publish_fail(client: httpx.AsyncClient, httpx_mock,
                                                post_draft_to_publish: Any,
                                                output_dir):
    """Test that nothing is published when IWXXM fails with an Exception"""
    backend_config.exception_on_iwxxm_publish_fail = True
    backend_config.exception_on_tac_publish_fail = False

    # Mock the failure and success responses for the messageconverter endpoints
    httpx_mock.add_response(method='POST',
                            url=IWXXM_URL,
                            json={'message': 'Server error'},
                            status_code=500)

    r = await client.post("/airmet", json=post_draft_to_publish)
    post = r.json()
    assert post['message'] == 'Unable to publish AIRMET IWXXM.'


@freeze_time('2020-11-18T11:31:00Z', ignore=['asyncio'])
async def test_airmet_no_exception_on_tac_publish_fail(client: httpx.AsyncClient, httpx_mock,
                                                 post_draft_to_publish: Any,
                                                 output_dir):
    """Test that nothing is published even when all booleans are False"""
    backend_config.exception_on_iwxxm_publish_fail = False
    backend_config.exception_on_tac_publish_fail = False

    httpx_mock.add_response(method='POST',
                            url=IWXXM_URL,
                            text='',
                            status_code=500)

    httpx_mock.add_response(method='POST',
                            url=TAC_URL,
                            json={'message': 'Server error'},
                            status_code=500)

    r = await client.post("/airmet", json=post_draft_to_publish)
    post = r.json()
    files = os.listdir(output_dir)
    assert (len(files) == 0)
    # Check that it is not an error message but a real airmet
    assert 'airmet' in post


@freeze_time('2020-11-18T11:31:00Z', ignore=['asyncio'])
async def test_airmet_exception_on_tac_publish_fail(client: httpx.AsyncClient, httpx_mock,
                                              post_draft_to_publish: Any,
                                              output_dir):
    """Test that IWXXM is published when the boolean for TAC is True"""
    backend_config.exception_on_iwxxm_publish_fail = False
    backend_config.exception_on_tac_publish_fail = True
    backend_config.iwxxm_airmet_header = "LWNL31"
    backend_config.airmet_header = "WANL31"
    httpx_mock.add_response(method='POST',
                            url=IWXXM_URL,
                            text='TAC CONTENT',
                            status_code=200)

    httpx_mock.add_response(method='POST',
                            url=TAC_URL,
                            json={'message': 'Server error'},
                            status_code=500)

    # This publisher URL needs to be requested
    httpx_mock.add_response(
        method='POST',
        url=
        f"http://{docker_host}:8090/publish/A_LWNL31EHDB181131_C_EHDB_20201118113100.xml",
        text="Publishing OK",
        status_code=200)

    r = await client.post("/airmet", json=post_draft_to_publish)
    post = r.json()
    assert post["message"] == "Unable to publish AIRMET TAC."


async def test_airmetconfig(client: httpx.AsyncClient, airmetconfig: dict):
    """Test that airmetConfig is recieved and contains matching data"""
    r = await client.get("/airmet-config")
    assert sorted(r.json()) == sorted(airmetconfig)
