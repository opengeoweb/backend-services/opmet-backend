# pylint: disable=redefined-outer-name, unspecified-encoding
"""Tests for SIGMET functionalities and some shared functionality"""

import json
import logging
import os
import time
import uuid
from datetime import datetime
from typing import Any

import dateutil
import httpx
import pytest
from freezegun import freeze_time
from httpx import Response
from pydantic import ValidationError

import opmet_backend.publish as publish
from opmet_backend.config import backend_config, env_config, sigmet_config
from opmet_backend.models.shared import (
    DRAFT_SEQUENCE,
    PublishData,
    PublishMetaData,
)
from opmet_backend.models.sigmet import (
    ProductCanBe,
    Sigmet,
    SigmetBase,
    SigmetListItem,
    make_cancelled,
)
from opmet_backend.utils import HTTPXClientWrapper, build_url

from .testdata.mock_sigmets import postdrafttopublish

logging.basicConfig()
logging.getLogger("sqlalchemy.engine").setLevel(logging.INFO)

if sigmet_config is None:
    pytest.skip("sigmetConfig.json not deployed", allow_module_level=True)

IWXXM_URL = build_url(
    env_config.messageconverter_url, backend_config.sigmet_iwxxm_endpoint
)
TAC_URL = build_url(env_config.messageconverter_url, backend_config.sigmet_tac_endpoint)

docker_host = os.getenv("DOCKER_HOST") if os.getenv("GITLAB_CI") else "localhost"


@pytest.fixture
def post_draft_to_publish() -> Any:
    """Mock POST to publish a draft"""
    return json.loads(postdrafttopublish)


@pytest.fixture
def non_mocked_hosts() -> list:
    """Hosts that do not require mocking"""
    return ["testserver", "publisher"]


@pytest.fixture
def cancelled():
    """Fixture for cancelled sigmet json"""
    with open("./tests/testdata/from_frontend/sigmetexamples/cancelled.json") as file:
        data = json.load(file)
    return data


@pytest.fixture
def published() -> Any:
    """Fixture for published sigmet json"""
    with open("./tests/testdata/from_frontend/sigmetexamples/published.json") as file:
        data = json.load(file)
    return data


@pytest.fixture
def draft():
    """Fixture for drafted sigmet json"""
    with open("./tests/testdata/from_frontend/sigmetexamples/draft.json") as file:
        data = json.load(file)
    return data


@pytest.fixture
def draft_sequence():
    """Fixture for drafted sigmet json with -1 as a sequence number"""
    with open(
        "./tests/testdata/from_frontend/sigmetexamples/draft-sequence.json"
    ) as file:
        data = json.load(file)
    return data


@pytest.fixture
def draft_rdoact_cld():
    """Fixture for a radioactive cloud draft sigmet"""
    with open(
        "./tests/testdata/from_frontend/sigmetexamples/draft-rdoact-cld.json"
    ) as file:
        data = json.load(file)
    return data

@pytest.fixture
def draft_rdoact_cld_without_change():
    """Fixture for a radioactive cloud draft sigmet, without change"""
    with open(
        "./tests/testdata/from_frontend/sigmetexamples/draft-rdoact-cld-without-change.json"
    ) as file:
        data = json.load(file)
    return data


@pytest.fixture
def short_test() -> Any:
    """Fixture for a short test sigmet"""
    with open("./tests/testdata/from_frontend/sigmetexamples/short-test.json") as file:
        data = json.load(file)
    return data


@pytest.fixture
def short_va_test() -> Any:
    """Fixture for a short VA test sigmet"""
    with open(
        "./tests/testdata/from_frontend/sigmetexamples/short-va-test.json"
    ) as file:
        data = json.load(file)
    return data


@pytest.fixture
def missing_fields() -> Any:
    """Fixture with missing fields"""
    with open(
        "./tests/testdata/from_frontend/sigmetexamples/missing-fields.json"
    ) as file:
        data = json.load(file)
    return data


@pytest.fixture
def sample():
    """Fixture used in testing sigmet2tac"""
    with open(
        "./tests/testdata/from_frontend/sigmetexamples/sigmet-readGeoWe"
        "bSigmetAndCreateSigmet.json"
    ) as file:
        data = json.load(file)
    return data


@pytest.fixture
def sigmetconfig():
    """Fixture used in testing sigmetconfig"""
    with open("./configuration_files/sigmetConfig.json") as file:
        data = json.load(file)
    return data

def test_phenomenon_list_not_empty(sigmetconfig: dict):
    """Test that the phenomenon list is not empty"""
    assert "fir_areas" in sigmetconfig
    for fir_area in sigmetconfig["fir_areas"].values():
        assert "phenomenon" in fir_area
        assert len(fir_area["phenomenon"]) > 0

@pytest.fixture
def volcanic_ash_sigmet() -> dict:
    """Fixture for a volcanic ash cloud sigmet"""
    with open("./tests/testdata/from_frontend/sigmetexamples/volcanic-ash-cloud-test.json") as file:
        data = json.load(file)
    return data

async def draft_sigmet(client: httpx.AsyncClient, sigmet: dict) -> Response:
    """Create a draft sigmet"""
    return await client.post("/sigmet", json={"sigmet": sigmet, "changeStatusTo": "DRAFT"})


async def publish_sigmet(client: httpx.AsyncClient, sigmet: dict) -> Response:
    """Publish the given sigmet"""
    return await client.post(
        "/sigmet", json={"sigmet": sigmet, "changeStatusTo": "PUBLISHED"}
    )


async def cancel_sigmet(client: httpx.AsyncClient, sigmet: dict) -> Response:
    """Cancel the given sigmet"""
    return await client.post(
        "/sigmet", json={"sigmet": sigmet, "changeStatusTo": "CANCELLED"}
    )


@freeze_time("2020-11-09T13:13:26Z", ignore=['asyncio'])
async def test_sigmetlist(client: httpx.AsyncClient, published: dict, draft: dict):
    """A bit overly large test that populates database with published sigmet, a
    draft and a sigmet that cancels the first one. Also tests whether filtering
    and sorting works as specified."""
    draft.pop("uuid")
    published.pop("uuid")
    published["type"] = "NORMAL"
    post = await publish_sigmet(client, published)
    await draft_sigmet(client, draft)
    await cancel_sigmet(client, post.json()["sigmet"])

    r = await client.get("/sigmetlist")
    sigmetlist = r.json()

    assert sigmetlist[0]["canbe"] and sigmetlist[0]["canbe"] is not None

    assert len(sigmetlist) == 3
    draft["sequence"] = DRAFT_SEQUENCE
    draft["level"]["unit"] = draft["level"]["unit"].upper()
    draft["uuid"] = sigmetlist[0]["sigmet"]["uuid"]
    assert sigmetlist[0]["sigmet"] == draft
    assert sigmetlist[1]["sigmet"]["status"] == "CANCELLED"
    assert sigmetlist[1]["sigmet"]["sequence"] == "I01"
    assert sigmetlist[2]["sigmet"]["cancelsSigmetSequenceId"] == "I01"
    assert sigmetlist[2]["sigmet"]["sequence"] == "I02"
    # A bit lazy, but we are testing here that CancelSigmet does not report as cancellable
    assert sigmetlist[2]["canbe"] == []


async def test_read_main(client: httpx.AsyncClient):
    """Simple test for simple endpoint"""
    response = await client.get("/")
    assert response.status_code == 200
    assert response.text == "GeoWeb OPMET backend"


async def test_roundtrip(client: httpx.AsyncClient, draft: dict):
    """Test that verifies we can create and load a draft successfully"""
    draft.pop("uuid")
    r = await draft_sigmet(client, draft)
    post = r.json()
    get = await client.get(f"/sigmet/{post['uuid']}")
    assert get.json() == post


async def test_edit(client: httpx.AsyncClient, published: dict):
    """Test that verifies we can create, load and edit a sigmet"""
    published.pop("uuid")
    r = await draft_sigmet(client, published)
    post = r.json()

    sigmet = post["sigmet"]
    newstring = sigmet["firName"] + "A"
    sigmet["firName"] = newstring
    await draft_sigmet(client, sigmet)

    get = await client.get(f"/sigmet/{post['uuid']}")
    assert get.json()["sigmet"]["firName"] == newstring


def test_published_can_be_cancelled_or_renewed(published):
    """Test that the SigmetListItem validator fills the otherwise empty canBe
    field"""
    outputitem = SigmetListItem(
        uuid=uuid.uuid1(),
        lastUpdateDate=time.time(),
        creationDate=time.time(),
        sigmet=published,
    )
    assert outputitem.canbe == [ProductCanBe.CANCELLED, ProductCanBe.RENEWED]


def test_draft_can_be_drafted(draft):
    """Test that the SigmetListItem validator fills the otherwise empty canBe
    field correctly"""
    draft.pop("uuid")
    outputitem = SigmetListItem(
        uuid=uuid.uuid1(),
        lastUpdateDate=time.time(),
        creationDate=time.time(),
        sigmet=draft,
    )
    assert outputitem.canbe == [
        ProductCanBe.DRAFTED,
        ProductCanBe.PUBLISHED,
        ProductCanBe.DISCARDED,
    ]


async def test_draft_becomes_published(client: httpx.AsyncClient, draft: dict):
    """Test that a draft can be published."""
    draft.pop("uuid")
    r = await draft_sigmet(client, draft)
    post = r.json()
    r2 = await client.get(f"/sigmet/{post['uuid']}")
    get = r2.json()
    r3 = await publish_sigmet(client, get["sigmet"])
    post = r3.json()
    assert post["sigmet"]["status"] == "PUBLISHED"


async def test_sigmet2tac(client: httpx.AsyncClient, sample):
    """Test that sigmet2tac endpoint returns expected string"""
    r = await client.post("/sigmet2tac", json=sample)
    tacmessage = r.text
    assert tacmessage == (
        "EHAA SIGMET 2 VALID 091315/091459 EHDB-\nEHAA AMSTE"
        "RDAM FIR TEST\nSEV ICE OBS AT 1315Z WI N5321 E00025 "
        "- N5036 E00928 - N5001 E00247 - N5321 E00025 FL150 "
        "NC FCST AT 1459Z ENTIRE FIR="
    )


async def test_sigmet2tac_draft_sequence(client: httpx.AsyncClient, draft_sequence: dict):
    """Test that sigmet2tac endpoint returns expected
    string when the sequence number is -1"""
    r = await client.post("/sigmet2tac", json=draft_sequence)
    tacmessage = r.text
    assert tacmessage == (
        "EHAA SIGMET S01 VALID 081326/081526 EFKL-\n"
        "EHAA AMSTERDAM FIR HVY SS OBS N5222 E00532 "
        "FL500 STNR NC="
    )


async def test_sigmet2tac_draft_gets_next_sequence(client: httpx.AsyncClient, draft: dict):
    """Test that sigmet2tac endpoint returns expected
    string after publishing a sigmet before drafting a new one"""
    draft.pop("uuid")
    r = await draft_sigmet(client, draft)
    post = r.json()
    r2 = await client.get(f"/sigmet/{post['uuid']}")
    get = r2.json()
    await publish_sigmet(client, get["sigmet"])
    draft["sequence"] = DRAFT_SEQUENCE
    r3 = await client.post("/sigmet2tac", json=draft)
    tacmessage = r3.text
    assert tacmessage == (
        "EHAA SIGMET T02 VALID 111500/111800 EHDB-\n"
        "EHAA AMSTERDAM FIR OBSC TS OBS AT 1500Z N OF N5436 "
        "1000FT WKN FCST AT 1800Z S OF N5201="
    )


async def test_sigmet2tac_rdoact_cld_point(client: httpx.AsyncClient, draft_rdoact_cld: dict):
    """Test that sigmet2tac endpoint returns expected
    string for a radioactive cloud sigmet with point geometry
    when backend_config.radioactive_cloud_point_to_circle is False"""
    backend_config.radioactive_cloud_point_to_circle = False
    r = await client.post("/sigmet2tac", json=draft_rdoact_cld)
    tacmessage = r.text
    assert tacmessage == (
        "EHAA SIGMET R01 VALID 271153/271353 EFKL-\n"
        "EHAA AMSTERDAM FIR RDOACT CLD OBS N5234 E00541 "
        "SFC/FL650 STNR WKN="
    )


async def test_sigmet2tac_rdoact_cld_circle(client: httpx.AsyncClient, draft_rdoact_cld: dict):
    """Test that sigmet2tac endpoint returns expected
    string for a radioactive cloud sigmet with a circle
    when backend_config.radioactive_cloud_point_to_circle is True"""
    backend_config.radioactive_cloud_point_to_circle = True
    backend_config.radioactive_cloud_circle_radius_km = 15

    r = await client.post("/sigmet2tac", json=draft_rdoact_cld)
    tacmessage = r.text
    assert tacmessage == (
        "EHAA SIGMET R01 VALID 271153/271353 EFKL-\n"
        "EHAA AMSTERDAM FIR RDOACT CLD OBS WI 15KM OF N5234 E00541 "
        "SFC/FL650 STNR WKN="
    )

async def test_sigmet2tac_rdoact_cld_without_change(client: httpx.AsyncClient, draft_rdoact_cld_without_change: dict):
    """Test that sigmet2tac endpoint returns expected
    string for a radioactive cloud sigmet that has no change
    in payload"""
    sigmet_config.omit_change_va_and_rdoact_cloud = True

    r = await client.post("/sigmet2tac", json=draft_rdoact_cld_without_change)
    tacmessage = r.text
    assert tacmessage == (
        "EHAA SIGMET R01 VALID 271153/271353 EFKL-\n"
        "EHAA AMSTERDAM FIR RDOACT CLD OBS WI 15KM OF N5234 E00541 "
        "SFC/FL650 STNR="
    )

async def test_sigmet2tac_short_test(client: httpx.AsyncClient, short_test: dict):
    """Test that sigmet2tac endpoint returns expected
    string for a short test sigmet"""
    r = await client.post("/sigmet2tac", json=short_test)
    tacmessage = r.text
    assert tacmessage == (
        "EHAA SIGMET Z01 VALID 081326/081526 EHDB-\n" "EHAA AMSTERDAM FIR TEST="
    )


async def test_sigmet2tac_short_va_test(client: httpx.AsyncClient, short_va_test: dict):
    """Test that sigmet2tac endpoint returns expected
    string for a short test sigmet"""
    r = await client.post("/sigmet2tac", json=short_va_test)
    tacmessage = r.text
    assert tacmessage == (
        "EHAA SIGMET Y01 VALID 081326/081526 EHDB-\n" "EHAA AMSTERDAM FIR TEST="
    )


async def test_sigmet2tac_with_missing_fields(client: httpx.AsyncClient, missing_fields: dict):
    """Test that sigmet2tac endpoint returns a validation
    error when mandatory fields are missing"""
    response = await client.post("/sigmet2tac", json=missing_fields)
    assert response.status_code == 400
    assert "missing" == response.json()["message"][0]["type"]

async def test_sigmet2tac_volcanic_ash_sigmet(client: httpx.AsyncClient, volcanic_ash_sigmet: dict):
    """Test creating a Sigmet with Volcanic ash cloud phenomena considering the omit_change_va_and_rdoact_cloud flag"""
    # Set the flag to True and ensure 'change' is not part of the payload
    sigmet_config.omit_change_va_and_rdoact_cloud = True
    response = await client.post("/sigmet2tac", json=volcanic_ash_sigmet)
    assert response.status_code == 200
    r = await client.post("/sigmet2tac", json=volcanic_ash_sigmet)
    tacmessage = r.text
    assert tacmessage == (
        "EHAA SIGMET Y01 VALID 271153/271353 EFKL-\nEHAA AMSTERDAM FIR TEST\nVA ERUPTION VA CLD OBS N5234 E00541 SFC/FL650 STNR="
    )

@freeze_time("2020-11-18T11:31:00Z", ignore=['asyncio'])
async def test_sigmet_no_exception_on_iwxxm_publish_fail(
    client: httpx.AsyncClient, httpx_mock, post_draft_to_publish: dict, output_dir
):
    """Test that only TAC is published when IWXXM endpoint fails with a log"""
    backend_config.exception_on_iwxxm_publish_fail = False
    backend_config.exception_on_tac_publish_fail = True
    backend_config.sigmet_header.default = "WSNL31"
    backend_config.iwxxm_sigmet_header.default = "WSNL31"

    # Mock the failure and success responses for the messageconverter endpoints
    httpx_mock.add_response(
        method="POST", url=TAC_URL, text="TAC CONTENT", status_code=200
    )

    httpx_mock.add_response(
        method="POST", url=IWXXM_URL, json={"message": "Server error"}, status_code=500
    )

    # This publisher URL needs to be requested
    httpx_mock.add_response(
        method="POST",
        url=f"http://{docker_host}:8090/publish/WSNL31EHDB_2020-11-18T1131_20201118113100.txt",
        text="Publishing OK",
        status_code=200,
    )

    # Output dir is empty previous to this
    r = await client.post("/sigmet", json=post_draft_to_publish)
    post = r.json()
    assert "sigmet" in post


@freeze_time("2020-11-18T11:31:00Z", ignore=['asyncio'])
async def test_sigmet_exception_on_iwxxm_publish_fail(
    client: httpx.AsyncClient, httpx_mock, post_draft_to_publish: dict, output_dir
):
    """Test that nothing is published when IWXXM fails with an Exception"""
    backend_config.exception_on_iwxxm_publish_fail = True
    backend_config.exception_on_tac_publish_fail = False

    # Mock the failure and success responses for the messageconverter endpoints
    httpx_mock.add_response(
        method="POST", url=IWXXM_URL, json={"message": "Server error"}, status_code=500
    )

    r = await client.post("/sigmet", json=post_draft_to_publish)
    post = r.json()

    files = os.listdir(output_dir)
    assert len(files) == 0
    assert post["message"] == "Unable to publish SIGMET IWXXM."


@freeze_time("2020-11-18T11:31:00Z", ignore=['asyncio'])
async def test_sigmet_no_exception_on_tac_publish_fail(
    client: httpx.AsyncClient, httpx_mock, post_draft_to_publish: dict, output_dir
):
    """Test that nothing is published even when all booleans are False"""
    backend_config.exception_on_iwxxm_publish_fail = False
    backend_config.exception_on_tac_publish_fail = False

    httpx_mock.add_response(method="POST", url=IWXXM_URL, text="", status_code=500)

    httpx_mock.add_response(
        method="POST", url=TAC_URL, json={"message": "Server error"}, status_code=500
    )

    r =  await client.post("/sigmet", json=post_draft_to_publish)
    post =r.json()
    files = os.listdir(output_dir)
    assert len(files) == 0
    # Check that it is not an error message but a real SIGMET
    assert "sigmet" in post


@freeze_time("2020-11-18T11:31:00Z", ignore=['asyncio'])
async def test_sigmet_exception_on_tac_publish_fail(
    client: httpx.AsyncClient, httpx_mock, post_draft_to_publish: dict, output_dir
):
    """Test that IWXXM is published when the boolean for TAC is True"""
    backend_config.exception_on_iwxxm_publish_fail = False
    backend_config.exception_on_tac_publish_fail = True
    backend_config.sigmet_header.default = "WSNL31"

    httpx_mock.add_response(
        method="POST", url=IWXXM_URL, text="TAC CONTENT", status_code=200
    )

    httpx_mock.add_response(
        method="POST", url=TAC_URL, json={"message": "Server error"}, status_code=500
    )


    # This publisher URL needs to be requested since the IWXXM will be published
    httpx_mock.add_response(
        method="POST",
        url=f"http://{docker_host}:8090/publish/A_WSNL31EHDB181131_C_EHDB_20201118113100.xml",
        text="Publishing OK",
        status_code=200,
    )

    r = await client.post("/sigmet", json=post_draft_to_publish)
    post = r.json()
    assert "message" in post
    assert post["message"] == "Unable to publish SIGMET TAC."


async def test_get_missing(client: httpx.AsyncClient):
    """Test that verifies trying to fetch sigmet that does not exists returns
    404"""
    get = await client.get(f"/sigmet/{uuid.uuid1()}")
    assert get.status_code == 404


async def test_get_silly_input(client: httpx.AsyncClient):
    """Test that trying to get sigmet by uuid that is not a valid uuid returns
    400"""
    get = await client.get("/sigmet/notanuuid")
    assert get.status_code == 400


async def test_save_partial_sigmet_draft(client: httpx.AsyncClient, draft: dict):
    """Test that a missing GeoJson field does not cause problems"""
    """This is a good example to check that all serializers and attributes
    implemented in the original classes have been kept when creating a partial
    type """
    draft.pop("uuid")
    draft.pop("startGeometry")
    r = await draft_sigmet(client, draft)
    post = r.json()
    assert post["sigmet"]["status"] == "DRAFT"


async def test_cancellation(client: httpx.AsyncClient, published: dict):
    """Test that verifies a SIGMET can be first published and then cancelled"""
    published.pop("uuid")
    r = await publish_sigmet(client, published)
    post = r.json()
    r2 = await client.get(f"/sigmet/{post['uuid']}")
    get = r2.json()
    r3 = await cancel_sigmet(client, get["sigmet"])
    post = r3.json()
    assert post["sigmet"]["status"] == "CANCELLED"


def test_cancel_sigmet_type_with_original_type_config(published: dict):
    """Test that cancelled SIGMET keeps the original type if configured to do so"""
    backend_config.cnl_sigmet_original_type = True
    published_sigmet = Sigmet(**published)
    cancel_sigmet = make_cancelled(published_sigmet)
    assert cancel_sigmet.type == "TEST"


def test_cancel_sigmet_type_without_original_type_config(published: dict):
    """Test that cancelled SIGMET is of NORMAL type if it's not configured to
    keep the original type"""
    # Temporarily set the flag as False
    backend_config.cnl_sigmet_original_type = False
    published_sigmet = Sigmet(**published)
    cancel_sigmet = make_cancelled(published_sigmet)
    assert cancel_sigmet.type == "NORMAL"
    backend_config.cnl_sigmet_original_type = True


@freeze_time("2020-11-09", ignore=['asyncio'])
def test_sequence(published):
    """Test that next_in_sequence and sequence_prefix setting work"""
    assert (
        SigmetBase(
            status="PUBLISHED", type="TEST", phenomenon=published["phenomenon"]
        ).next_in_sequence(published)
        == "Z03"
    )


# def test_sigmets_expire():
#     """Test to verify old sigmets are returned as expired"""
#     raise NotImplementedError()

# We should also test that the API gives reasonable errors when asked for
# nonsensical endpoint, nonsensical input etc.


async def test_publish_verify(client: httpx.AsyncClient, published: dict):
    """Test that trying to publish with missing data causes error. This is
    essential to how we are using PublishBody etc. for branching code"""
    published.pop("firName")
    r = await publish_sigmet(client, published)
    assert r.status_code == 400


async def test_sigmetconfig(client: httpx.AsyncClient, sigmetconfig: dict):
    """Test that sigmetConfig is recieved and contains matching data"""
    r = await client.get("/sigmet-config")
    assert sorted(r.json()) == sorted(sigmetconfig)


async def test_lastupdatedate(client: httpx.AsyncClient, draft: dict):
    """Test to verify that lastupdatedate gets updated approppriately"""
    draft.pop("uuid")
    r = await draft_sigmet(client, draft)
    post = r.json()
    post["sigmet"]["type"] = "EXERCISE"
    time.sleep(1)
    r2 = await draft_sigmet(client, post["sigmet"])
    edit = r2.json()
    assert edit["creationDate"] == post["creationDate"]
    assert edit["lastUpdateDate"] > post["creationDate"]


def test_bulletin_header_location_indicator(sample: dict):
    """Test that the configured bulletin header location indicator is used"""
    backend_config.bulletin_header_location_indicator = "EHXX"
    backend_config.tac_envelope = "ZCZC/NNNN"
    backend_config.sigmet_header.default = "WSNL31"
    sigmet_sample: Sigmet = Sigmet(**sample)
    timestamp = sigmet_sample.issueDate
    tac_tuple = publish.format_tac(sigmet_sample, "TEST", timestamp)
    assert tac_tuple[1] == "ZCZC\nWSNL31 EHXX 091313\nTEST\nNNNN"


def test_tac_envelope(sample: dict):
    """Test TAC envelope configuration"""
    backend_config.tac_envelope = "HEADER/FOOTER"
    sigmet_sample: Sigmet = Sigmet(**sample)
    timestamp = sigmet_sample.issueDate
    tac_tuple = publish.format_tac(sigmet_sample, "TEST", timestamp)
    assert tac_tuple[1] == "HEADER\nWSNL31 EHXX 091313\nTEST\nFOOTER"


def test_tac_without_envelope(sample: dict):
    """Test TAC envelope configuration without the envelope"""
    backend_config.tac_envelope = None
    sigmet_sample: Sigmet = Sigmet(**sample)
    timestamp = sigmet_sample.issueDate
    tac_tuple = publish.format_tac(sigmet_sample, "TEST", timestamp)
    assert tac_tuple[1] == "WSNL31 EHXX 091313\nTEST"


def test_tac_line_separator_with_crlf(sample: dict):
    """Test TAC line separator with carriage return and line feed"""
    backend_config.tac_line_separator = "\r\n"
    sigmet_sample: Sigmet = Sigmet(**sample)
    timestamp = sigmet_sample.issueDate
    tac_tuple = publish.format_tac(sigmet_sample, "TEST\nTAC", timestamp)
    assert tac_tuple[1] == "WSNL31 EHXX 091313\r\nTEST\r\nTAC"


def test_tac_line_separator(sample: dict):
    """Test TAC line separator with line feed"""
    backend_config.tac_line_separator = "\n"
    sigmet_sample: Sigmet = Sigmet(**sample)
    timestamp = sigmet_sample.issueDate
    tac_tuple = publish.format_tac(sigmet_sample, "TEST\nTAC", timestamp)
    assert tac_tuple[1] == "WSNL31 EHXX 091313\nTEST\nTAC"


@freeze_time("2020-11-09T13:15:00Z", ignore=['asyncio'])
async def test_tac_formatting(sample: dict):
    """Test that tac formatting code under publish works"""
    sigmet_sample: Sigmet = Sigmet(**sample)
    now = datetime.now(tz=dateutil.tz.UTC)
    backend_config.bulletin_header_location_indicator = "EHDB"
    backend_config.tac_envelope = "HEADER/FOOTER"
    backend_config.sigmet_header.default = "WSNL31"

    client = await HTTPXClientWrapper.get_httpx_client()

    tac_request = await publish.translate_opmet(
        client,
        sigmet_sample,
        build_url(
            env_config.messageconverter_url, backend_config.sigmet_tac_endpoint
        ),
    )
    tac = tac_request.text
    tac_tuple = publish.format_tac(sigmet_sample, tac, now)
    assert tac_tuple == (
        f"WSNL31EHDB_2020-11-09T1315_20201109131500{backend_config.tac_filename_suffix}",
        (
            "HEADER\nWSNL31 EHDB 091315\nEHAA SIGMET 2 VALID 091315/091459 EHDB-\n"
            "EHAA AMSTERDAM FIR TEST\n"
            "SEV ICE OBS AT 1315Z WI N5321 E00025 - N5036 E00928 - N5001 E00247 -\n"
            "N5321 E00025 FL150 NC FCST AT 1459Z ENTIRE FIR=\n"
            "FOOTER"
        ),
    )


def compare_xml(xml1, xml2):
    # Clean/Simplify xml documents
    xml1_clean = " ".join(xml1.split())
    xml2_clean = " ".join(xml2.split())

    return xml1_clean == xml2_clean


@freeze_time("2020-11-09T13:15:00Z", ignore=['asyncio'])
async def test_iwxxm_formatting(sample: dict):
    """Test that IWXXM formatting code under publish works"""
    sigmet_sample: Sigmet = Sigmet(**sample)
    now = datetime.now(tz=dateutil.tz.UTC)
    backend_config.bulletin_header_location_indicator = "EHDB"
    backend_config.iwxxm_sigmet_header.default = "WSNL31"

    client = await HTTPXClientWrapper.get_httpx_client()

    iwxxm_request = await publish.translate_opmet(
        client,
        sigmet_sample,
        build_url(
            env_config.messageconverter_url, backend_config.sigmet_iwxxm_endpoint
        )
    )
    iwxxm = iwxxm_request.text
    iwxxm_tuple = publish.format_iwxxm(sigmet_sample, iwxxm, now)
    assert (
        iwxxm_tuple[0]
        == f"A_WSNL31EHDB091315_C_EHDB_20201109131500{backend_config.iwxxm_filename_suffix}"
    )
    assert compare_xml(iwxxm_tuple[1], "WSNL31 EHDB 091315\n" + iwxxm)


async def test_draft_becomes_published_without_metadata(
    client: httpx.AsyncClient, draft: dict, caplog
):
    """Test that a payload has NOT metadata when published"""
    backend_config.publish_metadata = False
    caplog.set_level(logging.INFO)

    draft.pop("uuid")
    r = await draft_sigmet(client, draft)
    post = r.json()
    r2 = await client.get(f"/sigmet/{post['uuid']}")
    get = r2.json()
    await publish_sigmet(client, get["sigmet"])

    model_check = True

    for record in caplog.records:
        try:
            log_data = json.loads(record.message)
            log_data_json = json.dumps(log_data)

        except (json.JSONDecodeError, ValidationError):
            continue

        if not len(log_data) == len(PublishData.model_fields):
            print(
                "PublishData model fields do not match payload fields, to many or to few fields"
            )
            model_check = False

        try:
            PublishData.model_validate_json(log_data_json)
        except ValidationError as e:
            model_check = False
            print(e)

    assert model_check


async def test_draft_becomes_published_with_metadata(client: httpx.AsyncClient, draft: dict, caplog):
    """Test that a payload has metadata when published"""
    backend_config.publish_metadata = True
    caplog.set_level(logging.INFO)

    draft.pop("uuid")
    r = await draft_sigmet(client, draft)
    post = r.json()
    r2 = await client.get(f"/sigmet/{post['uuid']}")
    get = r2.json()
    await publish_sigmet(client, get["sigmet"])

    model_check = True

    for record in caplog.records:
        try:
            log_data = json.loads(record.message)
            log_data_json = json.dumps(log_data)

        except (json.JSONDecodeError, ValidationError):
            continue

        if not len(log_data) == len(PublishMetaData.model_fields):
            print(
                "PublishData model fields do not match payload fields, to many or to few fields"
            )
            model_check = False

        try:
            PublishData.model_validate_json(log_data_json)
        except ValidationError as e:
            model_check = False
            print(e)

    assert model_check

    backend_config.publish_metadata = False
