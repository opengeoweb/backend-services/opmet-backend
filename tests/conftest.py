"""Pytest fixtures common to all tests"""

import logging
import os
import subprocess
import time

import httpx
import psycopg2
import pytest
import pytest_asyncio
from psycopg2 import OperationalError
from sqlalchemy import create_engine
from sqlalchemy.event import listens_for
from sqlalchemy.orm import sessionmaker
from tests.env_setup import (
    database_name,
    database_port,
    docker_host,
    messageconverter_port,
    publisher_port,
)

from opmet_backend.database import Base, engine, get_db


def pytest_collection_modifyitems(items):
    """Mark asyncio tests as having session scope"""
    session_scope_marker = pytest.mark.asyncio(loop_scope="session")
    pytest_asyncio_tests = (
        item for item in items if pytest_asyncio.is_async_test(item))
    for async_test in pytest_asyncio_tests:
        async_test.add_marker(session_scope_marker, append=False)


async def wait_for_service(url, timeout=300):
    """Poll until service is up"""
    start_time = time.time()
    while True:
        try:
            response = await httpx.AsyncClient().get(url)
            if response.status_code == 200:
                return
        except httpx.RequestError:
            pass
        if time.time() - start_time > timeout:
            logging.error(f"Service at {url} timed out after {timeout} seconds")
            raise RuntimeError(
                f"Service at {url} timed out after {timeout} seconds")
        time.sleep(1)


def check_database_up(host, port, dbname, user, password):
    try:
        # Connect to the PostgreSQL server
        connection = psycopg2.connect(host=host,
                                      port=port,
                                      dbname=dbname,
                                      user=user,
                                      password=password)
        cursor = connection.cursor()

        # Simple query to check connection
        cursor.execute("SELECT 1")
        cursor.fetchone()
        logging.info("Database is up and connection is OK.")
        cursor.close()
        connection.close()
    except OperationalError as e:
        logging.error(
            f"Error: Could not connect to the PostgreSQL database. {e}")
        raise RuntimeError(f"Could not connect to the PostgreSQL database: {e}")


# Database setup
engine = create_engine(os.environ['SQLALCHEMY_DATABASE_URL'])
TestingSessionLocal = sessionmaker(autocommit=False,
                                   autoflush=False,
                                   bind=engine)


@pytest_asyncio.fixture(loop_scope="session", scope="session")
def output_dir(tmp_path_factory):
    # Create a temporary directory
    dir = tmp_path_factory.mktemp("output")
    os.environ["DESTINATION"] = str(dir)
    logging.info(f"Output dir for testing is: {dir}")
    yield dir


@pytest_asyncio.fixture(loop_scope="session", scope="session", autouse=True)
async def docker_compose(output_dir):

    # Create messageservices, db, and publisher
    subprocess.run([
        "docker", "compose", "-f", "docker-compose-tests.yml", "up", "--build",
        "-d"
    ],
                   check=True)

    # Poll until all services are up and check that database is running
    await wait_for_service(
        f'http://{docker_host}:{messageconverter_port}/healthcheck')
    await wait_for_service(f'http://{docker_host}:{publisher_port}/healthcheck')
    check_database_up(docker_host, database_port, database_name, "geoweb",
                      "geoweb")

    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)

    yield

    # Stop after testing
    subprocess.run(
        ["docker", "compose", "-f", "docker-compose-tests.yml", "down"],
        check=True)


@pytest.fixture
def session(docker_compose):
    """Session fixture that creates a nested transaction and
       rolls it back at the end"""
    connection = engine.connect()
    transaction = connection.begin()
    session = TestingSessionLocal(bind=connection)

    nested = connection.begin_nested()

    @listens_for(session, "after_transaction_end")
    def end_savepoint(_session, _transaction):
        nonlocal nested
        if not nested.is_active:
            nested = connection.begin_nested()

    yield session

    session.close()
    transaction.rollback()
    connection.close()


@pytest.fixture
async def client(session):
    """Test client with session that rolls back db changes"""
    from opmet_backend.main import app

    def override_get_db():
        yield session

    app.dependency_overrides[get_db] = override_get_db

    async with httpx.AsyncClient(transport=httpx.ASGITransport(app=app),
                                 base_url="http://test") as client:
        yield client
    del app.dependency_overrides[get_db]
