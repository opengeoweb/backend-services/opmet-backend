"""Tests for utils"""
import httpx
import pytest
from pydantic import AnyHttpUrl

from opmet_backend.utils import build_url, strip_sequence


@pytest.mark.parametrize("sequence, output", [("-1", 1), ("2", 2), ("I01", 1),
                                              ("T05", 5), ("U10", 10)])
def test_strip_sequenced(sequence: str, output: int):
    """Test sequence_strip"""
    assert strip_sequence(sequence) == output


@pytest.mark.parametrize(
    "base_url, endpoint, output",
    [(AnyHttpUrl("https://example.com"), "/getsigmettac",
      httpx.URL("https://example.com/getsigmettac")),
     (AnyHttpUrl("https://example.com"), "getsigmettac",
      httpx.URL("https://example.com/getsigmettac")),
     (AnyHttpUrl("https://example.com/"), "/getsigmettac",
      httpx.URL("https://example.com/getsigmettac")),
     (AnyHttpUrl("https://example.com/"), "/getsigmettac/",
      httpx.URL("https://example.com/getsigmettac/")),
     (AnyHttpUrl("https://example.com/path"), "/getsigmettac",
      httpx.URL("https://example.com/path/getsigmettac")),
     (AnyHttpUrl("https://example.com/path/"), "getsigmettac",
      httpx.URL("https://example.com/path/getsigmettac")),
     (AnyHttpUrl("https://example.com/path/"), "/getsigmettac",
      httpx.URL("https://example.com/path/getsigmettac")),
     (AnyHttpUrl("https://example.com/path/"), "/getsigmettac/",
      httpx.URL("https://example.com/path/getsigmettac/")),
     (AnyHttpUrl("https://example.com:80/path//"), "/getsigmettac/",
      httpx.URL("https://example.com:80/path/getsigmettac/"))])
def test_build_url(base_url: AnyHttpUrl, endpoint: str, output: httpx.URL):
    """Test build_url"""
    assert build_url(base_url, endpoint) == output
