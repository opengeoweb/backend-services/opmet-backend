airmet1 = """
{
  \"canbe\": [

  ],
  \"lastUpdateDate\": \"2020-11-12T14:54:50Z\",
  \"creationDate\": \"2020-11-12T14:54:50Z\",
  \"airmet\": {
    \"change\": \"NC\",
    \"firName\": \"AMSTERDAM FIR\",
    \"isObservationOrForecast\": \"OBS\",
    \"issueDate\": \"2020-11-12T14:54:50Z\",
    \"level\": {
      \"unit\": \"FL\",
      \"value\": 100
    },
    \"levelInfoMode\": \"AT\",
    \"locationIndicatorATSR\": \"EHAA\",
    \"locationIndicatorATSU\": \"EHAA\",
    \"locationIndicatorMWO\": \"EHDB\",
    \"movementType\": \"STATIONARY\",
    \"observationOrForecastTime\": \"2020-11-12T14:54:06Z\",
    \"phenomenon\": \"FRQ_CB\",
    \"sequence\": \"1\",
    \"firGeometry\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[[4.999999, 54.999999],[5.000002, 55.000002],[6.500002, 55.000002],[6.5, 53.666667],[7.191667, 53.3],[7.14218, 52.898244],[7.133055, 52.888887],[7.065557, 52.385828],[7.063612, 52.346109],[7.031389, 52.268885],[7.053095, 52.237764],[6.405001, 51.830828],[5.94639, 51.811663],[6.222223, 51.361666],[5.934168, 51.036386],[6.011797, 50.757273],[5.651667, 50.824717],[5.848333, 51.139444],[5.078611, 51.391665],[4.397501, 51.452776],[3.952501, 51.214441],[3.373613, 51.309999],[3.36389, 51.313608],[3.362223, 51.320002],[3.370527, 51.36867],[3.370001, 51.369722],[2.000002, 51.500002],[3.15576, 52.913554],[2.761908, 54.379261],[3.368817, 55.764314], [4.331914, 55.332644], [5.0, 55.0], [4.999999, 54.999999]]]}, \"properties\": { \"centlong\": 4.98042633, \"REGION\": \"EUR\", \"StateName\": \"Netherlands\", \"FIRname\": \"AMSTERDAM FIR\",  \"StateCode\": \"NLD\", \"centlat\": 52.8618788, \"ICAOCODE\": \"EHAA\" }}]},
    \"startGeometry\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"properties\": {\"stroke\": \"#f24a00\", \"stroke-width\": 1.5, \"stroke-opacity\": 1, \"fill\": \"#f24a00\", \"fill-opacity\": 0.25, \"selectionType\": \"point\"}, \"geometry\": {\"type\": \"Point\", \"coordinates\": [3.444764981026363, 52.16315876932395]}}]},
    \"startGeometryIntersect\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"properties\": {\"stroke\": \"#f24a00\", \"stroke-width\": 1.5, \"stroke-opacity\": 1, \"fill\": \"#f24a00\", \"fill-opacity\": 0.5}, \"geometry\": {\"type\": \"Point\", \"coordinates\": [3.444765, 52.163159]}}]},
    \"status\": \"CANCELLED\",
    \"type\": \"NORMAL\",
    \"uuid\": \"d7f558bc-28e4-11eb-a6e4-6ad9decf9cdf\",
    \"validDateEnd\": \"2020-11-12T16:54:00Z\",
    \"validDateStart\": \"2020-11-12T15:54:00Z\"
  },
  \"uuid\": \"d7f558bc-28e4-11eb-a6e4-6ad9decf9cdf\"
}
"""

airmet2 = """
{
  \"canbe": [

  ],
  \"lastUpdateDate\": \"2020-11-12T14:55:56Z\",
  \"creationDate\": \"2020-11-12T14:55:56Z\",
  \"airmet": {
    \"change\": \"WKN\",
    \"firName\": \"AMSTERDAM FIR\",
    \"isObservationOrForecast\": \"OBS\",
    \"issueDate\": \"2020-11-12T14:55:56Z\",
    \"level\": {
      \"unit\": \"FL\",
      \"value\": 300
    },
    \"levelInfoMode\": \"BETW\",
    \"locationIndicatorATSR\": \"EHAA\",
    \"locationIndicatorATSU\": \"EHAA\",
    \"locationIndicatorMWO\": \"EHDB\",
    \"lowerLevel\": {
      \"unit\": \"FL\",
      \"value\": 200
    },
    \"movementType\": \"STATIONARY\",
    \"observationOrForecastTime\": \"2020-11-12T14:54:54Z\",
    \"phenomenon\": \"MOD_TURB\",
    \"sequence\": \"2\",
    \"firGeometry\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[[4.999999, 54.999999],[5.000002, 55.000002],[6.500002, 55.000002],[6.5, 53.666667],[7.191667, 53.3],[7.14218, 52.898244],[7.133055, 52.888887],[7.065557, 52.385828],[7.063612, 52.346109],[7.031389, 52.268885],[7.053095, 52.237764],[6.405001, 51.830828],[5.94639, 51.811663],[6.222223, 51.361666],[5.934168, 51.036386],[6.011797, 50.757273],[5.651667, 50.824717],[5.848333, 51.139444],[5.078611, 51.391665],[4.397501, 51.452776],[3.952501, 51.214441],[3.373613, 51.309999],[3.36389, 51.313608],[3.362223, 51.320002],[3.370527, 51.36867],[3.370001, 51.369722],[2.000002, 51.500002],[3.15576, 52.913554],[2.761908, 54.379261],[3.368817, 55.764314], [4.331914, 55.332644], [5.0, 55.0], [4.999999, 54.999999]]]}, \"properties\": { \"centlong\": 4.98042633, \"REGION\": \"EUR\", \"StateName\": \"Netherlands\", \"FIRname\": \"AMSTERDAM FIR\",  \"StateCode\": \"NLD\", \"centlat\": 52.8618788, \"ICAOCODE\": \"EHAA\" }}]},
    \"startGeometry\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"properties\": {\"selectionType\": \"box\", \"stroke\": \"#f24a00\", \"stroke-width\": 1.5, \"stroke-opacity\": 1, \"fill\": \"#f24a00\", \"fill-opacity\": 0.25, \"_adaguctype\": \"box\"}, \"geometry\": {\"type\": \"Polygon\", \"coordinates\": [[[2.2453981332917325, 54.245915586709486], [2.2453981332917325, 56.12844740334871], [5.243815252628305, 56.12844740334871], [5.243815252628305, 54.245915586709486], [2.2453981332917325, 54.245915586709486]]]}}]},
    \"startGeometryIntersect\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"properties\": {\"stroke\": \"#f24a00\", \"stroke-width\": 1.5, \"stroke-opacity\": 1, \"fill\": \"#f24a00\", \"fill-opacity\": 0.5}, \"geometry\": {\"type\": \"Polygon\", \"coordinates\": [[[5.243815, 54.99999948763103], [5.243815, 54.245916], [2.7977393052608734, 54.245916], [2.761908, 54.379261], [3.368817, 55.764314], [4.331914, 55.332644], [4.999999, 54.999999], [5.243815, 54.99999948763103]]]}}]},
    \"status\": \"CANCELLED\",
    \"type\": \"NORMAL\",
    \"uuid\": \"ff2afc3e-28e4-11eb-a6e4-6ad9decf9cdf\",
    \"validDateEnd\": \"2020-11-12T19:00:00Z\",
    \"validDateStart\": \"2020-11-12T18:00:00Z\"
  },
  \"uuid\": \"ff2afc3e-28e4-11eb-a6e4-6ad9decf9cdf\"
}
"""

airmet3 = """
{
  \"canbe\": [

  ],
  \"lastUpdateDate\": \"2020-11-12T15:04:05Z\",
  \"creationDate\": \"2020-11-12T15:04:05Z\",
  \"airmet\": {
    \"cancelsAirmetSequenceId\": \"2\",
    \"firName\": \"AMSTERDAM FIR\",
    \"issueDate\": \"2020-11-12T15:04:06Z\",
    \"locationIndicatorATSR\": \"EHAA\",
    \"locationIndicatorATSU\": \"EHAA\",
    \"locationIndicatorMWO\": \"EHDB\",
    \"sequence\": \"3\",
    \"status\": \"PUBLISHED\",
    \"type\": \"NORMAL\",
    \"uuid\": \"22f38446-28e6-11eb-9ad7-de92886c686f\",
    \"validDateEnd\": \"2020-11-12T16:54:00Z\",
    \"validDateEndOfAirmetToCancel\": \"2020-11-12T16:54:00Z\",
    \"validDateStart\": \"2020-11-12T15:04:05Z\",
    \"validDateStartOfAirmetToCancel\": \"2020-11-12T15:54:00Z\"
  },
  \"uuid\": \"22f38446-28e6-11eb-9ad7-de92886c686f\"
}
"""

airmet4 = """
{
  \"canbe": [

  ],
  \"lastUpdateDate\": \"2020-11-12T15:04:32Z",
  \"creationDate\": \"2020-11-12T15:04:32Z",
  \"airmet\": {
    \"cancelsAirmetSequenceId\": \"3\",
    \"firName\": \"AMSTERDAM FIR\",
    \"issueDate\": \"2020-11-12T15:04:32Z\",
    \"locationIndicatorATSR\": \"EHAA\",
    \"locationIndicatorATSU\": \"EHAA\",
    \"locationIndicatorMWO\": \"EHDB\",
    \"sequence\": \"4\",
    \"status\": \"PUBLISHED\",
    \"type\": \"NORMAL\",
    \"uuid\": \"32c57622-28e6-11eb-9ad7-de92886c686f\",
    \"validDateEnd\": \"2020-11-12T19:00:00Z\",
    \"validDateEndOfAirmetToCancel\": \"2020-11-12T19:00:00Z\",
    \"validDateStart\": \"2020-11-12T15:04:32Z\",
    \"validDateStartOfAirmetToCancel\": \"2020-11-12T18:00:00Z\"
  },
  \"uuid\": \"32c57622-28e6-11eb-9ad7-de92886c686f\"
}
"""

airmet5 = """
{
  \"canbe\": [
    \"CANCELLED\"
  ],
  \"lastUpdateDate\": \"2020-11-13T15:06:39Z\",
  \"creationDate\": \"2020-11-13T15:06:39Z\",
  \"airmet\": {
    \"change\": \"NC\",
    \"isObservationOrForecast\": \"FCST\",
    \"issueDate\": \"2020-11-13T15:06:44Z\",
    \"level\": {
      \"unit\": \"FL\",
      \"value\": 150
    },
    \"levelInfoMode\": \"AT\",
    \"locationIndicatorATSR\": \"EHAA\",
    \"movementType\": \"STATIONARY\",
    \"observationOrForecastTime\": null,
    \"phenomenon\": \"FRQ_CB\",
    \"sequence\": \"1\",
    \"firGeometry\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[[4.999999, 54.999999],[5.000002, 55.000002],[6.500002, 55.000002],[6.5, 53.666667],[7.191667, 53.3],[7.14218, 52.898244],[7.133055, 52.888887],[7.065557, 52.385828],[7.063612, 52.346109],[7.031389, 52.268885],[7.053095, 52.237764],[6.405001, 51.830828],[5.94639, 51.811663],[6.222223, 51.361666],[5.934168, 51.036386],[6.011797, 50.757273],[5.651667, 50.824717],[5.848333, 51.139444],[5.078611, 51.391665],[4.397501, 51.452776],[3.952501, 51.214441],[3.373613, 51.309999],[3.36389, 51.313608],[3.362223, 51.320002],[3.370527, 51.36867],[3.370001, 51.369722],[2.000002, 51.500002],[3.15576, 52.913554],[2.761908, 54.379261],[3.368817, 55.764314], [4.331914, 55.332644], [5.0, 55.0], [4.999999, 54.999999]]]}, \"properties\": { \"centlong\": 4.98042633, \"REGION\": \"EUR\", \"StateName\": \"Netherlands\", \"FIRname\": \"AMSTERDAM FIR\",  \"StateCode\": \"NLD\", \"centlat\": 52.8618788, \"ICAOCODE\": \"EHAA\" }}]},
    \"startGeometry\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"properties\": {\"selectionType\": \"box\", \"stroke\": \"#f24a00\", \"stroke-width\": 1.5, \"stroke-opacity\": 1, \"fill\": \"#f24a00\", \"fill-opacity\": 0.25, \"_adaguctype\": \"box\"}, \"geometry\": {\"type\": \"Polygon\", \"coordinates\": [[[2.7147343291401085, 50.52424208221409], [2.7147343291401085, 51.363677162117305], [7.1074168973984575, 51.363677162117305], [7.1074168973984575, 50.52424208221409], [2.7147343291401085, 50.52424208221409]]]}}]},
    \"startGeometryIntersect\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"properties\": {\"stroke\": \"#f24a00\", \"stroke-width\": 1.5, \"stroke-opacity\": 1, \"fill\": \"#f24a00\", \"fill-opacity\": 0.5}, \"geometry\": {\"type\": \"MultiPolygon\", \"coordinates\": [[[[3.3690553441271116, 51.363677], [4.2311424920175424, 51.363677], [3.952501, 51.214441], [3.373613, 51.309999], [3.36389, 51.313608], [3.362223, 51.320002], [3.3690553441271116, 51.363677]]], [[[5.164024107298758, 51.363677], [6.220990324753274, 51.363677], [6.222223, 51.361666], [5.934168, 51.036386], [6.011797, 50.757273], [5.651667, 50.824717], [5.848333, 51.139444], [5.164024107298758, 51.363677]]]]}}]},
    \"status\": \"PUBLISHED\",
    \"type\": \"NORMAL\",
    \"uuid\": \"d48f541e-25c1-11eb-b136-ead1e58c2b45\",
    \"validDateEnd\": \"2020-11-13T18:00:27Z\",
    \"validDateStart\": \"2020-11-13T16:05:27Z\"
  },
  \"uuid\": \"d48f541e-25c1-11eb-b136-ead1e58c2b45\"
}
"""

airmet6 = """
{
  \"canbe\": [
    \"DRAFTED\",
    \"DISCARDED\",
    \"PUBLISHED\"
  ],
  \"lastUpdateDate\": \"2020-11-16T18:51:19Z\",
  \"creationDate\": \"2020-11-16T18:51:19Z\",
  \"airmet\": {
    \"change\": \"WKN\",
    \"firName\": \"AMSTERDAM FIR\",
    \"isObservationOrForecast\": \"OBS\",
    \"level\": {
      \"unit\": \"FL\",
      \"value\": 100
    },
    \"levelInfoMode\": \"AT\",
    \"locationIndicatorATSR\": \"EHAA\",
    \"locationIndicatorATSU\": \"EHAA\",
    \"locationIndicatorMWO\": \"EHDB\",
    \"movementType\": \"STATIONARY\",
    \"observationOrForecastTime\": \"2020-11-16T18:50:30Z\",
    \"phenomenon\": \"FRQ_CB\",
    \"sequence\": \"-1\",
    \"firGeometry\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[[4.999999, 54.999999],[5.000002, 55.000002],[6.500002, 55.000002],[6.5, 53.666667],[7.191667, 53.3],[7.14218, 52.898244],[7.133055, 52.888887],[7.065557, 52.385828],[7.063612, 52.346109],[7.031389, 52.268885],[7.053095, 52.237764],[6.405001, 51.830828],[5.94639, 51.811663],[6.222223, 51.361666],[5.934168, 51.036386],[6.011797, 50.757273],[5.651667, 50.824717],[5.848333, 51.139444],[5.078611, 51.391665],[4.397501, 51.452776],[3.952501, 51.214441],[3.373613, 51.309999],[3.36389, 51.313608],[3.362223, 51.320002],[3.370527, 51.36867],[3.370001, 51.369722],[2.000002, 51.500002],[3.15576, 52.913554],[2.761908, 54.379261],[3.368817, 55.764314], [4.331914, 55.332644], [5.0, 55.0], [4.999999, 54.999999]]]}, \"properties\": { \"centlong\": 4.98042633, \"REGION\": \"EUR\", \"StateName\": \"Netherlands\", \"FIRname\": \"AMSTERDAM FIR\",  \"StateCode\": \"NLD\", \"centlat\": 52.8618788, \"ICAOCODE\": \"EHAA\" }}]},
    \"startGeometry\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"properties\": {\"stroke\": \"#f24a00\", \"stroke-width\": 1.5, \"stroke-opacity\": 1, \"fill\": \"#f24a00\", \"fill-opacity\": 0.25, \"selectionType\": \"point\"}, \"geometry\": {\"type\": \"Point\", \"coordinates\": [4.344290116827334, 54.070343536664275]}}]},
    \"startGeometryIntersect\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"properties\": {\"stroke\": \"#f24a00\", \"stroke-width\": 1.5, \"stroke-opacity\": 1, \"fill\": \"#f24a00\", \"fill-opacity\": 0.5}, \"geometry\": {\"type\": \"Point\", \"coordinates\": [4.34429, 54.070344]}}]},
    \"status\": \"DRAFT\",
    \"type\": \"NORMAL\",
    \"uuid\": \"b6b9b682-283c-11eb-8aad-46b76cfdc0c0\",
    \"validDateEnd\": \"2020-11-16T20:50:00Z\",
    \"validDateStart\": \"2020-11-16T19:50:00Z\"
  },
  \"uuid\": \"b6b9b682-283c-11eb-8aad-46b76cfdc0c0\"
}
"""

airmet7 = """
{
  \"canbe\": [
    \"DRAFTED\",
    \"DISCARDED\",
    \"PUBLISHED\"
  ],
  \"lastUpdateDate\": \"2020-11-16T19:03:12Z\",
  \"creationDate\": \"2020-11-16T19:03:12Z\",
  \"airmet\": {
    \"change\": \"NC\",
    \"firName\": \"AMSTERDAM FIR\",
    \"isObservationOrForecast\": \"OBS\",
    \"issueDate\": \"2020-11-16T19:03:12Z\",
    \"level\": {
      \"unit\": \"FL\",
      \"value\": 200
    },
    \"levelInfoMode\": \"BETW\",
    \"locationIndicatorATSR\": \"EHAA\",
    \"locationIndicatorATSU\": \"EHAA\",
    \"locationIndicatorMWO\": \"EHDB\",
    \"lowerLevel\": {
      \"unit\": \"FL\",
      \"value\": 100
    },
    \"movementType\": \"STATIONARY\",
    \"observationOrForecastTime\": \"2020-11-16T19:02:01Z\",
    \"phenomenon\": \"MOD_ICE\",
    \"sequence\": \"-1\",
    \"firGeometry\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[[4.999999, 54.999999],[5.000002, 55.000002],[6.500002, 55.000002],[6.5, 53.666667],[7.191667, 53.3],[7.14218, 52.898244],[7.133055, 52.888887],[7.065557, 52.385828],[7.063612, 52.346109],[7.031389, 52.268885],[7.053095, 52.237764],[6.405001, 51.830828],[5.94639, 51.811663],[6.222223, 51.361666],[5.934168, 51.036386],[6.011797, 50.757273],[5.651667, 50.824717],[5.848333, 51.139444],[5.078611, 51.391665],[4.397501, 51.452776],[3.952501, 51.214441],[3.373613, 51.309999],[3.36389, 51.313608],[3.362223, 51.320002],[3.370527, 51.36867],[3.370001, 51.369722],[2.000002, 51.500002],[3.15576, 52.913554],[2.761908, 54.379261],[3.368817, 55.764314], [4.331914, 55.332644], [5.0, 55.0], [4.999999, 54.999999]]]}, \"properties\": { \"centlong\": 4.98042633, \"REGION\": \"EUR\", \"StateName\": \"Netherlands\", \"FIRname\": \"AMSTERDAM FIR\",  \"StateCode\": \"NLD\", \"centlat\": 52.8618788, \"ICAOCODE\": \"EHAA\" }}]},
    \"startGeometry\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"properties\": {\"selectionType\": \"box\", \"stroke\": \"#f24a00\", \"stroke-width\": 1.5, \"stroke-opacity\": 1, \"fill\": \"#f24a00\", \"fill-opacity\": 0.25, \"_adaguctype\": \"box\"}, \"geometry\": {\"type\": \"Polygon\", \"coordinates\": [[[2.4253031604519273, 52.41991273980649], [2.4253031604519273, 53.360584848568955], [4.284321774440601, 53.360584848568955], [4.284321774440601, 52.41991273980649], [2.4253031604519273, 52.41991273980649]]]}}]},
    \"startGeometryIntersect\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"properties\": {\"stroke\": \"#f24a00\", \"stroke-width\": 1.5, \"stroke-opacity\": 1, \"fill\": \"#f24a00\", \"fill-opacity\": 0.5}, \"geometry\": {\"type\": \"Polygon\", \"coordinates\": [[[3.0356377290331555, 53.360585], [4.284322, 53.360585], [4.284322, 52.419913], [2.752145888260214, 52.419913], [3.15576, 52.913554], [3.0356377290331555, 53.360585]]]}}]},
    \"status\": \"DRAFT\",
    \"type\": \"NORMAL\",
    \"uuid\": \"5fe78f1c-283e-11eb-ac4d-765eba51329b\",
    \"validDateEnd\": \"2020-11-16T21:02:00Z\",
    \"validDateStart\": \"2020-11-16T20:02:00Z\"
  },
  \"uuid\": \"5fe78f1c-283e-11eb-ac4d-765eba51329b\"
}
"""

postdraft = """
{
   \"changeStatusTo\":\"DRAFT\",
   \"airmet\":{
      \"phenomenon\":\"ISOL_TS\",
      \"isObservationOrForecast\":\"OBS\",
      \"observationOrForecastTime\":\"2020-11-17T17:27:23Z\",
      \"validDateStart\":\"2020-11-17T18:27:00Z\",
      \"validDateEnd\":\"2020-11-17T19:27:00Z\",
      \"locationIndicatorATSR\":\"EHAA\",
      \"locationIndicatorATSU\":\"EHAA\",
      \"locationIndicatorMWO\":\"EHDB\",
      \"firName\":\"AMSTERDAM FIR\",
      \"firGeometry\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[[4.999999, 54.999999],[5.000002, 55.000002],[6.500002, 55.000002],[6.5, 53.666667],[7.191667, 53.3],[7.14218, 52.898244],[7.133055, 52.888887],[7.065557, 52.385828],[7.063612, 52.346109],[7.031389, 52.268885],[7.053095, 52.237764],[6.405001, 51.830828],[5.94639, 51.811663],[6.222223, 51.361666],[5.934168, 51.036386],[6.011797, 50.757273],[5.651667, 50.824717],[5.848333, 51.139444],[5.078611, 51.391665],[4.397501, 51.452776],[3.952501, 51.214441],[3.373613, 51.309999],[3.36389, 51.313608],[3.362223, 51.320002],[3.370527, 51.36867],[3.370001, 51.369722],[2.000002, 51.500002],[3.15576, 52.913554],[2.761908, 54.379261],[3.368817, 55.764314], [4.331914, 55.332644], [5.0, 55.0], [4.999999, 54.999999]]]}, \"properties\": { \"centlong\": 4.98042633, \"REGION\": \"EUR\", \"StateName\": \"Netherlands\", \"FIRname\": \"AMSTERDAM FIR\",  \"StateCode\": \"NLD\", \"centlat\": 52.8618788, \"ICAOCODE\": \"EHAA\" }}]},
      \"startGeometry\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill\":\"#f24a00\",
                  \"fill-opacity\":0.25,
                  \"selectionType\":\"point\"
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[
                     4.344290116827334,
                     53.073320281625854
                  ]
               }
            }
         ]
      },
      \"startGeometryIntersect\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill\":\"#f24a00\",
                  \"fill-opacity\":0.5
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[
                     4.34429,
                     53.07332
                  ]
               }
            }
         ]
      },
      \"levelInfoMode\":\"AT\",
      \"movementType\":\"STATIONARY\",
      \"change\":\"WKN\",
      \"sequence\":\"-1\",
      \"type\":\"NORMAL\",
      \"level\":{
         \"unit\":\"FL\",
         \"value\":350
      }
   }
}
"""

postdrafttostore = """
{
   \"changeStatusTo\":\"DRAFT\",
   \"airmet\":{
      \"phenomenon\":\"OCNL_CB\",
      \"isObservationOrForecast\":\"FCST\",
      \"observationOrForecastTime\":\"2020-11-18T10:31:00Z\",
      \"validDateStart\":\"2020-11-18T11:31:00Z\",
      \"validDateEnd\":\"2020-11-18T12:31:00Z\",
      \"issueDate\":\"1970-01-01T00:00:00Z\",
      \"locationIndicatorATSR\":\"EHAA\",
      \"locationIndicatorATSU\":\"EHAA\",
      \"locationIndicatorMWO\":\"EHDB\",
      \"firName\":\"AMSTERDAM FIR\",
      \"firGeometry\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[[4.999999, 54.999999],[5.000002, 55.000002],[6.500002, 55.000002],[6.5, 53.666667],[7.191667, 53.3],[7.14218, 52.898244],[7.133055, 52.888887],[7.065557, 52.385828],[7.063612, 52.346109],[7.031389, 52.268885],[7.053095, 52.237764],[6.405001, 51.830828],[5.94639, 51.811663],[6.222223, 51.361666],[5.934168, 51.036386],[6.011797, 50.757273],[5.651667, 50.824717],[5.848333, 51.139444],[5.078611, 51.391665],[4.397501, 51.452776],[3.952501, 51.214441],[3.373613, 51.309999],[3.36389, 51.313608],[3.362223, 51.320002],[3.370527, 51.36867],[3.370001, 51.369722],[2.000002, 51.500002],[3.15576, 52.913554],[2.761908, 54.379261],[3.368817, 55.764314], [4.331914, 55.332644], [5.0, 55.0], [4.999999, 54.999999]]]}, \"properties\": { \"centlong\": 4.98042633, \"REGION\": \"EUR\", \"StateName\": \"Netherlands\", \"FIRname\": \"AMSTERDAM FIR\",  \"StateCode\": \"NLD\", \"centlat\": 52.8618788, \"ICAOCODE\": \"EHAA\" }}]},
      \"startGeometry\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill\":\"#f24a00\",
                  \"fill-opacity\":0.25,
                  \"selectionType\":\"point\"
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[
                     4.224353432053872,
                     52.19992904814126
                  ]
               }
            }
         ]
      },
      \"startGeometryIntersect\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill\":\"#f24a00\",
                  \"fill-opacity\":0.5
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[
                     4.224353,
                     52.199929
                  ]
               }
            }
         ]
      },
      \"levelInfoMode\":\"AT\",
      \"movementType\":\"STATIONARY\",
      \"change\":\"WKN\",
      \"sequence\":\"-1\",
      \"type\":\"NORMAL\",
      \"level\":{
         \"unit\":\"FL\",
         \"value\":150
      }
   }
}
"""

postdrafttopublish = """
{
   \"changeStatusTo\":\"PUBLISHED\",
   \"airmet\":{
      \"phenomenon\":\"MOD_ICE\",
      \"isObservationOrForecast\":\"FCST\",
      \"observationOrForecastTime\":\"2020-11-18T11:33:00Z\",
      \"validDateStart\":\"2020-11-18T11:31:00Z\",
      \"validDateEnd\":\"2020-11-18T12:31:00Z\",
      \"locationIndicatorATSR\":\"EHAA\",
      \"locationIndicatorATSU\":\"EHAA\",
      \"locationIndicatorMWO\":\"EHDB\",
      \"firName\":\"AMSTERDAM FIR\",
      \"firGeometry\":{\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[[4.999999, 54.999999],[5.000002, 55.000002],[6.500002, 55.000002],[6.5, 53.666667],[7.191667, 53.3],[7.14218, 52.898244],[7.133055, 52.888887],[7.065557, 52.385828],[7.063612, 52.346109],[7.031389, 52.268885],[7.053095, 52.237764],[6.405001, 51.830828],[5.94639, 51.811663],[6.222223, 51.361666],[5.934168, 51.036386],[6.011797, 50.757273],[5.651667, 50.824717],[5.848333, 51.139444],[5.078611, 51.391665],[4.397501, 51.452776],[3.952501, 51.214441],[3.373613, 51.309999],[3.36389, 51.313608],[3.362223, 51.320002],[3.370527, 51.36867],[3.370001, 51.369722],[2.000002, 51.500002],[3.15576, 52.913554],[2.761908, 54.379261],[3.368817, 55.764314], [4.331914, 55.332644], [5.0, 55.0], [4.999999, 54.999999]]]}, \"properties\": { \"centlong\": 4.98042633, \"REGION\": \"EUR\", \"StateName\": \"Netherlands\", \"FIRname\": \"AMSTERDAM FIR\",  \"StateCode\": \"NLD\", \"centlat\": 52.8618788, \"ICAOCODE\": \"EHAA\" }}]},
      \"startGeometry\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill\":\"#f24a00\",
                  \"fill-opacity\":0.25,
                  \"selectionType\":\"point\"
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[
                     4.224353432053872,
                     52.19992904814126
                  ]
               }
            }
         ]
      },
      \"startGeometryIntersect\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill":"#f24a00\",
                  \"fill-opacity\":0.5
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[
                     4.224353,
                     52.199929
                  ]
               }
            }
         ]
      },
      \"level\":{
         \"unit\":\"FL\",
         \"value\":150
      },
      \"levelInfoMode\":\"AT\",
      \"movementType\":\"STATIONARY\",
      \"change\":\"WKN\",
      \"sequence\":\"-1\",
      \"type\":\"NORMAL\"
   }
}
"""

postdrafttostore2 = """
{
   \"changeStatusTo\":\"DRAFT\",
   \"airmet\":
   {
      \"phenomenon\":\"MOD_ICE\",
      \"isObservationOrForecast\":\"FCST\",
      \"observationOrForecastTime\":\"2020-11-18T10:31:00Z\",
      \"validDateStart\":\"2020-11-18T11:31:00Z\",
      \"validDateEnd\":\"2020-11-18T12:31:00Z\",
      \"issueDate\":\"1970-01-01T00:00:00Z\",
      \"locationIndicatorATSR\":\"EHAA\",
      \"locationIndicatorATSU\":\"EHAA\",
      \"locationIndicatorMWO\":\"EHDB\",
      \"firName\":\"AMSTERDAM FIR\",
      \"firGeometry\":{\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[[4.999999, 54.999999],[5.000002, 55.000002],[6.500002, 55.000002],[6.5, 53.666667],[7.191667, 53.3],[7.14218, 52.898244],[7.133055, 52.888887],[7.065557, 52.385828],[7.063612, 52.346109],[7.031389, 52.268885],[7.053095, 52.237764],[6.405001, 51.830828],[5.94639, 51.811663],[6.222223, 51.361666],[5.934168, 51.036386],[6.011797, 50.757273],[5.651667, 50.824717],[5.848333, 51.139444],[5.078611, 51.391665],[4.397501, 51.452776],[3.952501, 51.214441],[3.373613, 51.309999],[3.36389, 51.313608],[3.362223, 51.320002],[3.370527, 51.36867],[3.370001, 51.369722],[2.000002, 51.500002],[3.15576, 52.913554],[2.761908, 54.379261],[3.368817, 55.764314], [4.331914, 55.332644], [5.0, 55.0], [4.999999, 54.999999]]]}, \"properties\": { \"centlong\": 4.98042633, \"REGION\": \"EUR\", \"StateName\": \"Netherlands\", \"FIRname\": \"AMSTERDAM FIR\",  \"StateCode\": \"NLD\", \"centlat\": 52.8618788, \"ICAOCODE\": \"EHAA\" }}]},
      \"startGeometry\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":
               {
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill\":\"#f24a00\",
                  \"fill-opacity\":0.25,
                  \"selectionType\":\"point\"
               },
               \"geometry\":
               {
                  \"type\":\"Point\",
                  \"coordinates\":[4.224353432053872,52.19992904814126]
               }
            }
         ]
      },
      \"startGeometryIntersect\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill\":\"#f24a00\",
                  \"fill-opacity\":0.5
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[4.224353,52.199929]
               }
            }
         ]
      },
      \"levelInfoMode\":\"AT\",
      \"movementType\":\"STATIONARY\",
      \"change\":\"WKN\",
      \"sequence\":\"-1\",
      \"type\":\"NORMAL\",
      \"level\":{
         \"unit\":\"FL\",
         \"value\":150
      }
   }
}
"""

postdrafttopublish2 = """
{
   \"changeStatusTo\":\"PUBLISHED\",
   \"airmet\":{
      \"phenomenon\":\"MOD_ICE\",
      \"isObservationOrForecast\":\"FCST\",
      \"observationOrForecastTime\":\"2020-11-18T10:31:00Z\",
      \"validDateStart\":\"2020-11-18T11:31:00Z\",
      \"validDateEnd\":\"2020-11-18T12:31:00Z\",
      \"locationIndicatorATSR\":\"EHAA\",
      \"locationIndicatorATSU\":\"EHAA\",
      \"locationIndicatorMWO\":\"EHDB\",
      \"firName\":\"AMSTERDAM FIR\",
      \"firGeometry\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[[4.999999, 54.999999],[5.000002, 55.000002],[6.500002, 55.000002],[6.5, 53.666667],[7.191667, 53.3],[7.14218, 52.898244],[7.133055, 52.888887],[7.065557, 52.385828],[7.063612, 52.346109],[7.031389, 52.268885],[7.053095, 52.237764],[6.405001, 51.830828],[5.94639, 51.811663],[6.222223, 51.361666],[5.934168, 51.036386],[6.011797, 50.757273],[5.651667, 50.824717],[5.848333, 51.139444],[5.078611, 51.391665],[4.397501, 51.452776],[3.952501, 51.214441],[3.373613, 51.309999],[3.36389, 51.313608],[3.362223, 51.320002],[3.370527, 51.36867],[3.370001, 51.369722],[2.000002, 51.500002],[3.15576, 52.913554],[2.761908, 54.379261],[3.368817, 55.764314], [4.331914, 55.332644], [5.0, 55.0], [4.999999, 54.999999]]]}, \"properties\": { \"centlong\": 4.98042633, \"REGION\": \"EUR\", \"StateName\": \"Netherlands\", \"FIRname\": \"AMSTERDAM FIR\",  \"StateCode\": \"NLD\", \"centlat\": 52.8618788, \"ICAOCODE\": \"EHAA\" }}]},
      \"startGeometry\":{
         \"type\":\"FeatureCollection\",
         \"features\":
         [
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill\":\"#f24a00\",
                  \"fill-opacity\":0.25,
                  \"selectionType\":\"point\"
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[4.224353432053872,52.19992904814126]
               }
            }
         ]
      },
      \"startGeometryIntersect\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill\":\"#f24a00\",
                  \"fill-opacity\":0.5
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[4.224353,52.199929]
               }
            }
         ]
      },
      \"level\":{
         \"unit\":\"FL\",
         \"value\":150
      },
      \"levelInfoMode\":\"AT\",
      \"movementType\":\"STATIONARY\",
      \"change\":\"WKN\",
      \"sequence\":\"-1\",
      \"type\":\"NORMAL\",
      \"uuid\":\"00529800-2981-11eb-8999-dac116e0fe31\"
   }
}
"""

publishairmet = """
{
   \"changeStatusTo\":\"PUBLISHED\",
   \"airmet\":{
      \"phenomenon\":\"ISOL_TS\",
      \"isObservationOrForecast\":\"OBS\",
      \"observationOrForecastTime\":\"2020-11-17T17:27:23Z\",
      \"validDateStart\":\"2020-11-17T18:27:00Z\",
      \"validDateEnd\":\"2020-11-17T19:27:00Z\",
      \"locationIndicatorATSR\":\"EHAA\",
      \"locationIndicatorATSU\":\"EHAA\",
      \"locationIndicatorMWO\":\"EHDB\",
      \"firName\":\"AMSTERDAM FIR\",
      \"firGeometry\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[[4.999999, 54.999999],[5.000002, 55.000002],[6.500002, 55.000002],[6.5, 53.666667],[7.191667, 53.3],[7.14218, 52.898244],[7.133055, 52.888887],[7.065557, 52.385828],[7.063612, 52.346109],[7.031389, 52.268885],[7.053095, 52.237764],[6.405001, 51.830828],[5.94639, 51.811663],[6.222223, 51.361666],[5.934168, 51.036386],[6.011797, 50.757273],[5.651667, 50.824717],[5.848333, 51.139444],[5.078611, 51.391665],[4.397501, 51.452776],[3.952501, 51.214441],[3.373613, 51.309999],[3.36389, 51.313608],[3.362223, 51.320002],[3.370527, 51.36867],[3.370001, 51.369722],[2.000002, 51.500002],[3.15576, 52.913554],[2.761908, 54.379261],[3.368817, 55.764314], [4.331914, 55.332644], [5.0, 55.0], [4.999999, 54.999999]]]}, \"properties\": { \"centlong\": 4.98042633, \"REGION\": \"EUR\", \"StateName\": \"Netherlands\", \"FIRname\": \"AMSTERDAM FIR\",  \"StateCode\": \"NLD\", \"centlat\": 52.8618788, \"ICAOCODE\": \"EHAA\" }}]},
      \"startGeometry\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill\":\"#f24a00\",
                  \"fill-opacity\":0.25,
                  \"selectionType\":\"point\"
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[
                     4.344290116827334,
                     53.073320281625854
                  ]
               }
            }
         ]
      },
      \"startGeometryIntersect\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill\":\"#f24a00\",
                  \"fill-opacity\":0.5
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[
                     4.34429,
                     53.07332
                  ]
               }
            }
         ]
      },
      \"levelInfoMode\":\"AT\",
      \"movementType\":\"STATIONARY\",
      \"change\":\"WKN\",
      \"sequence\":\"-1\",
      \"type\":\"NORMAL\",
      \"level\":{
         \"unit\":\"FL\",
         \"value\":350
      }
   }
}
"""

publishairmet2 = """
{
   \"changeStatusTo\":\"PUBLISHED\",
   \"airmet\":{
      \"phenomenon\":\"ISOL_CB\",
      \"isObservationOrForecast\":\"OBS\",
      \"observationOrForecastTime\":\"2020-11-17T17:27:23Z\",
      \"validDateStart\":\"2020-11-17T18:27:00Z\",
      \"validDateEnd\":\"2020-11-17T19:27:00Z\",
      \"locationIndicatorATSR\":\"EHAA\",
      \"locationIndicatorATSU\":\"EHAA\",
      \"locationIndicatorMWO\":\"EHDB\",
      \"firName\":\"AMSTERDAM FIR\",
      \"firGeometry\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[[4.999999, 54.999999],[5.000002, 55.000002],[6.500002, 55.000002],[6.5, 53.666667],[7.191667, 53.3],[7.14218, 52.898244],[7.133055, 52.888887],[7.065557, 52.385828],[7.063612, 52.346109],[7.031389, 52.268885],[7.053095, 52.237764],[6.405001, 51.830828],[5.94639, 51.811663],[6.222223, 51.361666],[5.934168, 51.036386],[6.011797, 50.757273],[5.651667, 50.824717],[5.848333, 51.139444],[5.078611, 51.391665],[4.397501, 51.452776],[3.952501, 51.214441],[3.373613, 51.309999],[3.36389, 51.313608],[3.362223, 51.320002],[3.370527, 51.36867],[3.370001, 51.369722],[2.000002, 51.500002],[3.15576, 52.913554],[2.761908, 54.379261],[3.368817, 55.764314], [4.331914, 55.332644], [5.0, 55.0], [4.999999, 54.999999]]]}, \"properties\": { \"centlong\": 4.98042633, \"REGION\": \"EUR\", \"StateName\": \"Netherlands\", \"FIRname\": \"AMSTERDAM FIR\",  \"StateCode\": \"NLD\", \"centlat\": 52.8618788, \"ICAOCODE\": \"EHAA\" }}]},
      \"startGeometry\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill\":\"#f24a00\",
                  \"fill-opacity\":0.25,
                  \"selectionType\":\"point\"
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[
                     4.344290116827334,
                     53.073320281625854
                  ]
               }
            }
         ]
      },
      \"startGeometryIntersect\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill\":\"#f24a00\",
                  \"fill-opacity\":0.5
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[
                     4.34429,
                     53.07332
                  ]
               }
            }
         ]
      },
      \"levelInfoMode\":\"AT\",
      \"movementType\":\"STATIONARY\",
      \"change\":\"WKN\",
      \"sequence\":\"-1\",
      \"type\":\"NORMAL\",
      \"level\":{
         \"unit\":\"FL\",
         \"value\":350
      }
   }
}
"""

nonexistingairmet = """
{
   \"changeStatusTo\":\"PUBLISHED\",
   \"uuid\":\"nonexisting\",
   \"airmet\":{
      \"uuid\":\"nonexisting\",
      \"phenomenon\":\"ISOL_TS\",
      \"isObservationOrForecast\":\"OBS\",
      \"observationOrForecastTime\":\"2020-11-17T17:27:23Z\",
      \"validDateStart\":\"2020-11-17T18:27:00Z\",
      \"validDateEnd\":\"2020-11-17T19:27:00Z\",
      \"locationIndicatorATSR\":\"EHAA\",
      \"locationIndicatorATSU\":\"EHAA\",
      \"locationIndicatorMWO\":\"EHDB\",
      \"firName\":\"AMSTERDAM FIR\",
      \"firGeometry\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[[4.999999, 54.999999],[5.000002, 55.000002],[6.500002, 55.000002],[6.5, 53.666667],[7.191667, 53.3],[7.14218, 52.898244],[7.133055, 52.888887],[7.065557, 52.385828],[7.063612, 52.346109],[7.031389, 52.268885],[7.053095, 52.237764],[6.405001, 51.830828],[5.94639, 51.811663],[6.222223, 51.361666],[5.934168, 51.036386],[6.011797, 50.757273],[5.651667, 50.824717],[5.848333, 51.139444],[5.078611, 51.391665],[4.397501, 51.452776],[3.952501, 51.214441],[3.373613, 51.309999],[3.36389, 51.313608],[3.362223, 51.320002],[3.370527, 51.36867],[3.370001, 51.369722],[2.000002, 51.500002],[3.15576, 52.913554],[2.761908, 54.379261],[3.368817, 55.764314], [4.331914, 55.332644], [5.0, 55.0], [4.999999, 54.999999]]]}, \"properties\": { \"centlong\": 4.98042633, \"REGION\": \"EUR\", \"StateName\": \"Netherlands\", \"FIRname\": \"AMSTERDAM FIR\",  \"StateCode\": \"NLD\", \"centlat\": 52.8618788, \"ICAOCODE\": \"EHAA\" }}]},
      \"startGeometry\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill\":\"#f24a00\",
                  \"fill-opacity\":0.25,
                  \"selectionType\":\"point\"
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[
                     4.344290116827334,
                     53.073320281625854
                  ]
               }
            }
         ]
      },
      \"startGeometryIntersect\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill\":\"#f24a00\",
                  \"fill-opacity\":0.5
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[
                     4.34429,
                     53.07332
                  ]
               }
            }
         ]
      },
      \"levelInfoMode\":\"AT\",
      \"movementType\":\"STATIONARY\",
      \"change\":\"WKN\",
      \"sequence\":\"-1\",
      \"type\":\"NORMAL\",
      \"level\":{
         \"unit\":\"FL\",
         \"value\":350
      }
   }
}
"""

postincompletedrafttostore = """
{
   \"changeStatusTo\":\"DRAFT\",
   \"airmet\":{
      \"phenomenon\":\"ISOL_TS\",
      \"isObservationOrForecast\":\"\",
      \"observationOrForecastTime\":\"2020-11-18T10:31:00Z\",
      \"validDateStart\":\"2020-11-18T11:31:00Z\",
      \"validDateEnd\":\"2020-11-18T12:31:00Z\",
      \"locationIndicatorATSR\":\"EHAA\",
      \"locationIndicatorATSU\":\"EHAA\",
      \"locationIndicatorMWO\":\"EHDB\",
      \"firName\":\"AMSTERDAM FIR\",
      \"firGeometry\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[[4.999999, 54.999999],[5.000002, 55.000002],[6.500002, 55.000002],[6.5, 53.666667],[7.191667, 53.3],[7.14218, 52.898244],[7.133055, 52.888887],[7.065557, 52.385828],[7.063612, 52.346109],[7.031389, 52.268885],[7.053095, 52.237764],[6.405001, 51.830828],[5.94639, 51.811663],[6.222223, 51.361666],[5.934168, 51.036386],[6.011797, 50.757273],[5.651667, 50.824717],[5.848333, 51.139444],[5.078611, 51.391665],[4.397501, 51.452776],[3.952501, 51.214441],[3.373613, 51.309999],[3.36389, 51.313608],[3.362223, 51.320002],[3.370527, 51.36867],[3.370001, 51.369722],[2.000002, 51.500002],[3.15576, 52.913554],[2.761908, 54.379261],[3.368817, 55.764314], [4.331914, 55.332644], [5.0, 55.0], [4.999999, 54.999999]]]}, \"properties\": { \"centlong\": 4.98042633, \"REGION\": \"EUR\", \"StateName\": \"Netherlands\", \"FIRname\": \"AMSTERDAM FIR\",  \"StateCode\": \"NLD\", \"centlat\": 52.8618788, \"ICAOCODE\": \"EHAA\" }}]},
      \"startGeometry\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill\":\"#f24a00\",
                  \"fill-opacity\":0.25,
                  \"selectionType\":\"point\"
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[
                     4.224353432053872,
                     52.19992904814126
                  ]
               }
            }
         ]
      },
      \"startGeometryIntersect\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill":"#f24a00\",
                  \"fill-opacity\":0.5
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[
                     4.224353,
                     52.199929
                  ]
               }
            }
         ]
      },
      \"levelInfoMode\":\"\",
      \"movementType\":\"\",
      \"change\":\"\",
      \"type\":\"\"
   }
}
"""

postincompletedrafttopublish = """
{
   \"changeStatusTo\":\"PUBLISHED\",
   \"airmet\":{
      \"phenomenon\":\"ISOL_TS\",
      \"isObservationOrForecast\":\"\",
      \"observationOrForecastTime\":\"2020-11-18T10:31:00Z\",
      \"validDateStart\":\"2020-11-18T11:31:00Z\",
      \"validDateEnd\":\"2020-11-18T12:31:00Z\",
      \"locationIndicatorATSR\":\"EHAA\",
      \"locationIndicatorATSU\":\"EHAA\",
      \"locationIndicatorMWO\":\"EHDB\",
      \"firName\":\"AMSTERDAM FIR\",
      \"firGeometry\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[[4.999999, 54.999999],[5.000002, 55.000002],[6.500002, 55.000002],[6.5, 53.666667],[7.191667, 53.3],[7.14218, 52.898244],[7.133055, 52.888887],[7.065557, 52.385828],[7.063612, 52.346109],[7.031389, 52.268885],[7.053095, 52.237764],[6.405001, 51.830828],[5.94639, 51.811663],[6.222223, 51.361666],[5.934168, 51.036386],[6.011797, 50.757273],[5.651667, 50.824717],[5.848333, 51.139444],[5.078611, 51.391665],[4.397501, 51.452776],[3.952501, 51.214441],[3.373613, 51.309999],[3.36389, 51.313608],[3.362223, 51.320002],[3.370527, 51.36867],[3.370001, 51.369722],[2.000002, 51.500002],[3.15576, 52.913554],[2.761908, 54.379261],[3.368817, 55.764314], [4.331914, 55.332644], [5.0, 55.0], [4.999999, 54.999999]]]}, \"properties\": { \"centlong\": 4.98042633, \"REGION\": \"EUR\", \"StateName\": \"Netherlands\", \"FIRname\": \"AMSTERDAM FIR\",  \"StateCode\": \"NLD\", \"centlat\": 52.8618788, \"ICAOCODE\": \"EHAA\" }}]},
      \"startGeometry\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill\":\"#f24a00\",
                  \"fill-opacity\":0.25,
                  \"selectionType\":\"point\"
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[
                     4.224353432053872,
                     52.19992904814126
                  ]
               }
            }
         ]
      },
      \"startGeometryIntersect\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill":"#f24a00\",
                  \"fill-opacity\":0.5
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[
                     4.224353,
                     52.199929
                  ]
               }
            }
         ]
      },
      \"levelInfoMode\":\"\",
      \"movementType\":\"\",
      \"change\":\"\",
      \"sequence\":\"-1\",
      \"type\":\"\"
   }
}
"""

postcompletedrafttopublish = """
{
   \"changeStatusTo\":\"PUBLISHED\",
   \"airmet\":{
      \"phenomenon\":\"ISOL_TS\",
      \"isObservationOrForecast\":\"FCST\",
      \"observationOrForecastTime\":\"2020-11-18T10:31:00Z\",
      \"validDateStart\":\"2020-11-18T11:31:00Z\",
      \"validDateEnd\":\"2020-11-18T12:31:00Z\",
      \"locationIndicatorATSR\":\"EHAA\",
      \"locationIndicatorATSU\":\"EHAA\",
      \"locationIndicatorMWO\":\"EHDB\",
      \"firName\":\"AMSTERDAM FIR\",
      \"firGeometry\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[[4.999999, 54.999999],[5.000002, 55.000002],[6.500002, 55.000002],[6.5, 53.666667],[7.191667, 53.3],[7.14218, 52.898244],[7.133055, 52.888887],[7.065557, 52.385828],[7.063612, 52.346109],[7.031389, 52.268885],[7.053095, 52.237764],[6.405001, 51.830828],[5.94639, 51.811663],[6.222223, 51.361666],[5.934168, 51.036386],[6.011797, 50.757273],[5.651667, 50.824717],[5.848333, 51.139444],[5.078611, 51.391665],[4.397501, 51.452776],[3.952501, 51.214441],[3.373613, 51.309999],[3.36389, 51.313608],[3.362223, 51.320002],[3.370527, 51.36867],[3.370001, 51.369722],[2.000002, 51.500002],[3.15576, 52.913554],[2.761908, 54.379261],[3.368817, 55.764314], [4.331914, 55.332644], [5.0, 55.0], [4.999999, 54.999999]]]}, \"properties\": { \"centlong\": 4.98042633, \"REGION\": \"EUR\", \"StateName\": \"Netherlands\", \"FIRname\": \"AMSTERDAM FIR\",  \"StateCode\": \"NLD\", \"centlat\": 52.8618788, \"ICAOCODE\": \"EHAA\" }}]},
      \"startGeometry\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill\":\"#f24a00\",
                  \"fill-opacity\":0.25,
                  \"selectionType\":\"point\"
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[
                     4.224353432053872,
                     52.19992904814126
                  ]
               }
            }
         ]
      },
      \"startGeometryIntersect\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill":"#f24a00\",
                  \"fill-opacity\":0.5
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[
                     4.224353,
                     52.199929
                  ]
               }
            }
         ]
      },
      \"level\":{
         \"unit\":\"FL\",
         \"value\":150
      },
      \"levelInfoMode\":\"AT\",
      \"movementType\":\"STATIONARY\",
      \"change\":\"WKN\",
      \"sequence\":\"-1\",
      \"type\":\"NORMAL\"
   }
}
"""

publishincompleteairmet = """
{
   \"changeStatusTo\":\"PUBLISHED\",
   \"airmet\":{
      \"phenomenon\":\"ISOL_TS\",
      \"isObservationOrForecast\":\"OBS\",
      \"observationOrForecastTime\":null,
      \"validDateStart\":\"2020-11-17T18:27:00Z\",
      \"validDateEnd\":null,
      \"locationIndicatorATSR\":\"EHAA\",
      \"locationIndicatorATSU\":\"EHAA\",
      \"locationIndicatorMWO\":\"EHDB\",
      \"firName\":\"AMSTERDAM FIR\",
      \"firGeometry\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[[4.999999, 54.999999],[5.000002, 55.000002],[6.500002, 55.000002],[6.5, 53.666667],[7.191667, 53.3],[7.14218, 52.898244],[7.133055, 52.888887],[7.065557, 52.385828],[7.063612, 52.346109],[7.031389, 52.268885],[7.053095, 52.237764],[6.405001, 51.830828],[5.94639, 51.811663],[6.222223, 51.361666],[5.934168, 51.036386],[6.011797, 50.757273],[5.651667, 50.824717],[5.848333, 51.139444],[5.078611, 51.391665],[4.397501, 51.452776],[3.952501, 51.214441],[3.373613, 51.309999],[3.36389, 51.313608],[3.362223, 51.320002],[3.370527, 51.36867],[3.370001, 51.369722],[2.000002, 51.500002],[3.15576, 52.913554],[2.761908, 54.379261],[3.368817, 55.764314], [4.331914, 55.332644], [5.0, 55.0], [4.999999, 54.999999]]]}, \"properties\": { \"centlong\": 4.98042633, \"REGION\": \"EUR\", \"StateName\": \"Netherlands\", \"FIRname\": \"AMSTERDAM FIR\",  \"StateCode\": \"NLD\", \"centlat\": 52.8618788, \"ICAOCODE\": \"EHAA\" }}]},
      \"startGeometry\": null,
      \"startGeometryIntersect\":null,
      \"levelInfoMode\":\"AT\",
      \"level\":{
         \"unit\":\"FL\",
         \"value\":350
      },
      \"movementType\":\"STATIONARY\",
      \"change\":\"WKN\",
      \"type\":\"NORMAL\"
   }
}
"""

publishcompleteairmet = """
{
   \"changeStatusTo\":\"PUBLISHED\",
   \"airmet\":{
      \"phenomenon\":\"ISOL_TS\",
      \"isObservationOrForecast\":\"OBS\",
      \"observationOrForecastTime\":\"2020-11-17T17:27:23Z\",
      \"validDateStart\":\"2020-11-17T18:27:00Z\",
      \"validDateEnd\":\"2020-11-17T19:27:00Z\",
      \"locationIndicatorATSR\":\"EHAA\",
      \"locationIndicatorATSU\":\"EHAA\",
      \"locationIndicatorMWO\":\"EHDB\",
      \"firName\":\"AMSTERDAM FIR\",
      \"firGeometry\": {\"type\": \"FeatureCollection\", \"features\": [{\"type\": \"Feature\", \"geometry\": {\"type\": \"Polygon\",\"coordinates\": [[[4.999999, 54.999999],[5.000002, 55.000002],[6.500002, 55.000002],[6.5, 53.666667],[7.191667, 53.3],[7.14218, 52.898244],[7.133055, 52.888887],[7.065557, 52.385828],[7.063612, 52.346109],[7.031389, 52.268885],[7.053095, 52.237764],[6.405001, 51.830828],[5.94639, 51.811663],[6.222223, 51.361666],[5.934168, 51.036386],[6.011797, 50.757273],[5.651667, 50.824717],[5.848333, 51.139444],[5.078611, 51.391665],[4.397501, 51.452776],[3.952501, 51.214441],[3.373613, 51.309999],[3.36389, 51.313608],[3.362223, 51.320002],[3.370527, 51.36867],[3.370001, 51.369722],[2.000002, 51.500002],[3.15576, 52.913554],[2.761908, 54.379261],[3.368817, 55.764314], [4.331914, 55.332644], [5.0, 55.0], [4.999999, 54.999999]]]}, \"properties\": { \"centlong\": 4.98042633, \"REGION\": \"EUR\", \"StateName\": \"Netherlands\", \"FIRname\": \"AMSTERDAM FIR\",  \"StateCode\": \"NLD\", \"centlat\": 52.8618788, \"ICAOCODE\": \"EHAA\" }}]},
      \"startGeometry\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill\":\"#f24a00\",
                  \"fill-opacity\":0.25,
                  \"selectionType\":\"point\"
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[
                     4.344290116827334,
                     53.073320281625854
                  ]
               }
            }
         ]
      },
      \"startGeometryIntersect\":{
         \"type\":\"FeatureCollection\",
         \"features\":[
            {
               \"type\":\"Feature\",
               \"properties\":{
                  \"stroke\":\"#f24a00\",
                  \"stroke-width\":1.5,
                  \"stroke-opacity\":1,
                  \"fill\":\"#f24a00\",
                  \"fill-opacity\":0.5
               },
               \"geometry\":{
                  \"type\":\"Point\",
                  \"coordinates\":[
                     4.34429,
                     53.07332
                  ]
               }
            }
         ]
      },
      \"levelInfoMode\":\"AT\",
      \"movementType\":\"STATIONARY\",
      \"change\":\"WKN\",
      \"type\":\"NORMAL\",
      \"level\":{
         \"unit\":\"FL\",
         \"value\":350
      }
   }
}
"""
