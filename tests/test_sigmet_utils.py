"""Tests for SIGMET utils"""

from typing import Any

import pytest
from geojson_pydantic import Feature, FeatureCollection

from opmet_backend.sigmet_utils import point_to_circle


@pytest.fixture
def point() -> dict[str, Any]:
    """Point geometry"""
    return {
        "type": "Point",
        "coordinates": [3.5186289528575676, 53.444948116668186],
    }


@pytest.fixture
def point_collection(point: dict[str, Any]) -> FeatureCollection:
    """Point feature collection"""
    point_feature: Feature = Feature(geometry=point,
                                     type='Feature',
                                     properties={})
    return FeatureCollection(type="FeatureCollection", features=[point_feature])


def test_point_to_circle(point: dict[str, Any],
                         point_collection: FeatureCollection):
    """Test point_to_circle_with_radius"""
    expected_feature: Feature = Feature(
        geometry=point,
        type="Feature",
        properties={
            "selectionType": "circle",
            "radius": 30,
            "radiusUnit": "km"
        },
    )
    expected_collection: FeatureCollection = FeatureCollection(
        type="FeatureCollection", features=[expected_feature])
    assert point_to_circle(point_collection, 30) == expected_collection


def test_point_to_circle_with_props(point: dict[str, Any],
                                    point_collection: FeatureCollection):
    point_collection.features[0].properties = {"someProperty": "test"}
    expected_feature: Feature = Feature(geometry=point,
                                        type='Feature',
                                        properties={
                                            "someProperty": "test",
                                            "selectionType": "circle",
                                            "radius": 10,
                                            "radiusUnit": "km"
                                        })
    expected_collection: FeatureCollection = FeatureCollection(
        type="FeatureCollection", features=[expected_feature])
    assert point_to_circle(point_collection, 10) == expected_collection
