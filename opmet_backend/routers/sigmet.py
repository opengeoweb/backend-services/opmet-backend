"""Router for SIGMET-specific endpoints"""

import json
import logging
from datetime import datetime, timedelta
from typing import Literal, Union

import httpx
from dateutil import tz
from fastapi import APIRouter, Depends, status
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import HTTPException
from fastapi.responses import Response
from pydantic import UUID1, BaseModel
from sqlalchemy.exc import DataError
from sqlalchemy.orm import Session
from sqlalchemy.types import DateTime

import opmet_backend.database as db
import opmet_backend.publish as publish
from opmet_backend import sigmet_utils
from opmet_backend.config import backend_config, env_config, sigmet_config
from opmet_backend.models.shared import (
    DRAFT_SEQUENCE,
    ProductStatus,
    PublishData,
)
from opmet_backend.models.sigmet import (
    AnySigmet,
    CancelSigmet,
    ShortTestSigmet,
    Sigmet,
    SigmetDraft,
    SigmetListItem,
    make_cancelled,
)
from opmet_backend.utils import HTTPXClientWrapper, build_url

router = APIRouter(tags=["sigmet"])
logger = logging.getLogger(__name__)


@router.get("/sigmetlist",
            response_model=list[SigmetListItem],
            response_model_exclude_none=True)
def get_sigmet_list(
        published_limit: int = 15,
        time_limit: timedelta = backend_config.default_time_limit,
        session: Session = Depends(db.get_db),
):
    """Endpoint to fetch list of Sigmet objects. By default queries for 15
    latest published and all drafts from within the configured default timespan."""

    # Apply time limit
    query = session.query(db.SigmetDbItem).filter(
        db.SigmetDbItem.creationDate > datetime.now(tz=tz.UTC) - time_limit)
    # Up to published_limit non-drafts in descending order
    others = (query.filter(
        db.SigmetDbItem.sigmet["status"].astext !=
        ProductStatus.DRAFT).order_by(
            db.SigmetDbItem.sigmet["issueDate"].astext.cast(
                DateTime).desc()).limit(published_limit).all())
    # All drafts in descending order
    drafts = (query.filter(db.SigmetDbItem.sigmet["status"].astext ==
                           ProductStatus.DRAFT).order_by(
                               db.SigmetDbItem.lastUpdateDate.desc()).all())

    # Expire outdated messages
    for item in others:
        # Note: Solution "item.sigmet['validDateEnd'] < datetime.now(tz = dateutil.tz.UTC)" valid for mypy
        # but yields an unhandled error on testing
        if datetime.strptime(str(item.sigmet["validDateEnd"]),
                             "%Y-%m-%dT%H:%M:%SZ").replace(
                                 tzinfo=tz.UTC) < datetime.now(tz=tz.UTC):
            item.sigmet["status"] = ProductStatus.EXPIRED
    return drafts + others


async def publish_sigmet(
    session: Session,
    client: httpx.AsyncClient,
    sigmet: Sigmet | CancelSigmet | ShortTestSigmet,
):
    """Helper function to call all the functions from the publish module. Needs
    to be expanded to give better responses."""

    response = None
    now = datetime.now(tz=tz.UTC).replace(microsecond=0)
    sigmet_updated = publish.set_issue_date_and_sequence(session, now, sigmet)
    if isinstance(sigmet_updated, Sigmet):
        sigmet_final = sigmet_utils.radioactive_cloud_point_to_circle(
            sigmet_updated)
    else:
        sigmet_final = sigmet_updated
    # Generate and publish IWXXM (log if errors happen)
    try:
        url = build_url(env_config.messageconverter_url,
                        backend_config.sigmet_iwxxm_endpoint)
        result = await publish.translate_opmet(client, sigmet_final, url)
        iwxxm = result.text
        iwxxm_tuple = publish.format_iwxxm(sigmet_final, iwxxm, now)
        filename, contents = iwxxm_tuple
        if not backend_config.publish_metadata:
            iwxxm = PublishData(data=contents).model_dump()
        else:
            iwxxm = publish.iwxxm_meta(sigmet_final, iwxxm, now).model_dump()

        # Log the payload
        logging.info(iwxxm)

        response = await client.post(
            f"{env_config.publisher_url}/{filename}",
            json=iwxxm,
        )

        response.raise_for_status()

    except Exception as exc:
        if backend_config.exception_on_iwxxm_publish_fail:
            raise HTTPException(status.HTTP_500_INTERNAL_SERVER_ERROR,
                                "Unable to publish SIGMET IWXXM.") from exc
        # Only log the error
        logging.error("Error while publishing SIGMET IWXXM: %s", exc)

    # Generate and publish TAC
    try:
        url = build_url(env_config.messageconverter_url,
                        backend_config.sigmet_tac_endpoint)
        r = await publish.translate_opmet(client, sigmet_final, url)
        tac = r.text
        tac_tuple = publish.format_tac(sigmet_final, tac, now)
        filename, contents = tac_tuple

        if not backend_config.publish_metadata:
            tac = PublishData(data=contents).model_dump()
        else:
            tac = publish.tac_meta(sigmet_final, tac, now).model_dump()

        # Log the payload
        logging.info(tac)

        response = await client.post(f"{env_config.publisher_url}/{filename}",
                                     json=tac)

        response.raise_for_status()
    except Exception as exc:
        if backend_config.exception_on_tac_publish_fail:
            raise HTTPException(status.HTTP_500_INTERNAL_SERVER_ERROR,
                                "Unable to publish SIGMET TAC.") from exc
        # Log the error and stop publishing if TAC fails
        logging.error("Error while publishing SIGMET TAC: %s", exc)

    return response.status_code if response else 200


class DiscardBody(BaseModel):
    """Pydantic model that validates if input is approppriate for discarding
    a Sigmet"""

    changeStatusTo: Literal["DISCARDED"]
    sigmet: AnySigmet

    def update(
        self,
        session: Session,
        db_sigmet: db.SigmetDbItem,
    ):
        """Update method for removing a discarded SIGMET from database"""
        session.delete(db_sigmet)
        session.commit()
        return db_sigmet

    def create(self, _client, session: Session):
        """Method for creating a discarded SIGMET in database. Should
        probably never be reached."""
        db_sigmet = db.create_sigmet(self.sigmet, session)
        return db_sigmet


class DraftBody(BaseModel):
    """Pydantic model that validates if input is approppriate for storing or
    editing a draft"""

    changeStatusTo: Literal["DRAFT"]
    sigmet: AnySigmet

    def update(
        self,
        session: Session,
        db_sigmet: db.SigmetDbItem,
    ):
        """Update method for storing changes to a SIGMET draft"""
        self.sigmet.sequence = DRAFT_SEQUENCE

        db_sigmet.sigmet = jsonable_encoder(self.sigmet,
                                            exclude_none=True,
                                            exclude_unset=True)
        db_sigmet.lastUpdateDate = datetime.now(tz=tz.UTC).replace(
            microsecond=0)  # type: ignore
        session.commit()
        session.refresh(db_sigmet)
        return db_sigmet

    def create(self, session: Session):
        """Method to create a new SIGMET draft into database"""
        self.sigmet.sequence = DRAFT_SEQUENCE

        db_sigmet = db.create_sigmet(self.sigmet, session)
        return db_sigmet


class PublishBody(BaseModel):
    """Pydantic model that validates whether input is approppriate for
    publishing a SIGMET"""

    changeStatusTo: Literal["PUBLISHED"]
    sigmet: Sigmet | ShortTestSigmet

    async def update(self, session: Session, client: httpx.AsyncClient,
                     db_sigmet: db.SigmetDbItem):
        """Method for publishing an existing SIGMET"""
        await publish_sigmet(session, client, self.sigmet)

        db_sigmet.sigmet = jsonable_encoder(self.sigmet,
                                            exclude_none=True,
                                            exclude_unset=True)
        db_sigmet.lastUpdateDate = datetime.now(tz=tz.UTC).replace(
            microsecond=0)  # type: ignore
        session.commit()
        session.refresh(db_sigmet)
        return db_sigmet

    async def create(self, session: Session, client: httpx.AsyncClient):
        """Method for publishing and creating a new SIGMET message"""
        await publish_sigmet(session, client, self.sigmet)

        db_sigmet = db.create_sigmet(self.sigmet, session)
        return db_sigmet


class CancelBody(BaseModel):
    """Pydantic model that validates whether input is approppriate for
    cancelling a published SIGMET"""

    changeStatusTo: Literal["CANCELLED"]
    sigmet: Sigmet | ShortTestSigmet

    async def update(
        self,
        session: Session,
        client: httpx.AsyncClient,
        db_sigmet: db.SigmetDbItem,
    ):
        """Method for cancelling a previously existing published SIGMET"""
        cancelled = make_cancelled(self.sigmet)
        await publish_sigmet(session, client, cancelled)
        db.create_sigmet(cancelled, session)
        db_sigmet.sigmet = jsonable_encoder(self.sigmet,
                                            exclude_none=True,
                                            exclude_unset=True)
        session.commit()
        session.refresh(db_sigmet)
        return db_sigmet

    def create(self, session: Session):
        """Method to create a cancelled SIGMET in database. Should probably only
        be reached in testing."""
        db_sigmet = db.create_sigmet(self.sigmet, session)
        return db_sigmet


AnyBody = Union[PublishBody, CancelBody, DraftBody, DiscardBody]


@router.post("/sigmet",
             response_model=SigmetListItem,
             response_model_exclude_none=True)
async def post_sigmet(body: AnyBody, session: Session = Depends(db.get_db)):
    """Method for storing a new Sigmet in database or updating an existing
    entry with new data. Use changeStatusTo to publish or discard a
    draft or cancel a published Sigmet. Most of the business logic depends on
    pattern matching done by Pydantic as type of the input is being detected."""

    body.sigmet.status = ProductStatus(body.changeStatusTo)

    # Get client for all downstream requests
    client = await HTTPXClientWrapper.get_httpx_client()

    # Branch for modifying an existing item.
    if body.sigmet.uuid and body.sigmet.uuid is not None:
        try:
            db_sigmet: db.SigmetDbItem = db.get_sigmet_by_uuid(
                session, body.sigmet.uuid)
        except DataError as exc:
            raise HTTPException(
                status.HTTP_400_BAD_REQUEST,
                f"Request cannot be processed. Is the UUID {body.sigmet.uuid} valid?",
            ) from exc

        if db_sigmet is None:
            raise HTTPException(
                status.HTTP_404_NOT_FOUND,
                f"Sigmet with UUID {body.sigmet.uuid} not found in database.",
            )

        # The check for valid_status could also be implemented as validator for
        # the Body classes, but this is probably more readable
        if not SigmetDraft(**db_sigmet.sigmet).is_status_change_valid(  # pylint: disable=not-callable
                ProductStatus(body.changeStatusTo)):
            raise HTTPException(
                status.HTTP_422_UNPROCESSABLE_ENTITY,
                f"New status {body.changeStatusTo} not allowed for a {body.sigmet.status} object.",
            )
        if isinstance(body, PublishBody) or isinstance(body, CancelBody):
            return await body.update(session, client, db_sigmet)
        return body.update(session, db_sigmet)

    # Otherwise, we are simply creating a new item.
    if isinstance(body, PublishBody):
        return await body.create(session, client)
    return body.create(session)


@router.get("/sigmet/{uuid}",
            response_model=SigmetListItem,
            response_model_exclude_none=True)
def get_sigmet(uuid=UUID1, session: Session = Depends(db.get_db)):
    """Endpoint for fetching a Sigmet from database by its UUID. Does not exist
    in old implementation and is mainly used for testing."""

    try:
        sigmet = db.get_sigmet_by_uuid(session, uuid)
    except DataError as exc:
        raise HTTPException(
            status.HTTP_400_BAD_REQUEST,
            "Request cannot be processed. Is the path variable valid UUID?",
        ) from exc

    if sigmet is None:
        raise HTTPException(status.HTTP_404_NOT_FOUND,
                            f"Sigmet with UUID {uuid} not found.")

    return sigmet


@router.post("/sigmet2tac")
async def sigmet_to_tac(sigmet: Sigmet | CancelSigmet | ShortTestSigmet,
                        session: Session = Depends(db.get_db)):
    """Endpoint to translate a given Sigmet into a corresponding TAC message."""

    client = await HTTPXClientWrapper.get_httpx_client()

    if sigmet.sequence == DRAFT_SEQUENCE:
        # Update issue time and sequence for drafts
        sigmet_updated: publish.Publishable | ShortTestSigmet = (
            publish.set_issue_date_and_sequence(session,
                                                datetime.now(tz=tz.UTC),
                                                sigmet))
    else:
        sigmet_updated = sigmet

    if isinstance(sigmet_updated, Sigmet):
        sigmet_final = sigmet_utils.radioactive_cloud_point_to_circle(
            sigmet_updated)
    else:
        sigmet_final = sigmet_updated

    url = build_url(env_config.messageconverter_url,
                    backend_config.sigmet_tac_endpoint)
    tac_response = await publish.translate_opmet(client, sigmet_final, url)
    return Response(
        content=tac_response.text,
        media_type="text/plain",
        status_code=tac_response.status_code,
    )


@router.get("/sigmet-config")
def sigmetconfig():
    """Endpoint for delivering sigmetConfig.json contents to frontend"""
    return sigmet_config.model_dump(exclude_none=True)
