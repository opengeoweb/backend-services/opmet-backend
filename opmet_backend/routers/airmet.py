"""Router for AIRMET-specific endpoints"""

import logging
from datetime import datetime, timedelta
from typing import Literal

import httpx
from dateutil import tz
from fastapi import APIRouter, Depends, status
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import HTTPException
from fastapi.responses import Response
from pydantic import UUID1, BaseModel
from sqlalchemy import Column
from sqlalchemy.exc import DataError
from sqlalchemy.orm import Session
from sqlalchemy.types import DateTime

import opmet_backend.database as db
import opmet_backend.publish as publish
from opmet_backend.config import airmet_config, backend_config, env_config
from opmet_backend.models.airmet import (
    Airmet,
    AirmetDraft,
    AirmetListItem,
    AnyAirmet,
    CancelAirmet,
)
from opmet_backend.models.shared import (
    DRAFT_SEQUENCE,
    ProductStatus,
    PublishData,
)
from opmet_backend.utils import HTTPXClientWrapper, build_url

router = APIRouter(tags=["airmet"])
logger = logging.getLogger(__name__)


@router.get("/airmetlist",
            response_model=list[AirmetListItem],
            response_model_exclude_none=True)
def get_airmet_list(
        published_limit: int = 15,
        time_limit: timedelta = backend_config.default_time_limit,
        session: Session = Depends(db.get_db),
):
    """Endpoint to fetch list of Airmet objects. By default queries for 15
    latest published and all drafts from within the configured default timespan."""

    # Apply time limit
    query = session.query(db.AirmetDbItem).filter(
        db.AirmetDbItem.creationDate > datetime.now(tz=tz.UTC) - time_limit)
    # Up to published_limit non-drafts in descending order
    others = (query.filter(
        db.AirmetDbItem.airmet["status"].astext !=
        ProductStatus.DRAFT).order_by(
            db.AirmetDbItem.airmet["issueDate"].astext.cast(
                DateTime).desc()).limit(published_limit).all())
    # All drafts in descending order
    drafts = (query.filter(db.AirmetDbItem.airmet["status"].astext ==
                           ProductStatus.DRAFT).order_by(
                               db.AirmetDbItem.lastUpdateDate.desc()).all())

    # Expire outdated messages
    for item in others:
        # Note: Solution "item.airmet['validDateEnd'] < datetime.now(tz = dateutil.tz.UTC)" valid for mypy
        # but yields an unhandled error on testing (test_airmetlist)
        if datetime.strptime(str(item.airmet["validDateEnd"]),
                             "%Y-%m-%dT%H:%M:%SZ").replace(
                                 tzinfo=tz.UTC) < datetime.now(tz=tz.UTC):
            item.airmet["status"] = ProductStatus.EXPIRED
    return drafts + others


async def publish_airmet(session: Session, client: httpx.AsyncClient,
                         airmet: Airmet | CancelAirmet):
    """Helper function to call all the functions from the publish module. Needs
    to be expanded to give better responses."""

    response = None
    now = datetime.now(tz=tz.UTC).replace(microsecond=0)
    updated_airmet = publish.set_issue_date_and_sequence(session, now, airmet)
    # Generate and publish IWXXM (log possible errors)
    try:
        url = build_url(env_config.messageconverter_url,
                        backend_config.airmet_iwxxm_endpoint)
        r = await publish.translate_opmet(client, updated_airmet, url)
        iwxxm_txt = r.text
        iwxxm_tuple = publish.format_iwxxm(updated_airmet, iwxxm_txt, now)
        filename, contents = iwxxm_tuple

        if not backend_config.publish_metadata:
            iwxxm = PublishData(data=contents).model_dump()
        else:
            iwxxm = publish.iwxxm_meta(updated_airmet, iwxxm_txt,
                                       now).model_dump()

        # Log the payload
        logging.info(iwxxm)

        response = await client.post(
            f"{env_config.publisher_url}/{filename}",
            json=iwxxm,
        )
        response.raise_for_status()
    except Exception as exc:
        if backend_config.exception_on_iwxxm_publish_fail:
            raise HTTPException(status.HTTP_500_INTERNAL_SERVER_ERROR,
                                "Unable to publish AIRMET IWXXM.") from exc
        # Only log the error
        logging.error("Error while publishing AIRMET IWXXM: %s", exc)

    # Generate and publish TAC
    try:
        url = build_url(env_config.messageconverter_url,
                        backend_config.airmet_tac_endpoint)
        r = await publish.translate_opmet(client, updated_airmet, url)
        tac_txt = r.text
        tac_tuple = publish.format_tac(updated_airmet, tac_txt, now)
        filename, contents = tac_tuple

        if not backend_config.publish_metadata:
            tac = PublishData(data=contents).model_dump()
        else:
            tac = publish.tac_meta(updated_airmet, tac_txt, now).model_dump()

        # Log the payload
        logging.info(tac)

        response = await client.post(f"{env_config.publisher_url}/{filename}",
                                     json=tac)
        response.raise_for_status()
    except Exception as exc:
        # Log the error and stop publishing on publishing failure
        if backend_config.exception_on_tac_publish_fail:
            raise HTTPException(status.HTTP_500_INTERNAL_SERVER_ERROR,
                                "Unable to publish AIRMET TAC.") from exc
        # Log the error and stop publishing if TAC fails
        logger.error("Error while publishing AIRMET TAC: %s", exc)

    return response.status_code if response else 200


class DiscardBody(BaseModel):
    """Pydantic model that validates if input is approppriate for discarding
    a Airmet"""

    changeStatusTo: Literal["DISCARDED"]
    airmet: AnyAirmet

    def update(
        self,
        session: Session,
        db_airmet: db.AirmetDbItem,
    ):
        """Update method for removing a discarded AIRMET from database"""
        session.delete(db_airmet)
        session.commit()
        return db_airmet

    def create(self, session: Session):
        """Method for creating a discarded AirMET in database. Should
        probably never be reached."""
        db_airmet = db.create_airmet(self.airmet, session)
        return db_airmet


class DraftBody(BaseModel):
    """Pydantic model that validates if input is approppriate for storing or
    editing a draft"""

    changeStatusTo: Literal["DRAFT"]
    airmet: AnyAirmet

    def update(self, session: Session, db_airmet: db.AirmetDbItem):
        """Update method for storing changes to a AIRMET draft"""
        self.airmet.sequence = DRAFT_SEQUENCE

        db_airmet.airmet = jsonable_encoder(self.airmet,
                                            exclude_none=True,
                                            exclude_unset=True)
        db_airmet.lastUpdateDate = datetime.now(tz=tz.UTC).replace(
            microsecond=0)  # type: ignore
        session.commit()
        session.refresh(db_airmet)
        return db_airmet

    def create(self, session: Session):
        """Method to create a new AIRMET draft into database"""
        self.airmet.sequence = DRAFT_SEQUENCE

        db_airmet = db.create_airmet(self.airmet, session)
        return db_airmet


class PublishBody(BaseModel):
    """Pydantic model that validates whether input is approppriate for
    publishing aan AIRMET"""

    changeStatusTo: Literal["PUBLISHED"]
    airmet: Airmet

    async def update(self, session: Session, client: httpx.AsyncClient,
                     db_airmet: db.AirmetDbItem):
        """Method for publishing an existing AIRMET"""
        await publish_airmet(session, client, self.airmet)

        db_airmet.airmet = jsonable_encoder(self.airmet,
                                            exclude_none=True,
                                            exclude_unset=True)
        db_airmet.lastUpdateDate = datetime.now(tz=tz.UTC).replace(
            microsecond=0)  # type: ignore
        session.commit()
        session.refresh(db_airmet)
        return db_airmet

    async def create(self, session: Session, client: httpx.AsyncClient):
        """Method for publishing and creating a new AIRMET message"""
        await publish_airmet(session, client, self.airmet)

        db_airmet = db.create_airmet(self.airmet, session)
        return db_airmet


class CancelBody(BaseModel):
    """Pydantic model that validates whether input is approppriate for
    cancelling a published AIRMET"""

    changeStatusTo: Literal["CANCELLED"]
    airmet: Airmet

    async def update(
        self,
        session: Session,
        client: httpx.AsyncClient,
        db_airmet: db.AirmetDbItem,
    ):
        """Method for cancelling a previously existing published AIRMET"""

        cancelled = self.airmet.make_cancelled()
        await publish_airmet(session, client, cancelled)
        db.create_airmet(cancelled, session)

        db_airmet.airmet = jsonable_encoder(self.airmet,
                                            exclude_none=True,
                                            exclude_unset=True)
        session.commit()
        session.refresh(db_airmet)
        return db_airmet

    def create(self, session: Session):
        """Method to create a cancelled AIRMET in database. Should probably only
        be reached in testing."""
        db_airmet = db.create_airmet(self.airmet, session)
        return db_airmet


AnyBody = PublishBody | CancelBody | DraftBody | DiscardBody


@router.post("/airmet",
             response_model=AirmetListItem,
             response_model_exclude_none=True)
async def post_airmet(body: AnyBody, session: Session = Depends(db.get_db)):
    """Method for storing a new Airmet in database or updating an existing
    entry with new data. Use changeStatusTo to publish or discard a
    draft or cancel a published Airmet. Most of the business logic depends on
    pattern matching done by Pydantic as type of the input is being detected."""

    client = await HTTPXClientWrapper.get_httpx_client()

    body.airmet.status = ProductStatus(body.changeStatusTo)

    # Branch for modifying an existing item.
    if body.airmet.uuid and body.airmet.uuid is not None:
        try:
            db_airmet: db.AirmetDbItem = db.get_airmet_by_uuid(
                session, body.airmet.uuid)
        except DataError as exc:
            raise HTTPException(
                status.HTTP_400_BAD_REQUEST,
                f"Request cannot be processed. Is the UUID {body.airmet.uuid} valid?",
            ) from exc

        if db_airmet is None:
            raise HTTPException(
                status.HTTP_404_NOT_FOUND,
                f"Airmet with UUID {body.airmet.uuid} not found in database.",
            )

        # The check for valid_status could also be implemented as validator for
        # the Body classes, but this is probably more readable
        inner_airmet = AirmetDraft.model_validate(db_airmet.airmet)
        if not inner_airmet.is_status_change_valid(
                ProductStatus(body.changeStatusTo)):
            raise HTTPException(
                status.HTTP_422_UNPROCESSABLE_ENTITY,
                f"New status {body.changeStatusTo} not allowed for a {body.airmet.status} object.",
            )

        if isinstance(body, PublishBody) or isinstance(body, CancelBody):
            return await body.update(session, client, db_airmet)
        return body.update(session, db_airmet)

    if isinstance(body, DiscardBody):
        raise HTTPException(
            status.HTTP_422_UNPROCESSABLE_ENTITY,
            f"New status {body.changeStatusTo} not allowed when discarding.",
        )

    # Otherwise, we are simply creating a new item.
    if isinstance(body, PublishBody):
        return await body.create(session, client)
    return body.create(session)


@router.get("/airmet/{uuid}",
            response_model=AirmetListItem,
            response_model_exclude_none=True)
def get_airmet(uuid=UUID1, session: Session = Depends(db.get_db)):
    """Endpoint for fetching a Airmet from database by its UUID. Does not exist
    in old implementation and is mainly used for testing."""

    try:
        airmet = db.get_airmet_by_uuid(session, uuid)
    except DataError as exc:
        raise HTTPException(
            status.HTTP_400_BAD_REQUEST,
            "Request cannot be processed. Is the path variable valid UUID?",
        ) from exc

    if airmet is None:
        raise HTTPException(status.HTTP_404_NOT_FOUND,
                            f"Airmet with UUID {uuid} not found.")

    return airmet


@router.post("/airmet2tac")
async def airmet_to_tac(
        airmet: Airmet | CancelAirmet,
        session: Session = Depends(db.get_db),
):
    """Endpoint to translate a given Airmet into a corresponding TAC message."""

    client = await HTTPXClientWrapper.get_httpx_client()

    if airmet.sequence == DRAFT_SEQUENCE:
        # Update issue time and sequence for drafts
        updated_airmet = publish.set_issue_date_and_sequence(
            session, datetime.now(tz=tz.UTC), airmet)
    else:
        updated_airmet = airmet
    url = build_url(env_config.messageconverter_url,
                    backend_config.airmet_tac_endpoint)
    tac_response = await publish.translate_opmet(client, updated_airmet, url)
    return Response(
        content=tac_response.text,
        media_type="text/plain",
        status_code=tac_response.status_code,
    )


@router.get("/airmet-config")
def airmetconfig():
    """Endpoint for delivering airmetConfig.json contents to frontend"""
    return airmet_config.model_dump(exclude_none=True)
