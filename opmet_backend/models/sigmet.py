"""Module for Sigmet related models."""
import logging
from datetime import date, datetime, timedelta
from typing import Literal

import dateutil
from geojson_pydantic import FeatureCollection
from pydantic import UUID1, BaseModel, field_validator, model_validator
from sqlalchemy import select
from sqlalchemy.orm import Session
from sqlalchemy.types import DateTime

import opmet_backend.database as db
from opmet_backend.config import backend_config
from opmet_backend.utils import strip_sequence

from .shared import (
    Coordinates,
    Direction,
    Level,
    MetBase,
    ProductCanBe,
    ProductStatus,
    partial_model,
)


class SigmetBase(MetBase):
    """Shared base for Sigmet and CancelSigmet"""
    phenomenon: str | None = None
    type: Literal['NORMAL', 'TEST', 'EXERCISE', 'SHORT_TEST',
                  'SHORT_VA_TEST'] | None = None
    firName: str | None = None

    @field_validator('phenomenon')
    def validate_phenomenon(cls, value):
        """Validate that phenomenon is allowed."""
        assert value in backend_config.sigmet_phenomena
        return value

    def get_data_designator(self):
        """Helper method to get Opmet type and phenomenon specific header designator"""
        if self.phenomenon == 'VA_CLD':
            return backend_config.sigmet_header.va_cld
        if self.phenomenon == 'TC':
            return backend_config.sigmet_header.tc
        return backend_config.sigmet_header.default

    def get_iwxxm_prefix(self):
        """Helper method to get Opmet type and phenomenon specific prefix"""
        if self.phenomenon == 'VA_CLD':
            return 'LV'
        if self.phenomenon == 'TC':
            return 'LY'
        return 'LS'

    def get_iwxxm_header(self):
        """Helper method to get Opmet type and phenomenon specific header"""
        if self.phenomenon == 'VA_CLD':
            return backend_config.iwxxm_sigmet_header.va_cld
        if self.phenomenon == 'TC':
            return backend_config.iwxxm_sigmet_header.tc
        return backend_config.iwxxm_sigmet_header.default

    def get_sequence_prefix(self):
        """Gets approppriate prefix for sequence based on type and phenomenon"""
        if self.type == 'TEST':
            if self.phenomenon == 'VA_CLD':
                return backend_config.sigmet_prefixes['WV TEST']
            if self.phenomenon == "TC":
                return backend_config.sigmet_prefixes['WC TEST']
            return backend_config.sigmet_prefixes['WS TEST']
        if self.type == 'SHORT_TEST':
            return backend_config.sigmet_prefixes['WS TEST']
        if self.type == 'SHORT_VA_TEST':
            return backend_config.sigmet_prefixes['WV TEST']
        return backend_config.sigmet_prefixes[backend_config.sigmet_phenomena[
            self.phenomenon]]

    def old_get_previous(self,
                         session: Session,
                         time_limit=timedelta(hours=24),
                         published_limit=15):
        """Finds the approppriate previous published Sigmet. Obsolete version
        for backwards compatibility."""
        query = session.query(db.SigmetDbItem).filter(
            db.SigmetDbItem.creationDate > datetime.now(tz=dateutil.tz.UTC) -
            time_limit)
        published: list[db.SigmetDbItem] = query.filter(
            db.SigmetDbItem.sigmet['status'].astext !=
            ProductStatus.DRAFT).order_by(
                db.SigmetDbItem.sigmet['issueDate'].astext.cast(
                    DateTime).desc()).limit(published_limit).all()

        if len(published) > 0:
            if self.phenomenon in {"VA_CLD", "TC"}:
                for item in published:
                    if item.sigmet['phenomenon'] == self.phenomenon:
                        return item.sigmet  # Return first match from ordered list
            else:
                for item in published:
                    if item.sigmet['phenomenon'] not in ["VA_CLD", "TC"]:
                        return item.sigmet
        return None

    def get_previous(self, session: Session, time_limit=timedelta(hours=24)):
        """Finds the approppriate previous published Sigmet. Note that the
        default time_limit of 24 hours is usually NOT viable for production."""
        if backend_config.sigmet_sequence_prefix is not None:
            return self.old_get_previous(session, time_limit)
        if self.phenomenon is None:
            raise ValueError("Sigmet must have a phenomenon")
        phenomenon_group = backend_config.sigmet_phenomena[self.phenomenon]
        valid_phenomena = [
            key for key, value in backend_config.sigmet_phenomena.items()
            if value == phenomenon_group
        ]
        by_time = session.query(db.SigmetDbItem).filter(
            db.SigmetDbItem.creationDate > (datetime.now(tz=dateutil.tz.UTC) -
                                            time_limit))
        by_status = by_time.filter(
            db.SigmetDbItem.sigmet['status'].astext != ProductStatus.DRAFT)

        test_types = ['TEST', 'SHORT_TEST', 'SHORT_VA_TEST']
        if self.type in test_types:
            by_type = by_status.filter(
                db.SigmetDbItem.sigmet['type'].astext.in_(test_types))
            # these include self.type == 'SHORT_VA_TEST'
            if self.phenomenon == 'VA_CLD':
                by_phenomena = by_type.filter(
                    db.SigmetDbItem.sigmet['phenomenon'].astext == 'VA_CLD')
            else:
                by_phenomena = by_type.filter(
                    db.SigmetDbItem.sigmet['phenomenon'].astext != 'VA_CLD')
        else:
            by_type = by_status.filter(
                db.SigmetDbItem.sigmet['type'].astext.notin_(test_types))
            by_phenomena = by_type.filter(
                db.SigmetDbItem.sigmet['phenomenon'].astext.in_(
                    valid_phenomena))

        by_FIR = by_phenomena.filter(
            db.SigmetDbItem.sigmet['firName'].astext == self.firName)

        found_sigmets: list[db.SigmetDbItem] = by_FIR.order_by(
            db.SigmetDbItem.sigmet['issueDate'].astext.cast(
                DateTime).desc()).all()

        # Return most recent or None if no hits
        if len(found_sigmets) > 0:
            return found_sigmets[0].sigmet
        return None

    def old_next_in_sequence(self, previous: dict | None):
        """Helper function to find approppriate sequence value for a model.
        Old version to make migration easier. Should be deprecated soon."""

        if previous is None or date.today() != datetime.strptime(
                previous['issueDate'], "%Y-%m-%dT%H:%M:%SZ").date():
            return f'{backend_config.sigmet_sequence_prefix}1'
        return f"{backend_config.sigmet_sequence_prefix}{strip_sequence(previous['sequence']) + 1}"

    def next_in_sequence(self, previous: dict | None):
        """Helper function to find approppriate sequence value for a model"""
        if backend_config.sigmet_sequence_prefix is not None:
            return self.old_next_in_sequence(previous)
        prefix = self.get_sequence_prefix()

        if previous is None or date.today() != datetime.strptime(
                previous['issueDate'], "%Y-%m-%dT%H:%M:%SZ").date():
            return f'{prefix}01'

        sequence_int = strip_sequence(previous['sequence'])
        if sequence_int < 9:
            return f"{prefix}0{sequence_int + 1}"
        return f"{prefix}{sequence_int + 1}"


class CancelSigmet(SigmetBase):
    """Sigmet message for cancelling a published Sigmet."""
    validDateStart: datetime
    validDateEnd: datetime
    firName: str
    locationIndicatorMWO: str
    locationIndicatorATSU: str
    locationIndicatorATSR: str
    uuid: UUID1 | None = None
    issueDate: datetime | None = None
    cancelsSigmetSequenceId: str
    validDateStartOfSigmetToCancel: datetime
    validDateEndOfSigmetToCancel: datetime
    status: ProductStatus
    vaSigmetMoveToFIR: str | None = None
    sequence: str

    def product_can_be(self) -> list[ProductCanBe]:
        """Overloaded method for making sure CancelOpmets don't report as
        being possible to cancel. I am assuming a CancelSigmet will
        never have the status DRAFT."""
        return []


class Sigmet(SigmetBase):
    """Pydantic model of SIGMET messages"""
    validDateStart: datetime
    validDateEnd: datetime
    firName: str
    locationIndicatorMWO: str
    locationIndicatorATSU: str
    locationIndicatorATSR: str
    uuid: UUID1 | None = None
    issueDate: datetime | None = None
    phenomenon: str
    status: ProductStatus | None = None
    isObservationOrForecast: Literal["OBS", "FCST"]
    observationOrForecastTime: datetime | None = None
    change: Literal["INTSF", "WKN", "NC"] | None = None
    levelInfoMode: Literal["AT", "BETW", "BETW_SFC", "TOPS", "TOPS_ABV",
                           "TOPS_BLW", "ABV"]
    level: Level
    lowerLevel: Level | None = None
    firGeometry: FeatureCollection | None = None
    startGeometry: FeatureCollection
    startGeometryIntersect: FeatureCollection
    endGeometry: FeatureCollection | None = None
    endGeometryIntersect: FeatureCollection | None = None
    movementType: Literal["STATIONARY", "MOVEMENT", "FORECAST_POSITION",
                          "NO_VA_EXP"]
    movementSpeed: int | None = None
    movementUnit: Literal["KT", "KMH"] | None = None
    movementDirection: Direction | None = None
    vaSigmetVolcanoName: str | None = None
    vaSigmetVolcanoCoordinates: Coordinates | None = None
    vaSigmetMoveToFIR: str | None = None
    sequence: str


class ShortTestSigmet(SigmetBase):
    """Extra model to allow publishing short test sigmets in minimal form."""
    type: Literal['SHORT_TEST', 'SHORT_VA_TEST']
    phenomenon: str
    status: ProductStatus | None = None
    validDateStart: datetime
    validDateEnd: datetime
    firName: str
    locationIndicatorMWO: str
    locationIndicatorATSU: str
    locationIndicatorATSR: str
    issueDate: datetime | None = None
    uuid: UUID1 | None = None
    vaSigmetMoveToFIR: str | None = None
    sequence: str | None = None


@partial_model()
class SigmetDraft(Sigmet):
    """Version of Sigmet that allows storing incomplete (partial) messages.
    Applying this decorator, all fields become optional in a recursive way."""
    pass


AnySigmet = CancelSigmet | SigmetDraft | ShortTestSigmet


class SigmetListItem(BaseModel):
    """The model that matches database ORM"""
    uuid: UUID1
    lastUpdateDate: datetime
    creationDate: datetime
    sigmet: AnySigmet
    author: str | None = None  # Unused for now
    canbe: list[ProductCanBe] = []

    @model_validator(mode='after')
    def make_canbe(self) -> 'SigmetListItem':
        if self.sigmet:
            self.canbe = self.sigmet.product_can_be()
        return self

    class ConfigDict:
        """Pydantic configuration"""
        from_attributes = True
        json_encoders = {
            datetime: lambda x: x.strftime("%Y-%m-%dT%H:%M:%SZ"),
        }


def make_cancelled(sigmet: Sigmet | ShortTestSigmet) -> CancelSigmet:
    """Method for generating a CancelSigmet to cancel this Sigmet."""
    now = datetime.now(tz=dateutil.tz.UTC).replace(microsecond=0)
    cnl_type = sigmet.type if backend_config.cnl_sigmet_original_type else 'NORMAL'
    if sigmet.sequence is None:
        raise ValueError("Sigmet must have a sequence number")
    cancels_sequence_id: str = sigmet.sequence
    return CancelSigmet(validDateStart=now,
                        validDateEnd=sigmet.validDateEnd,
                        sequence=sigmet.sequence,
                        issueDate=now,
                        firName=sigmet.firName,
                        locationIndicatorATSU=sigmet.locationIndicatorATSU,
                        locationIndicatorATSR=sigmet.locationIndicatorATSR,
                        locationIndicatorMWO=sigmet.locationIndicatorMWO,
                        vaSigmetMoveToFIR=sigmet.vaSigmetMoveToFIR,
                        status=ProductStatus.PUBLISHED,
                        cancelsSigmetSequenceId=cancels_sequence_id,
                        validDateEndOfSigmetToCancel=sigmet.validDateEnd,
                        validDateStartOfSigmetToCancel=sigmet.validDateStart,
                        phenomenon=sigmet.phenomenon,
                        type=cnl_type)
