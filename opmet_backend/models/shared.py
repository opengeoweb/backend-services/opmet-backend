"""Shared classes for both Sigmet and Airmet."""

import copy
import typing
from dataclasses import asdict
from datetime import datetime
from enum import Enum
from typing import Any, Callable, ClassVar, Literal, Optional, Type, cast

import pydantic
from pydantic import (
    BaseModel,
    SerializationInfo,
    field_validator,
    model_serializer,
    validator,
)
from pydantic.fields import FieldInfo

DRAFT_SEQUENCE = "-1"


class ProductStatus(str, Enum):
    """Enum for making using status values safer"""

    DRAFT = "DRAFT"
    PUBLISHED = "PUBLISHED"
    CANCELLED = "CANCELLED"
    EXPIRED = "EXPIRED"
    DISCARDED = "DISCARDED"


class ProductCanBe(str, Enum):
    """Enum for can be values"""

    DRAFTED = "DRAFTED"
    DISCARDED = "DISCARDED"
    PUBLISHED = "PUBLISHED"
    CANCELLED = "CANCELLED"
    RENEWED = "RENEWED"


class Level(BaseModel):
    """Flight level unit and value. messageservices expects uppercase.
    This could possibly be refined using data from sigmetConfig and airmetConfig."""

    unit: Literal["FT", "M", "FL"]  # , "ft", "m", "fl"]
    value: int

    @field_validator("unit")
    def make_uppercase(cls, value: str):
        """Some of my test data has lowercase values in this field but the
        converter API gives errors unless this field is uppercase."""
        return value.upper()


class Direction(str, Enum):
    """Enum for meaking dealing with direction strings safer."""

    NORTH = "N"
    NORTH_NORTHEAST = "NNE"
    NORTHEAST = "NE"
    EAST_NORTHEAST = "ENE"
    EAST = "E"
    EAST_SOUTHEAST = "ESE"
    SOUTHEAST = "SE"
    SOUTH_SOUTHEAST = "SSE"
    SOUTH = "S"
    SOUTH_SOUTHWEST = "SSW"
    SOUTHWEST = "SW"
    WEST_SOUTHWEST = "WSW"
    WEST = "W"
    WEST_NORTHWEST = "WNW"
    NORTHWEST = "NW"
    NORTH_NORTHWEST = "NNW"


class Coordinates(BaseModel):
    """Model for coordinates with latitude and longitude. Currently set as
    floats, but could perhaps be Decimal instead."""

    latitude: float
    longitude: float


class PublishMetaData(BaseModel):
    """Publish data with metadata information, including data, format, location_indicator, issued date,
    valid from and until dates, header, provider, and product.
    """

    data: str
    format: str
    location_indicator_ATSU: str
    issued_at: str
    valid_from: str
    valid_until: str
    header: str
    provider: str
    product: str


class PublishData(BaseModel):
    """
    Represents the data to be published.
    """

    data: str


class MetBase(BaseModel):
    """Base mode for storing shared methods and configuration for both SIGMET
    and AIRMET objects"""

    status: ProductStatus | None = None

    @field_validator("*", mode="before")
    @classmethod
    def empty_str_to_none(cls, value):
        """Convert empty strings to None to get around the issue of Frontend
        delivering some empty fields as '' and omitting some empty fields."""
        if value == "":
            return None
        return value

    def product_can_be(self) -> list[ProductCanBe]:
        """Method for returning valid can be values of an individual Opmet"""
        if self.status == ProductStatus.DRAFT:
            return [
                ProductCanBe.DRAFTED,
                ProductCanBe.PUBLISHED,
                ProductCanBe.DISCARDED,
            ]
        if self.status == ProductStatus.PUBLISHED:
            return [ProductCanBe.CANCELLED, ProductCanBe.RENEWED]
        if self.status == ProductStatus.EXPIRED:
            return [ProductCanBe.RENEWED]
        return []

    def is_status_change_valid(self, new_status: ProductStatus) -> bool:
        """Method for checking whether a status change is valid"""
        if self.status == ProductStatus.DRAFT:
            return new_status in [
                ProductStatus.DRAFT,
                ProductStatus.PUBLISHED,
                ProductStatus.DISCARDED,
            ]
        if self.status == ProductStatus.PUBLISHED:
            return new_status == ProductStatus.CANCELLED
        return False

    class ConfigDict:
        """Pydantic configuration"""

        json_encoders = {
            datetime: lambda x: x.strftime("%Y-%m-%dT%H:%M:%SZ"),
        }


Model = typing.TypeVar("Model", bound=BaseModel)


def partial_model(
    without_fields: Optional[list[str]] = None,
) -> Callable[[type[Model]], type[Model]]:
    """A decorator that creates a partial model.

    Use:
        @partial_model(without_fields=['field1','field2'])
        class PartialModel(OriginalModel):
    where PartialModel has the same fields as OriginalModel, but everything is optional.

    Optionally, some fields can be excluded using without_fields
    """
    if without_fields is None:
        without_fields = []

    def wrapper(model: Type[Model]) -> Type[Model]:

        def make_field_optional(field: FieldInfo,
                                default: Any = None,
                                omit: bool = False) -> tuple[Any, FieldInfo]:
            new = cast(FieldInfo, copy.deepcopy(field))
            new.default = default
            new.annotation = field.annotation

            # Wrap annotation in ClassVar if field in without_fields
            return ClassVar[new.annotation] if omit else new.annotation, new

        model_copy = copy.deepcopy(model)

        # Pydantic will error if validators are present without the field
        # so we set check_fields to false on all validators
        for dec_group_label, decs in asdict(
                model_copy.__pydantic_decorators__).items():
            for validator in decs.keys():
                decorator_info = getattr(
                    getattr(model.__pydantic_decorators__,
                            dec_group_label)[validator],
                    "info",
                )
                if hasattr(decorator_info, "check_fields"):
                    setattr(
                        decorator_info,
                        "check_fields",
                        False,
                    )
        return pydantic.create_model(  # type: ignore[call-overload,no-any-return]
            model.__name__,
            __base__=model,
            __module__=model.__module__,
            **{
                field_name: make_field_optional(
                    field_info, omit=(field_name in without_fields)
                )
                for field_name, field_info in model.model_fields.items()
            },
        )

    return wrapper
