"""Module for AIRMET related models"""
from datetime import date, datetime, timedelta
from typing import Literal

import dateutil
from geojson_pydantic import FeatureCollection
from pydantic import (
    UUID1,
    BaseModel,
    field_validator,
    model_validator,
    validator,
)
from sqlalchemy.orm import Session
from sqlalchemy.types import DateTime

from opmet_backend.config import backend_config
from opmet_backend.utils import strip_sequence

from .. import database as db
from .shared import (
    Direction,
    Level,
    MetBase,
    ProductCanBe,
    ProductStatus,
    partial_model,
)


class AirmetBase(MetBase):
    """Shared base for Airmet and CancelAirmet"""

    def get_previous(self,
                     session: Session,
                     time_limit=timedelta(hours=24),
                     published_limit=15):
        """Finds the approppriate previous published Airmet.
        Copied from Sigmet and not verified to work."""
        query = session.query(db.AirmetDbItem).filter(
            db.AirmetDbItem.creationDate > datetime.now(tz=dateutil.tz.UTC) -
            time_limit)
        published: list[db.AirmetDbItem] = query.filter(
            db.AirmetDbItem.airmet['status'].astext !=
            ProductStatus.DRAFT).order_by(
                db.AirmetDbItem.airmet['issueDate'].astext.cast(
                    DateTime).desc()).limit(published_limit).all()

        if len(published) > 0:
            return published[0].airmet
        return None

    def next_in_sequence(self, previous: dict | None):
        """Helper function to find approppriate sequence value for a model"""

        if previous is None or date.today() != datetime.strptime(
                previous['issueDate'], "%Y-%m-%dT%H:%M:%SZ").date():
            return f'{backend_config.airmet_sequence_prefix}1'
        return f"{backend_config.airmet_sequence_prefix}{strip_sequence(previous['sequence']) + 1}"

    def get_data_designator(self):
        """Helper method to get Opmet type and phenomenon specific header"""
        return backend_config.airmet_header

    def get_iwxxm_prefix(self):
        """Helper method to get Opmet type and phenomenon specific prefix"""
        return 'LW'

    def get_iwxxm_header(self):
        """Helper method to get Opmet type and phenomenon specific header"""
        return backend_config.iwxxm_airmet_header


class CancelAirmet(AirmetBase):
    """Pydantic model for AIRMET message that cancels a published AIRMET"""
    validDateStart: datetime
    validDateEnd: datetime
    firName: str
    locationIndicatorMWO: str
    locationIndicatorATSU: str
    locationIndicatorATSR: str
    uuid: UUID1 | None = None
    issueDate: datetime | None = None
    phenomenon: Literal["BKN_CLD", "OVC_CLD", "FRQ_CB", "FRQ_TCU", "ISOL_CB",
                        "ISOL_TCU", "ISOL_TS", "ISOL_TSGR", "MOD_ICE",
                        "MOD_MTW", "MOD_TURB", "MT_OBSC", "OCNL_CB", "OCNL_TS",
                        "OCNL_TSGR", "OCNL_TCU", "SFC_VIS", "SFC_WIND"]
    cancelsAirmetSequenceId: str
    validDateStartOfAirmetToCancel: datetime
    validDateEndOfAirmetToCancel: datetime
    status: ProductStatus
    type: Literal['NORMAL', 'TEST', 'EXERCISE']
    sequence: str

    def product_can_be(self) -> list[ProductCanBe]:
        """Overloaded method for making sure CancelOpmets don't report as
        being possible to cancel. I am assuming a CancelAirmet will
        never have the status DRAFT."""
        return []


class Airmet(AirmetBase):
    """Pydantic model for AIRMET messages"""
    validDateStart: datetime
    validDateEnd: datetime
    firName: str
    locationIndicatorMWO: str
    locationIndicatorATSU: str
    locationIndicatorATSR: str
    uuid: UUID1 | None = None
    issueDate: datetime | None = None
    phenomenon: Literal["BKN_CLD", "OVC_CLD", "FRQ_CB", "FRQ_TCU", "ISOL_CB",
                        "ISOL_TCU", "ISOL_TS", "ISOL_TSGR", "MOD_ICE",
                        "MOD_MTW", "MOD_TURB", "MT_OBSC", "OCNL_CB", "OCNL_TS",
                        "OCNL_TSGR", "OCNL_TCU", "SFC_VIS", "SFC_WIND"]
    status: ProductStatus | None = None
    isObservationOrForecast: Literal["OBS", "FCST"]
    observationOrForecastTime: datetime | None = None
    type: Literal['NORMAL', 'TEST', 'EXERCISE']
    change: Literal["INTSF", "WKN", "NC"]
    levelInfoMode: Literal["AT", "BETW", "BETW_SFC", "TOPS", "TOPS_ABV",
                           "TOPS_BLW", "ABV"] | None = None
    level: Level | None = None
    lowerLevel: Level | None = None
    firGeometry: FeatureCollection | None = None
    startGeometry: FeatureCollection
    startGeometryIntersect: FeatureCollection
    movementType: Literal["STATIONARY", "MOVEMENT"]
    movementSpeed: float | None = None
    movementUnit: Literal["KT", "KMH"] | None = None
    movementDirection: Direction | None = None
    windSpeed: float | None = None
    windUnit: Literal["KT", "MPS"] | None = None
    windDirection: float | None = None
    visibilityValue: float | None = None
    visibilityCause: Literal["DZ", "DU", "PO", "DS", "FG", "FC", "GR", "HZ",
                             "PL", "BR", "RA", "SA", "SS", "FU", "SN", "SG",
                             "GS", "SQ", "VA"] | None = None
    visibilityUnit: Literal["M"] | None = None
    cloudLevelInfoMode: Literal["BETW", "BETW_SFC", "BETW_SFC_ABV",
                                "BETW_ABV"] | None = None
    cloudLevel: Level | None = None
    cloudLowerLevel: Level | None = None
    sequence: str

    def make_cancelled(self) -> CancelAirmet:
        """Method for generating a CancelAirmet to cancel this Airmet.
        Copied from Sigmet and currently untesed and incomplete"""
        now = datetime.now(tz=dateutil.tz.UTC).replace(microsecond=0)
        cnl_type = self.type if backend_config.cnl_airmet_original_type else 'NORMAL'
        return CancelAirmet(validDateStart=now,
                            validDateEnd=self.validDateEnd,
                            sequence=self.sequence,
                            issueDate=now,
                            firName=self.firName,
                            locationIndicatorATSU=self.locationIndicatorATSU,
                            locationIndicatorATSR=self.locationIndicatorATSR,
                            locationIndicatorMWO=self.locationIndicatorMWO,
                            status=ProductStatus.PUBLISHED,
                            cancelsAirmetSequenceId=self.sequence,
                            validDateEndOfAirmetToCancel=self.validDateEnd,
                            validDateStartOfAirmetToCancel=self.validDateStart,
                            phenomenon=self.phenomenon,
                            type=cnl_type)


@partial_model()
class AirmetDraft(Airmet):
    """Version of Airmet that allows storing incomplete (partial) messages.
    Applying this decorator, all fields become optional in a recursive way."""
    pass


AnyAirmet = CancelAirmet | AirmetDraft


class AirmetListItem(BaseModel):
    """The model that matches database ORM"""
    uuid: UUID1
    lastUpdateDate: datetime
    creationDate: datetime
    airmet: AnyAirmet
    author: str | None = None  # Unused for now
    canbe: list[ProductCanBe] = []

    @model_validator(mode='after')
    def make_canbe(self) -> 'AirmetListItem':
        if self.airmet:
            self.canbe = self.airmet.product_can_be()
        return self

    class ConfigDict:
        """Pydantic configuration"""
        from_attributes = True
        json_encoders = {
            datetime: lambda x: x.strftime("%Y-%m-%dT%H:%M:%SZ"),
        }
