"""Utility functions specific to SIGMETs"""

from geojson_pydantic import FeatureCollection

from opmet_backend.config import backend_config
from opmet_backend.models.sigmet import Sigmet


def radioactive_cloud_point_to_circle(sigmet: Sigmet):
    """Convert a radioactive cloud SIGMET geometry from
       a single point to a circle with radius"""
    if (sigmet.phenomenon == 'RDOACT_CLD' and
            backend_config.radioactive_cloud_point_to_circle and
            has_single_point(sigmet.startGeometry) and
            has_single_point(sigmet.startGeometryIntersect)):
        radius = backend_config.radioactive_cloud_circle_radius_km
        sigmet.startGeometry = point_to_circle(sigmet.startGeometry, radius)
        sigmet.startGeometryIntersect = point_to_circle(
            sigmet.startGeometryIntersect, radius)
    return sigmet


def has_single_point(feature_collection: FeatureCollection):
    """Check if feature collection contains a single feature
       with point geometry"""
    if len(feature_collection.features) != 1:
        return False
    feature = feature_collection.features[0]
    return feature.geometry is not None and feature.geometry.type == "Point"


def point_to_circle(feature_collection: FeatureCollection,
                    radius: int) -> FeatureCollection:
    """Convert a feature collection with a point to a
       feature collection with a circle with radius"""
    if radius < 1:
        raise ValueError("Radius must be a positive integer")
    if len(feature_collection.features) != 1:
        raise ValueError("FeatureCollection must contain exactly one feature")
    feature = feature_collection.features[0]
    if (feature.geometry is None or feature.geometry.type != "Point"):
        raise ValueError("Feature geometry must be a point")

    circle_props = {
        'selectionType': 'circle',
        'radius': radius,
        'radiusUnit': "km"
    }

    if feature.properties is None:
        feature.properties = circle_props
    else:
        feature.properties.update(circle_props)
    return feature_collection
