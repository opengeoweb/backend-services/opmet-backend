from logging.config import dictConfig

from opmet_backend.config import env_config


def configure_logging() -> None:
    log_conf: dict = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "simple": {"format": "[%(levelname)s] %(asctime)s %(name)s: %(message)s"},
        },
        "handlers": {
            "default": {
                "level": env_config.log_level,
                "formatter": "simple",
                "class": "logging.StreamHandler",
                "stream": "ext://sys.stdout",  # Default is stderr
            },
        },
        "loggers": {
            "root": {  # root logger
                "level": env_config.log_level,
                "handlers": ["default"],
                "propagate": False,
            },
            "uvicorn.error": {
                "level": env_config.log_level,
                "handlers": [],
            },
            "uvicorn.access": {
                "level": env_config.log_level,
                "handlers": [],
            },
        },
    }
    dictConfig(log_conf)
