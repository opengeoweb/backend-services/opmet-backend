"""Settings and configuration module. Note that for each model, content from
file overrides defaults, and content from env-variables overrides file.

The configuration is split into settings drawn from the deployment environment,
backend specific settings that mainly deal with formatting of messages and
the SIGMET and AIRMET specific settings that are served to the frontend."""
import json
import os
from abc import ABC, abstractmethod
from datetime import timedelta
from pathlib import Path
from typing import Any

from geojson_pydantic import FeatureCollection
from pydantic import AnyHttpUrl, BaseModel, Field
from pydantic.fields import FieldInfo
from pydantic_settings import (
    BaseSettings,
    PydanticBaseSettingsSource,
    SettingsConfigDict,
)


class JsonConfigSettingsSource(PydanticBaseSettingsSource):
    """
    JSON file settings source
    """

    def __init__(self, settings_cls: type[BaseSettings], json_file_path: str):
        super().__init__(settings_cls)
        self.json_file_path = json_file_path
        self.file_content_json = json.loads(
            Path(self.json_file_path).read_text('utf-8'))

    def __call__(self) -> dict[str, Any]:
        settings_dict: dict[str, Any] = {
            field_name: self.file_content_json.get(field_name)
            for field_name in self.settings_cls.model_fields.keys()
            if self.file_content_json.get(field_name) is not None
        }
        return settings_dict

    def get_field_value(self, _field: FieldInfo, field_name: str) -> Any:
        return self.file_content_json.get(field_name)


class JsonConfigBaseSettings(BaseSettings, ABC):

    @classmethod
    @abstractmethod
    def config_path_env_var(cls) -> str:
        pass

    @classmethod
    @abstractmethod
    def default_config_file_path(cls) -> str:
        pass

    @classmethod
    def settings_customise_sources(
        cls,
        settings_cls: type[BaseSettings],
        init_settings: PydanticBaseSettingsSource,
        env_settings: PydanticBaseSettingsSource,
        dotenv_settings: PydanticBaseSettingsSource,
        file_secret_settings: PydanticBaseSettingsSource,
    ) -> tuple[PydanticBaseSettingsSource, ...]:
        return (
            init_settings,
            JsonConfigSettingsSource(
                settings_cls,
                os.getenv(cls.config_path_env_var(),
                          cls.default_config_file_path())),
            env_settings,
            dotenv_settings,
            file_secret_settings,
        )


class Unit(BaseModel):
    """Model for units for FirArea"""
    unit_type: str
    allowed_units: list[str]


class Phenomenon(BaseModel):
    """Model for phenomenon for FirArea"""
    code: str
    description: str


class SigmetFirArea(BaseModel):
    """Model for FIR definitions used in sigmetConfig.json"""
    fir_name: str
    fir_location: FeatureCollection
    location_indicator_atsr: str = Field(pattern="[A-Z]{4}")
    location_indicator_atsu: str = Field(pattern="[A-Z]{4}")
    area_preset: str
    max_hours_of_validity: int
    hours_before_validity: int
    tc_max_hours_of_validity: int
    tc_hours_before_validity: int
    va_max_hours_of_validity: int
    va_hours_before_validity: int
    adjacent_firs: list[str]
    level_min: dict[str, int]
    level_max: dict[str, int]
    level_rounding_FL: int
    level_rounding_FT: int
    movement_min: dict[str, int]
    movement_max: dict[str, int]
    movement_rounding_kt: int
    units: list[Unit]
    max_polygon_points: int
    phenomenon: list[Phenomenon]


class AirmetFirArea(BaseModel):
    """Model for FIR definitions used in airmetConfig.json"""
    fir_name: str
    fir_location: FeatureCollection
    location_indicator_atsr: str = Field(pattern="[A-Z]{4}")
    location_indicator_atsu: str = Field(pattern="[A-Z]{4}")
    max_hours_of_validity: int
    hours_before_validity: int
    cloud_lower_level_min: dict[str, int]
    cloud_lower_level_max: dict[str, int]
    cloud_level_min: dict[str, int]
    cloud_level_max: dict[str, int]
    cloud_level_rounding_ft: int
    wind_direction_rounding: int
    wind_speed_min: dict[str, int]
    wind_speed_max: dict[str, int]
    visibility_max: int
    visibility_min: int
    visibility_rounding_below: int
    visibility_rounding_above: int
    level_min: dict[str, int]
    level_max: dict[str, int]
    level_rounding_FL: int
    level_rounding_FT: int
    movement_min: dict[str, int]
    movement_max: dict[str, int]
    movement_rounding_kt: int
    units: list[Unit]
    max_polygon_points: int
    phenomenon: list[Phenomenon]


class OpmetConfig(JsonConfigBaseSettings):
    """SIGMET or AIRMET specific settings to be primarily served to frontend and
    secondarily used in backend"""
    location_indicator_mwo: str | None = Field(None, pattern="[A-Z]{4}")
    valid_from_delay_minutes: int | None = None
    default_validity_minutes: int | None = None
    active_firs: list[str] | None = None
    mapPreset: dict | None = None


class SigmetConfig(OpmetConfig):
    """SIGMET specific settings to be primarily served to frontend and
    secondarily used in backend"""
    fir_areas: dict[str, SigmetFirArea] | None = None
    omit_change_va_and_rdoact_cloud: bool = False
    model_config = SettingsConfigDict(title="Sigmet Config",)

    @classmethod
    def config_path_env_var(cls) -> str:
        return 'SIGMET_CONFIG'

    @classmethod
    def default_config_file_path(cls) -> str:
        return 'configuration_files/sigmetConfig.json'


class AirmetConfig(OpmetConfig):
    """AIRMET specific settings to be primarily served to frontend and
    secondarily used in backend"""
    fir_areas: dict[str, AirmetFirArea] | None = None
    model_config = SettingsConfigDict(title="Airmet Config",)

    @classmethod
    def config_path_env_var(cls) -> str:
        return 'AIRMET_CONFIG'

    @classmethod
    def default_config_file_path(cls) -> str:
        return 'configuration_files/airmetConfig.json'


class SigmetHeader(BaseModel):
    """Base model for headers used in publish functionality"""
    va_cld: str
    tc: str  # pylint: disable=invalid-name
    default: str


DEFAULT_SIGMET_SEQUENCE_PREFIXES = {
    "TS": "T",
    "TURB": "U",
    "ICE": "I",
    "ICE (FZRA)": "F",
    "MTW": "M",
    "DS": "D",
    "SS": "S",
    "RDOACT CLD": "R",
    "WS TEST": "Z",
    "VA": "A",
    "WV TEST": "Y",
    "TC": "C",
    "WC TEST": "X"
}

DEFAULT_SIGMET_PHENOMENA = {
    "EMBD_TS": 'TS',
    "EMBD_TSGR": "TS",
    "FRQ_TS": "TS",
    "FRQ_TSGR": "TS",
    "HVY_DS": "DS",
    "HVY_SS": "SS",
    "OBSC_TS": "TS",
    "OBSC_TSGR": "TS",
    "RDOACT_CLD": "RDOACT CLD",
    "SEV_ICE": "ICE",
    "SEV_ICE_FRZ_RN": "ICE (FZRA)",
    "SEV_MTW": "MTW",
    "SEV_TURB": "TURB",
    "SQL_TS": "TS",
    "SQL_TSGR": "TS",
    "TC": "TC",
    "VA_CLD": "VA"
}


class BackendConfig(JsonConfigBaseSettings):
    """Configuration for the backend application itself"""
    model_config = SettingsConfigDict(title="Backend Config",)

    bulletin_header_location_indicator: str | None = None
    sigmet_header: SigmetHeader | None = SigmetHeader(va_cld="",
                                                      tc="",
                                                      default="")
    airmet_header: str | None = None
    iwxxm_sigmet_header: SigmetHeader = SigmetHeader(va_cld="",
                                                     tc="",
                                                     default="")
    iwxxm_airmet_header: str | None = None
    publish_metadata: bool = False
    provider: str = ""
    sigmet_sequence_prefix: str | None = None  # Placeholder for migration.
    airmet_sequence_prefix: str = ""  # Placeholder for AIRMETs.
    sigmet_prefixes: dict[str, str] = DEFAULT_SIGMET_SEQUENCE_PREFIXES
    sigmet_phenomena: dict[str, str] = DEFAULT_SIGMET_PHENOMENA
    default_time_limit: timedelta = timedelta(days=620)
    app_version: str = 'dev'
    avi_version: str = 'dev'
    sigmet_tac_endpoint: str = "/getsigmettac"
    sigmet_iwxxm_endpoint: str = "/getsigmetiwxxm2023_1"
    airmet_tac_endpoint: str = "/getairmettac"
    airmet_iwxxm_endpoint: str = "/getairmetiwxxm2023_1"
    tac_envelope: str | None = None
    tac_line_separator: str = "\n"
    tac_filename_template: str = "{header_designator}{location_indicator}_{valid_start}_{issue_time}{suffix}"
    tac_filename_suffix: str = ".tac"
    iwxxm_filename_suffix: str = ".xml"
    use_abbreviated_gts_heading: bool = False
    exception_on_iwxxm_publish_fail: bool = False
    exception_on_tac_publish_fail: bool = True
    cnl_sigmet_original_type: bool = True
    cnl_airmet_original_type: bool = True
    http_client_timeout_seconds: float = 5.0
    healthcheck_service_timeout_seconds: float = 5.0
    radioactive_cloud_point_to_circle: bool = False
    radioactive_cloud_circle_radius_km: int = 30

    @classmethod
    def config_path_env_var(cls) -> str:
        return 'BACKEND_CONFIG'

    @classmethod
    def default_config_file_path(cls) -> str:
        return 'configuration_files/backendConfig.json'


class EnvConfig(BaseSettings):
    """Base model for opmet-backend configuration. Primarily defined using ENV
    variables"""
    model_config = SettingsConfigDict(env_nested_delimiter="__",
                                      env_file=".env",
                                      extra="ignore",
                                      title="Env Config")
    sqlalchemy_database_url: str
    messageconverter_url: AnyHttpUrl
    publisher_url: AnyHttpUrl
    # this is not mandatory to set, so default to the empty string
    application_root_path: str = ""
    debug: bool = False
    log_level: str = "ERROR"


env_config = EnvConfig()
backend_config = BackendConfig()
airmet_config = AirmetConfig()
sigmet_config = SigmetConfig()
