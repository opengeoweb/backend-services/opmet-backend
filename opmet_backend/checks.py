import logging

import httpx
from sqlalchemy.orm import Session

import opmet_backend.database as db
from opmet_backend.config import backend_config, env_config
from opmet_backend.utils import HTTPXClientWrapper

logger = logging.getLogger(__name__)


def database(session: Session, message: dict) -> None:
    """Checks the database connection

    Args:
        session: database session
        message: status-message to update the database status
    """
    try:
        sigmet_rows = db.count_rows(session, db.SigmetDbItem)
        if sigmet_rows >= 0:
            message["database"]["sigmet-table"] = {"status": "OK"}
            message["database"].update({"status": "OK"})
        else:
            message["database"]["sigmet-table"] = {"status": "FAILED"}
            message["database"].update({"status": "FAILED"})
    except Exception as e:
        message["database"].update({"status": "FAILED"})
        message["status"] = "FAILED"
        logger.error(e)


async def check_service(message: dict, url: str, service_name: str) -> None:
    """Checks if the service is available by sending GET requests to specified url

    The service health is determined by http status code.

    Args:
        message (dict): status-message to put the availability information
        url (str): healthcheck url to send the request to
        service_name (str): service name to be used in the status message
    """
    try:
        client = await HTTPXClientWrapper.get_httpx_client()
        response = await client.get(
            url, timeout=backend_config.healthcheck_service_timeout_seconds)
        if response.status_code == httpx.codes.OK:
            message[service_name] = {"status": "OK"}
        else:
            message[service_name] = {"status": "FAILED"}
            message["status"] = "FAILED"
    except Exception as e:
        logger.error(e)
        message[service_name] = {"status": "FAILED"}
        message["status"] = "FAILED"


async def message_converter(message: dict) -> None:
    await check_service(message,
                        f"{env_config.messageconverter_url}/healthcheck",
                        "messageconverter")


async def publisher(message: dict) -> None:
    publisher_root_url = str(env_config.publisher_url).rstrip("/publish")
    await check_service(message, f"{publisher_root_url}/healthcheck",
                        "publisher")
