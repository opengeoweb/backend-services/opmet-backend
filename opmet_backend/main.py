"""Main module for defining the opmet-backend application."""
import logging
from asyncio import sleep
from contextlib import asynccontextmanager
from typing import Any

from alembic import command
from alembic.config import Config
from fastapi import Depends, FastAPI, Request, status
from fastapi.exceptions import HTTPException, RequestValidationError
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse, PlainTextResponse
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import Session

import opmet_backend.database as db
from opmet_backend import checks
from opmet_backend.config import (
    airmet_config,
    backend_config,
    env_config,
    sigmet_config,
)
from opmet_backend.logger import configure_logging
from opmet_backend.routers import airmet, sigmet
from opmet_backend.utils import HTTPXClientWrapper

configure_logging()

logger = logging.getLogger(__name__)

DebugToolbarMiddleware: Any | None = None

if env_config.debug:
    try:
        from debug_toolbar.middleware import DebugToolbarMiddleware
    except ImportError as e:
        logger.error(e)


@asynccontextmanager
async def lifespan(app: FastAPI):
    """Runs alembic migrations"""
    # startup
    alembic_cfg = Config("alembic.ini")
    command.upgrade(alembic_cfg, "head")
    await HTTPXClientWrapper.get_httpx_client()
    yield
    # Clean up client
    await HTTPXClientWrapper.close_httpx_client()


app = FastAPI(title="GeoWeb OPMET Backend",
              version=backend_config.app_version,
              description="GeoWeb SIGMET and AIRMET Backend API",
              root_path=env_config.application_root_path,
              docs_url="/api",
              debug=env_config.debug,
              lifespan=lifespan)

logger = logging.getLogger(__name__)
if env_config.debug and DebugToolbarMiddleware:
    app.add_middleware(
        DebugToolbarMiddleware,
        panels=["debug_toolbar.panels.sqlalchemy.SQLAlchemyPanel"],
        settings=[airmet_config, sigmet_config, env_config, backend_config])
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_methods=['*'],
    allow_headers=['*'],
)

# expose_headers=['*']?
# allow_credentials=True?
# Could also handle CORS in nginx


@app.middleware("http")
async def add_headers(request: Request, call_next):
    """Add security headers"""
    response = await call_next(request)
    response.headers['X-Content-Type-Options'] = "nosniff"
    response.headers[
        'Strict-Transport-Security'] = "max-age=31536000; includeSubDomains"
    return response


@app.exception_handler(HTTPException)
async def http_exception_handler(_: Request, exc: HTTPException):
    """Converts a HTTPException to a JSON response for frontend"""

    return JSONResponse({'message': exc.detail},
                        status_code=exc.status_code,
                        headers=exc.headers)


@app.exception_handler(RequestValidationError)
async def request_validation_error_handler(_: Request,
                                           exc: RequestValidationError):
    """Converts a HTTPException to a JSON response for frontend"""

    return JSONResponse({'message': exc.errors()}, status_code=400)


@app.exception_handler(SQLAlchemyError)
async def sqlalchemy_error_handler(_: Request, exc: SQLAlchemyError):
    """Converts a database error to a JSON response for frontend"""
    return JSONResponse({'message': str(exc)},
                        status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)


if sigmet_config:
    app.include_router(sigmet.router)
else:
    logger.warning("sigmetConfig.json not deployed")
if airmet_config:
    app.include_router(airmet.router)
else:
    logger.warning("airmetConfig.json not deployed")


@app.get('/healthcheck')
async def healthcheck(session: Session = Depends(db.get_db)):
    """Health check that can be called to see if service is accessible.

    Returns:
        JSON containing the healthcheck status

    Example:
        Successful response::

        {
            "status": "OK",
            "service": "Opmet",
            "database": {
                "status": "OK",
                "sigmet-table": {
                    "status": "OK"
                }
            },
            "messageconverter": {
                "status": "OK"
            },
            "publisher": {
                "status": "OK"
            }
        }

    """
    message: dict = {
        'status': 'OK',
        'service': 'Opmet',
        "database": {
            "status": "UNKNOWN",
            "sigmet-table": {
                "status": "UNKNOWN"
            }
        },
        "messageconverter": {
            "status": "UNKNOWN"
        },
        "publisher": {
            "status": "UNKNOWN"
        }
    }
    await checks.message_converter(message)
    await checks.publisher(message)
    checks.database(session, message)
    if message["status"] == "FAILED":
        logger.error(f"Healthcheck failed: {message}")
        raise HTTPException(503, detail=message)
    return message


@app.get("/", response_class=PlainTextResponse)
async def hello():
    """Returns a welcome message."""
    return "GeoWeb OPMET backend"


@app.get("/version", response_class=JSONResponse)
async def version():
    """Returns version that ultimately depends on VERSION env variable of
    deployment environment."""
    return {'version': backend_config.app_version}


@app.get("/aviversion", response_class=JSONResponse)
async def avi_version():
    """Returns version that ultimately depends on AVI_VERSION env variable of
    deployment environment."""
    return {'version': backend_config.avi_version}
