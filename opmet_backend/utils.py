"""Utils module"""
from __future__ import annotations

import httpx
from furl import furl
from pydantic import AnyHttpUrl

from opmet_backend.config import backend_config


def strip_sequence(string: str) -> int:
    """Strips string from field value and returns only numeric part"""
    return int(''.join(c for c in string if c.isdigit()))


class HTTPXClientWrapper(httpx.AsyncClient):
    """Singleton HTTPX client with retries and timeout"""
    _instance: HTTPXClientWrapper | None = None  # singleton

    def __init__(self):
        timeout = httpx.Timeout(backend_config.http_client_timeout_seconds)
        transport = httpx.AsyncHTTPTransport(retries=3)
        super().__init__(transport=transport, timeout=timeout)

    @classmethod
    async def get_httpx_client(cls):
        """
        Returns the singleton HTTPXClientWrapper
        """
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance

    @classmethod
    async def close_httpx_client(cls):
        """
        Closes the shared HTTPX client instance.
        """
        if cls._instance is not None:
            await cls._instance.aclose()
            cls._instance = None


def build_url(base_url: AnyHttpUrl, endpoint: str) -> httpx.URL:
    """Builds URL from base URL and endpoint. Handles leading and trailing slashes"""
    url_builder = furl(str(base_url)).add(path=endpoint)
    url_builder.path.normalize()
    return httpx.URL(url_builder.url)
