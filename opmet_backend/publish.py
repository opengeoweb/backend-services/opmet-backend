"""Module for for publishing SIGMET and AIRMET in TAC and IWXXM formats"""

import logging
from collections import defaultdict
from datetime import datetime, timedelta

import httpx
from fastapi import status
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import HTTPException
from sqlalchemy.orm import Session

from opmet_backend.config import backend_config
from opmet_backend.models.airmet import Airmet, CancelAirmet
from opmet_backend.models.shared import PublishMetaData
from opmet_backend.models.sigmet import CancelSigmet, ShortTestSigmet, Sigmet
from opmet_backend.utils import HTTPXClientWrapper

logger = logging.getLogger(__name__)

Publishable = CancelSigmet | CancelAirmet | Sigmet | Airmet | ShortTestSigmet | Sigmet


async def translate_opmet(client: httpx.AsyncClient, opmet: Publishable,
                          url: httpx.URL) -> httpx.Response:
    """Helper function to call converter api and handle its responses."""
    try:
        tacified_opmet = await client.post(url,
                                           json=jsonable_encoder(
                                               opmet,
                                               exclude_none=True,
                                               exclude_unset=True))
        tacified_opmet.raise_for_status()
    except httpx.HTTPStatusError as exc:
        logging.warning("Error from messageconverter: %s", exc.response)
        raise HTTPException(
            status_code=exc.response.status_code,
            detail=f"Error from messageconverter: {exc.response.text}",
        ) from exc
    except httpx.RequestError as exc:
        logging.warning("Error from messageconverter: %s", exc)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=f"Error from messageconverter: {exc}",
        ) from exc
    return tacified_opmet


def set_issue_date_and_sequence(session: Session, now: datetime,
                                opmet: Publishable) -> Publishable:
    """ "Set issue date and sequence number for an opmet message"""
    opmet.issueDate = now
    previous = opmet.get_previous(
        session,
        time_limit=timedelta(
            hours=now.hour,
            minutes=now.minute - 1,
            seconds=now.second,
            microseconds=now.microsecond,
        ),
    )
    opmet.sequence = opmet.next_in_sequence(previous)
    return opmet


def split_into_lines(tac: str) -> str:
    """Function taken from KNMI backend utils to split tac message into lines
    of max 69 characters"""
    maxlen = 69
    input_lines = tac.splitlines()
    output_lines = []
    for input_line in input_lines:
        if len(input_line) <= maxlen:
            output_lines.append(input_line)
        else:
            output_line = ""
            for word in input_line.split():
                if len(output_line) + len(word) + 1 <= maxlen:
                    if len(output_line) > 0:
                        output_line += " " + word
                    else:
                        output_line = word
                else:
                    output_lines.append(output_line)
                    output_line = word
            if len(output_line) > 0:
                output_lines.append(output_line)
    return backend_config.tac_line_separator.join(output_lines)


def get_bulletin_header_location_indicator(publishable: Publishable) -> str:
    """Function to get the bulletin header location indicator"""
    if backend_config.bulletin_header_location_indicator is None:
        return publishable.locationIndicatorMWO
    return backend_config.bulletin_header_location_indicator


def format_tac(opmet: Publishable, tac: str, now: datetime):
    """Function to format a tac message with corresponding Sigmet or Airmet for
    publishing"""
    header_designator = opmet.get_data_designator()
    location_indicator = get_bulletin_header_location_indicator(opmet)
    bulletin_header = (
        f'{header_designator} {location_indicator} {now.strftime("%d%H%M")}\n')
    tac_content = split_into_lines(wrap_tac_in_envelope(bulletin_header + tac))
    tac_filename = format_tac_filename(header_designator, location_indicator,
                                       opmet.validDateStart, now)

    return tac_filename, tac_content


def tac_meta(opmet: Publishable, tac: str, now: datetime):
    """Function to collect tac meta data for Sigmet or
    Airmet for publishing"""
    header_designator = opmet.get_data_designator()
    location_indicator = get_bulletin_header_location_indicator(opmet)
    bulletin_header = (
        f'{header_designator} {location_indicator} {now.strftime("%d%H%M")}\n')
    tac_content = split_into_lines(wrap_tac_in_envelope(bulletin_header + tac))

    tac_dt_format = "%Y-%m-%dT%H:%M:%SZ"

    tac_content_meta = PublishMetaData(
        data=tac_content,
        format="tac",
        location_indicator_ATSU=opmet.locationIndicatorATSU,
        issued_at=now.strftime(tac_dt_format),
        valid_from=opmet.validDateStart.strftime(tac_dt_format),
        valid_until=opmet.validDateEnd.strftime(tac_dt_format),
        header=header_designator,
        provider=backend_config.provider,
        product=str(opmet.__class__.__name__),
    )

    return tac_content_meta


def wrap_tac_in_envelope(tac: str) -> str:
    """Function to wrap a tac message in an envelope if one is configured"""
    if backend_config.tac_envelope:
        header, footer = backend_config.tac_envelope.split("/")
        tac = tac.rstrip("\n")
        return f"{header}\n{tac}\n{footer}\n"
    return tac


def format_tac_filename(
    header_designator: str,
    location_indicator: str,
    valid_start: datetime,
    now: datetime,
):
    """Function to format the filename for a TAC message"""
    return backend_config.tac_filename_template.format_map(
        defaultdict(
            str,
            header_designator=header_designator,
            location_indicator=location_indicator,
            valid_start=valid_start.strftime("%Y-%m-%dT%H%M"),
            issue_time=now.strftime("%Y%m%d%H%M%S"),
            suffix=backend_config.tac_filename_suffix,
        ))


def format_iwxxm(opmet: Publishable, iwxxm: str, now: datetime):
    """Function to format an IWXXM message with corresponding Sigmet or
    Airmet for publishing"""
    iwxxm_header = opmet.get_iwxxm_header()
    location_indicator = get_bulletin_header_location_indicator(opmet)

    iwxxm_content = iwxxm.rstrip("\n")
    if backend_config.use_abbreviated_gts_heading:
        iwxxm_content = f"{iwxxm_header} {location_indicator} {now.strftime('%d%H%M')}\n{iwxxm_content}"

    publish_date = opmet.validDateStart.strftime("%Y%m%d%H%M%S")
    bulletin_date = opmet.validDateStart.strftime("%d%H%M")

    filename_bulletin_header = f"A_{iwxxm_header}{location_indicator}{bulletin_date}"
    iwxxm_filename = (f"{filename_bulletin_header}_C_{location_indicator}"
                      f"_{publish_date}{backend_config.iwxxm_filename_suffix}")

    return iwxxm_filename, iwxxm_content


def iwxxm_meta(opmet: Publishable, iwxxm: str, now: datetime):
    """Function to collect IWXXM meta data for Sigmet or
    Airmet for publishing"""
    iwxxm_header = opmet.get_iwxxm_header()
    location_indicator = get_bulletin_header_location_indicator(opmet)

    iwxxm_content = iwxxm.rstrip("\n")
    if backend_config.use_abbreviated_gts_heading:
        iwxxm_content = f"{iwxxm_header} {location_indicator} {now.strftime('%d%H%M')}\n{iwxxm_content}"

    iwxxm_dt_format = "%Y-%m-%dT%H:%M:%SZ"

    iwxxm_content_meta = PublishMetaData(
        data=iwxxm_content,
        format="iwxxm",
        location_indicator_ATSU=opmet.locationIndicatorATSU,
        issued_at=now.strftime(iwxxm_dt_format),
        valid_from=opmet.validDateStart.strftime(iwxxm_dt_format),
        valid_until=opmet.validDateEnd.strftime(iwxxm_dt_format),
        header=iwxxm_header,
        provider=backend_config.provider,
        product=str(opmet.__class__.__name__),
    )

    return iwxxm_content_meta
