"""Module for interacting with the database using a declarative format.
Consider splitting into database.py, models.py and crud.py files."""
import logging
import uuid
from datetime import datetime
from typing import Type

from dateutil import tz
from fastapi.encoders import jsonable_encoder
from sqlalchemy import Column, DateTime, String, create_engine
from sqlalchemy.dialects.postgresql import JSON, UUID
from sqlalchemy.orm import Session, declarative_base, sessionmaker

from opmet_backend.config import env_config

logger = logging.getLogger(__name__)

engine = create_engine(str(env_config.sqlalchemy_database_url),
                       pool_recycle=120,
                       pool_pre_ping=True)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


def get_db():
    """Gets us the DB session for all the calls that read or write to DB.
    This is the bit we may want to override for testing to for example include
    rollbacks."""
    session = SessionLocal()
    try:
        yield session
    finally:
        session.close()


class DbItem(Base):  # type: ignore
    """Common base structure shared by both AIRMET and SIGMET. Note that we are
    using backend-side datetime.now to populate autofilled timestamps instead
    of database-side functions.now() simply to make the system easier to test
    with mocked timestamps. Otherwise functions.now() would be preferable."""
    __abstract__ = True
    uuid = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid1)
    lastUpdateDate = Column(DateTime)
    creationDate = Column(
        DateTime,
        index=True,
        default=datetime.now(tz=tz.UTC).replace(microsecond=0))
    author = Column(String, default=None)


class SigmetDbItem(DbItem):
    """Sigmet table."""
    __tablename__ = "sigmetitems"
    sigmet = Column(JSON)


class AirmetDbItem(DbItem):
    """Airmet table."""
    __tablename__ = "airmetitems"
    airmet = Column(JSON)


def get_model_by_uuid(db: Session, met_uuid, model: Type[DbItem]):
    """Shared function for getting items by UUID."""
    return db.query(model).filter(model.uuid == met_uuid).first()


def get_sigmet_by_uuid(db: Session, met_uuid):
    """Function for getting SIGMET by UUID"""
    return get_model_by_uuid(db, met_uuid, SigmetDbItem)


def get_airmet_by_uuid(db: Session, met_uuid):
    """Function for getting AIRMET by UUID"""
    return get_model_by_uuid(db, met_uuid, AirmetDbItem)


def count_rows(db: Session, model: Type[DbItem]) -> int:
    """Counts the number of rows in certain model table"""
    rows = db.query(model).count()
    return rows


def create_sigmet(item, db: Session):
    """Function to create a Sigmet in database. We are manually creating the
    UUID so it can be easily updated to both uuid column and the uuid field of
    the JSON data structure."""
    iidee = uuid.uuid1()
    item.uuid = iidee
    db_opmet = SigmetDbItem(
        uuid=iidee,
        lastUpdateDate=datetime.now(tz=tz.UTC).replace(microsecond=0),
        creationDate=datetime.now(tz=tz.UTC).replace(microsecond=0),
        sigmet=jsonable_encoder(item, exclude_unset=True, exclude_none=True))
    db.add(db_opmet)
    db.commit()
    db.refresh(db_opmet)
    return db_opmet


def create_airmet(item, db: Session):
    """Function to create an Airmet in database."""
    iidee = uuid.uuid1()
    item.uuid = iidee
    db_opmet = AirmetDbItem(
        uuid=iidee,
        lastUpdateDate=datetime.now(tz=tz.UTC).replace(microsecond=0),
        creationDate=datetime.now(tz=tz.UTC).replace(microsecond=0),
        airmet=jsonable_encoder(item, exclude_unset=True, exclude_none=True))
    db.add(db_opmet)
    db.commit()
    db.refresh(db_opmet)
    return db_opmet
