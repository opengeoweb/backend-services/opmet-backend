# Opmet Backend

This repository contains code to develop, run and deploy the Opmet backend.
This backend can be used to store and load SIGMET and AIRMET messages for a GeoWeb (frontend) application.

The Opmet backend consists of different components and services working together:
- In the [nginx](/nginx/) directory you can find the Nginx reversy proxy configuration file used with the [auth-backend](https://gitlab.com/opengeoweb/backend-services/auth-backend) Nginx code for load balancing and authentication
- In the [opmet_backend](/opmet_backend/) directory you can find the FastAPI code for the main application
- In the [publisher_local](/publisher_local/) directory you can find the code for a development publisher that saves output files to your machine. You can configure a publisher for a development environment from the [publisher-backend](https://gitlab.com/opengeoweb/backend-services/publisher-backend) repository.
- In the directory [configuration_files](/configuration_files/) you can find the configuration files used to customize the backend
- In the [test](/tests/) directory, the automated tests for the opmet-backend can be found.

Besides the components found in this repository, the opmet-backend also uses an [avi-messageconverter](https://gitlab.com/opengeoweb/avi-msgconverter) to generate aviation products. An overview of all components of the opmet-backend is presented below:

```mermaid
flowchart TD

id1{{Frontend application}}:::setstroke<-->|Requests from and reponses to FE|id2[NGINX]
id2:::beclass<-->|Validated response to FE. \n\n Authenticated and authorized request \n to the main Fastapi BE container.|id3[Opmet FastAPI BE]
id3:::beclass<-->|Retrieve and update \n SIGMET and AIRMET data|id4[(Opmet Database)]:::beclass
id3-->|TAC and IWXXM products \n for publishing|id5[Opmet Publisher]
id3<-->|Generate TAC and \n IWXXM products|id6[AVI Messageconverter]:::beclass
id5:::beclass-->|Save output products \n in configured location|id8[/File system/]:::setstroke


classDef setstroke stroke-width:3px

classDef beclass fill:#bbe3a4,stroke:#597549,stroke-width:3px

```


## Table of Contents

[[_TOC_]]

## Running the application

The opmet backend can be run with Docker.

### Develop with Docker

To run opmet-backend, its database and messageconverter in containers

First, the .env file should be created.
```
cat <<EOF > .env
SQLALCHEMY_DATABASE_URL=postgresql://geoweb:geoweb@db/opmetbackenddb
MESSAGECONVERTER_URL=http://messageservices:8080
PUBLISHER_URL=http://publisher:8090/publish

ENABLE_SSL=TRUE
ENABLE_DEBUG_LOG=TRUE
ENABLE_ACCESS_LOG=FALSE
BACKEND_PORT=4443
BACKEND_PORT_HTTP=80
OPMET_PORT_HTTP=8000
OAUTH2_USERINFO=https://gitlab.com/oauth/userinfo
GEOWEB_USERNAME_CLAIM="email"
# GEOWEB_REQUIRE_READ_PERMISSION='groups=opengeoweb/internal'
# GEOWEB_REQUIRE_WRITE_PERMISSION='groups=opengeoweb/internal'
EOF
```

run containers:

```
docker compose --file=docker-compose-dev.yml up
```
The Postgres database in this development docker-compose file exports port 5432 to the host machine (your laptop). This port number could clash with a Postgres server already running on your machine. The port export is only necessary if you want to look in the database with `psql`, for example. So the export can easily be left out or changed to for example 5433:5432 (making Postgres available on port 5433 on your host; it will remain reachable through port 5432 inside the Docker network).

### Running tests

Running the tests is easiest to do by running `sh tests.sh` which runs a script that brings up the environment specified in `docker-compose-tests.yml`.

### Local installation and development

Running and developing the backend locally as a Python FastAPI application requires Python version 3.10 or newer. Note that for most of the functionality you will also need a database service, messageservice and publish service running. It is generally recommended to simply use the docker compose instead.

Recommended way to install Python is by using Pyenv, Pipx and Poetry, described below.

#### 1. Install Pyenv with Pyenv installer

Check the [prerequisites](https://github.com/pyenv/pyenv/wiki#suggested-build-environment) from the pyenv wiki. for Debian Linux they are:

```bash
sudo apt update; sudo apt install build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev curl libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev
```

Run the Pyenv installer script:

```bash
curl https://pyenv.run | bash
```

Setup your shell environment with these [instructions](https://github.com/pyenv/pyenv#set-up-your-shell-environment-for-pyenv)

#### 2. Build and install Python

With Pyenv installed, check the available Python versions with

```bash
pyenv install -l
```

Install the latest Python 3.11 version from the list and set it as global Python version for your system.

```bash
pyenv install 3.11.8
pyenv global 3.11.8
```

#### 3. Install Pipx

Pipx is a helper utility for installing system wide Python packages, adding them to your path automatically.

Install pipx with these [instructions](https://pipx.pypa.io/latest/installation/). Linux users can install it with pip:

```bash
python3 -m pip install --user pipx
python3 -m pipx ensurepath

# Optional
sudo pipx ensurepath --global
```

#### 4. Install Poetry

Poetry is used for managing project dependencies and metadata. Install Poetry with

```bash
pipx install poetry
```

and add poetry export plugin to it

```bash
pipx inject poetry poetry-plugin-export
```

#### 5. Activate virtualenv and install dependencies

Activate the virtualenv with poetry

```bash
poetry shell
```

And install project dependencies

```bash
poetry install
```

#### Setting up the database

Before starting the backend, you must make sure that a Postgres database is available for the opmet-backend to use. This could be a system Postgres database or a database inside a docker container. However the databases hostname and port number have to be specified in the [`.env`](.env) file.

For example, if you want to use the system Postgres database, you should follow these two steps:
1. Set up a postgres database for the opmet backend:
```
sudo -u postgres createuser --superuser geoweb
sudo -u postgres psql postgres -c "ALTER USER geoweb PASSWORD 'geoweb';"
sudo -u postgres psql postgres -c "CREATE DATABASE opmetbackenddb;"
echo "\q" | psql "dbname=opmetbackenddb user=geoweb password=geoweb host=localhost"
```

2. Configure the `SQLALCHEMY_DATABASE_URL` in your [`.env`](.env) file with:
```
SQLALCHEMY_DATABASE_URL=postgresql://geoweb:geoweb@localhost/opmetbackenddb
```

Alternatively, run a Dockerized Postgres database (while running Uvicorn locally):

```
docker run --rm \
    --name opmet-backend-db \
    -e POSTGRES_USER=geoweb \
    -e POSTGRES_PASSWORD=geoweb \
    -e POSTGRES_DB=opmetbackenddb \
    -p 5432:5432 \
    postgres:13.4
```
Don't forget to update the [`.env`](.env) file for the database with:

```
SQLALCHEMY_DATABASE_URL=postgresql://geoweb:geoweb@localhost/opmetbackenddb
```

Also note that if you have a local database running, you might have to change the port number to run a Dockerized Postgress database, for example with: `-p 5433:5432`. You should still be able to access the database with the example URL above in your [`.env`](.env) file.

### Managing Python dependencies

With Poetry installed from previous step, you can add or update Python dependencies with poetry add command.

```bash
# adding dependency
poetry add fastapi

# updating dependency
poetry update fastapi
```

You can add development dependencies with --group keyword.

```bash
# adding development dependency
poetry add --group dev isort
```

All poetry operations respect version constraints set in pyproject.toml file.

##### Fully updating dependencies

You can see a list of outdated dependencies by running

```bash
poetry show --outdated
```

To update all dependencies, still respecting the version constraints on pyproject.toml, run

```bash
poetry update
```

### Dependabot integration

This repository has a Dependabot integration with the following behavior specified in the `.gitlab/dependabot.yml` file:

| definition               | behavior                                                                                                                                                                                                                                                                   |
|--------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| schedule block           | Scan the repository daily at random time between 6-11. Add any pending package update as MR and update any MR with merge conflicts by doing a rebase. If there is a newer release of the dependency that already has an open MR, make a new MR and close the previous one. |
| open-pull-requests-limit | Don't open more than 3 MRs per directory, even if there are more package updates available than 3.                                                                                                                                                                         |
| rebase-strategy          | Always rebase open MRs, if there are new merged changes on the main branch, even if there are no merge conflicts present.                                                                                                                                                  |

More detailed guide on how to work with dependabot can be found in the [opengeoweb wiki](https://gitlab.com/groups/opengeoweb/-/wikis/How-to-work-with-Dependabot-MRs)


### Using pre-commit

You can also use the pre-commit configuration. This is used to inspect the code that's about to be committed, to see if you've forgotten something, to make sure tests run, or to examine whatever you need to inspect in the code. To use pre-commit make sure the `pip install` command has succesfully ran with the `requirements-dev.txt` argument.
Then execute the following command in the terminal in the root folder:
 ```
 pre-commit install
 ```
 This will install the pre-commit functions and now pre-commit will run automatically on each `git commit`.
 Only changed files are taken into consideration.

 Optionally you can check all the files with the pre-commit checks (NOTE: this command can make changes to files):

```
pre-commit run --all-files
```

If you wish to skip the pre-commit test but do have pre-commit setup, you can use the `--no-verify` flag in git to skip the pre-commit checks. For example:
```
git commit -m "Message" --no-verify
```


### Start the Python FastAPI application

Run with Uvicorn
`uvicorn --host 0.0.0.0 --port=8000 --workers=1 --access-log --log-config opmet_logging.conf --reload opmet_backend.main:app`

Run Gunicorn with Uvicorn Workers
`gunicorn --bind 0.0.0.0:8000 --timeout 600 -k uvicorn.workers.UvicornWorker --error-logfile - --access-logfile - --log-config opmet_logging.conf opmet_backend.main:app`

### Use the debug toolbar

Debug toolbar shows useful information about requests, settings and environment, read more from [debug-toolbar docs](https://fastapi-debug-toolbar.domake.io/). You can enable debug toolbar in the /api endpoint by setting the DEBUG environmental variable to true in .env file:

```
DEBUG=TRUE
```

If you use docker-compose as a dev environment, you might have to do build command to enable the toolbar.

```bash
docker compose --file=docker-compose-dev.yml build
```

## Deployment

The standard 'docker-compose.yml' file contains a deployment that includes a similar nxginx container as found with other backend services as a proxy that takes care of user authentication.

## Configuration

The way configuration variables are read is defined in `config.py`, which can be used to see available values, if not found in sample configuration files.

Configuration related to the deployment environment should be given as ENV values either in a `.env` file or as values defined in the run environment. Manually given values will override values set in files. The backend itself expects
* SQLALCHEMY_DATABASE_URL to know where to find the database
* MESSAGECONVERTER_URL to know where to find messageconverter
* PUBLISHER_URL to know where to send TAC and IWXXM messages for publication

Configuration for the backend's internal settings, mainly used for variables needed to publish TAC and IWXXM messages, can be given in `configuration_files/backendConfig.json` or by creating a new configuration file and using `BACKEND_CONFIG` env variable to its path. See [Backend configuration options](#backend-configuration-options) for details.

Settings needed by the frontend should be given in `configuration_files/sigmetConfig.json` and `configuration_files/airmetConfig.json` or by creating new configuration files and using `SIGMET_CONFIG` and `AIRMET_CONFIG` env variables to their paths. See [Airmet configuration options](#airmet-configuration-options) and [Sigmet configuration options](#sigmet-configuration-options) for details.

The values in each configuration file can be overridden by using a corresponding ENV variable. The easiest way to include them when running optmet-backend in a container is to mount them into the `/app/configuration_files` directory as seen in the provided docker-compose files.

All three JSON files or equivalent ENV variables are currently expected by the backend and it will not run without them. This should be changed later to make it easier to deploy the backend only with SIGMET or only with AIRMET functionality.

### Backend configuration options

The following options are configurable in [`configuration_files/backendConfig.json`](./configuration_files/backendConfig.json). See `BackendConfig` class in [`config.py`](/opmet_backend/config.py) for the default values.

- `bulletin_header_location_indicator`: Optionally override the location indicator used in bulletin header. If this option is not specified, the meteorological watch office (MWO) from the message is used instead. Applies to both TAC and IWXXM.
- `sigmet_header`: TAC bulletin header data designator in SIGMET messages. Subkeys `va_cld` for volcanic ash cloud, `tc` for tropical cyclone and `default` for other phenomena.
- `airmet_header`: TAC bulletin header data designator in AIRMET messages.
- `iwxxm_sigmet_header`: IWXXM bulletin header data designator in SIGMET messages. Subkeys `va_cld` for volcanic ash cloud, `tc` for tropical cyclone and `default` for other phenomena.
- `iwxxm_airmet_header`: IWXXM bulletin header data designator in AIRMET messages.
- `sigmet_sequence_prefix`  See [Sequence Numbering](#sequence-numbering).
- `airmet_sequence_prefix`: See [Sequence Numbering](#sequence-numbering).
- `sigmet_prefixes`: See [Sequence Numbering](#sequence-numbering).
- `sigmet_phenomena`: All of the possible SIGMET phenomena and their code values. See [Sequence Numbering](#sequence-numbering).
- `default_time_limit`: Time delta to specify the maximum age of messages included in the SIGMET/AIRMET list. Note that there's also a limit of items fetched that is hardcoded to 15.
- `app_version`: Application version shown in the /version endpoint. Generally set in a CI/CD pipeline or by the runtime environment.
- `sigmet_tac_endpoint`: Converter service endpoint to fetch a SIGMET in TAC format.
- `sigmet_iwxxm_endpoint`: Converter service endpoint to fetch a SIGMET in IWXXM format.
- `airmet_tac_endpoint`: Converter service endpoint to fetch an AIRMET in TAC format.
- `airmet_iwxxm_endpoint`: Converter service endpoint to fetch an AIRMET in IWXXM format.
- `tac_envelope`: Optional control characters to envelope a TAC bulletin in, separated by slash. For example `ZCZC/NNNN` where `ZCZC` is be used to indicate the start of message and `NNNN` the end of message. If this option is not specified, TAC bulletins will not be enveloped.
- `tac_line_separator`: Line separator used in TAC bulletins. Commonly `\r\n` or `\n`.
- `tac_filename_template`: Filename template for TAC messages. The possible keys to use in the template are `header_designator`, `location_indicator`, `valid_start`, `issue_time` and `suffix`. For example `{header_designator}-{location_indicator}{suffix}`.
- `tac_filename_suffix`: Suffix used in TAC filenames.
- `iwxxm_filename_suffix`: Suffix used in IWXXM filenames.
- `use_abbreviated_gts_heading`: If `True`, adds an abbreviated GTS heading as the first line in IWXXM messages. The XML content starts from the second line.
- `exception_on_iwxxm_publish_fail`: Whether to throw an exception on IWXXM publication failure. If `True`, an error response will be returned. If `False`, the error will only be logged.
- `exception_on_tac_publish_fail`: Whether to throw an exception on TAC publication failure. If `True`, an error response will be returned. If `False`, the error will only be logged.
- `cnl_sigmet_original_type`: Whether to use the cancelled SIGMET's type in the cancel SIGMET. If `False`, cancel SIGMETs will always be of `NORMAL` type.
- `cnl_airmet_original_type`: Whether to use the cancelled AIRMET's type in the cancel AIRMET. If `False`, cancel AIRMETs will always be of `NORMAL` type.
- `http_client_timeout_seconds`: Timeout when calling the converter and publisher services.
- `healthcheck_service_timeout_seconds`: Timeout when the healthcheck calls other services over HTTP.
- `radioactive_cloud_point_to_circle`: Whether to convert point geometries to circles with radius when radioactive cloud phenomenon is selected for a SIGMET.
- `radioactive_cloud_circle_radius_km`: Circle radius in kilometers when `radioactive_cloud_point_to_circle` is set to `True`. Does nothing when `radioactive_cloud_point_to_circle` is `False`.
- `publish_metadata`: If `True` metadata will be published with the product. If `False`, no metadata will be published.
- `provider`: Optional meta information about the provider of the data. For example, `"MET"` or `"KNMI"` or `"FMI"`, etc

### Airmet configuration options
The following options are configurable in [`configuration_files/airmetConfig.json`](./configuration_files/airmetConfig.json). Also see the `AirmetConfig` class in [`config.py`](/opmet_backend/config.py).

- `location_indicator_mwo`: MWO location indicator code
- `fir_areas`: An object with one or more FIR configurations. The subkey is the FIR location code. For more details, see [fir_areas](#fir_areas)
- `active_firs`: list of FIRs as specified in `fir_areas`
- `mapPreset`: An object specifying which map data is shown in the Airmet-form. For more information, see [mapPreset](#mappreset).
- `valid_from_delay_minutes`: Integer that specifies the start time of an Airmet when opening the form as current time + `valid_from_delay_minutes`.
- `default_validity_minutes`: Integer that specifies how long the Airmet is valid when opening the form as start time + `default_validity_minutes`.

#### fir_areas
The following options are configurable in the `fir_areas` field of the `AirmetConfig` class. Also see the `AirmetFirArea` class in [`config.py`](/opmet_backend/config.py).

- `fir_name`: Name of the FIR
- `fir_location`: FeatureCollection specification of FIR boundaries
- `location_indicator_atsr`: ATSR FIR location code
- `location_indicator_atsu`: ATSU FIR location code
- `max_hours_of_validity`: Maximum number of hours an Airmet can be valid
- `hours_before_validity`: Maximum time between publishing an Airmet and when it can be active
- `cloud_lower_level_min`: Specifies the minimum lower cloud level that can be set. Object with a key of "FT" (feet) or "FL" (flight level) and height specified as an integer `{"FT": 100, "FL": 500}`.
- `cloud_lower_level_max`: Specifies the maximum lower cloud level that can be set. Object with a key of "FT" (feet) or "FL" (flight level) and height specified as an integer `{"FT": 100, "FL": 500}`.
- `cloud_level_min`: Specifies the minimum value for the upper cloud level that can be set.
- `cloud_level_max`: Specifies the maximum value for the upper cloud level that can be set. Object with a key of "FT" (feet) or "FL" (flight level) and height specified as an integer `{"FT": 100, "FL": 500}`.
- `cloud_level_rounding_ft`: Integer that sets how cloud levels should be rounded
- `wind_direction_rounding`: Integer that sets how wind direction should be rounded
- `wind_speed_min`: Minimum wind speed in knots. Specified with subkey `KT`: `{"KT": 199}`
- `wind_speed_max`: Maximum wind speed in knots. Specified with subkey `KT`: `{"KT": 199}`
- `visibility_max`: Integer that sets maximum visibility value in meters
- `visibility_min`: Integer that sets minimum visibility value in meters
- `visibility_rounding_below`: Integer that sets rounding of visibility when visibility is below 750 meters.
- `visiblity_rounding_above`: Integer that sets rounding of visibility when visibility is over 750 meters.
- `level_min`: Object that sets the minimum level per unit as subkeys: `{"FT": 100, "FL", 50}`
- `level_max`: Object that sets the maximum level per unit as subkeys: `{"FT": 100, "FL", 50}`
- `level_rounding_FL`: Integer that sets rounding of levels in flight level units
- `level_rounding_FT`: Integer that sets rounding of levels in feet.
- `movement_min`: Object that sets the minimum moving speed level per unit as subkeys: `{"KT": 150, "KMH": 99}`
- `movement_max`: Object that sets the maximum moving speed level per unit as subkeys: `{"KT": 150, "KMH": 99}`
- `movement_rounding_kt`: Integer that sets the rounding for movement speed in knots
- `max_polygon_points`: Maximum number of polygon points that a polygon drawn by the user can have
- `units`: List of objects that contain allowed units per unit type. Each object contains two keys: `"unit_type"` and `"allowed_units"`. Allowed values for `"unit_type"` are: `"level_unit", "movement_unit", "surfacewind_unit", "cloud_level_unit"`. The value for `"allowed_units"` is a list containing one or more units such as `["FT", "FL", "KT", "KMH"]`.

#### mapPreset
The following options are configurable in the `mapPreset` field of the `AirmetConfig` class.

- `layers`: list of objects with the following keys:
    - `id` (required): identifier for layer
    - `name` (required): name of the layer
    - `type`: type of layer (`"twms"`, `"wms"`, ...)
    - `layerType`: `"baseLayer"` or `"overLayer"`
    - `format`: image format
    - `enabled`: boolean, whether the layer is shown by default
    - `service`: link to WMS server

### Sigmet configuration options
The following options are configurable in [`configuration_files/sigmetConfig.json`](./configuration_files/sigmetConfig.json). Also see the `SigmetConfig` class in [`config.py`](/opmet_backend/config.py).

- `location_indicator_mwo`: MWO location indicator code
- `fir_areas`: An object with one or more FIR configurations. The subkey is the FIR location code. For more details, see [fir_areas](#fir_areas-1)
- `active_firs`: list of FIRs as specified in `fir_areas`
- `mapPreset`: An object specifying which map data is shown in the Sigmet form. For more information, see [mapPreset](#mappreset-1).
- `valid_from_delay_minutes`: Integer that specifies the start time of an Sigmet when opening the form as current time + `valid_from_delay_minutes`.
- `default_validity_minutes`: Integer that specifies how long the Airmet is valid when opening the form as start time + `default_validity_minutes`.
- `omit_change_va_and_radoact_cloud`: Boolean, whether to show the change options of the form for Volcanic Ash and Radio-active cloud Sigmets.

#### fir_areas
The following options are configurable in the `fir_areas` field of the `SigmetConfig` class. Also see the `SigmetFirArea` class in [`config.py`](/opmet_backend/config.py).

[//]: # (Cleanup of "area_preset" will be done in https://gitlab.com/opengeoweb/geoweb-assets/-/issues/3531)

- `fir_areas`: Name of the FIR
- `fir_location`: FeatureCollection specification of FIR boundaries
- `location_indicator_atsr`: ATSR FIR location code
- `location_indicator_atsu`: ATSU FIR location code
- `area_preset`: A deprecated field that is required. Value should be a string (will be cleaned up in the future)
- `max_hours_of_validity`: Maximum number of hours a Sigmet can be valid
- `max_hours_before_validity`: Maximum time between publishing a Sigmet and when it can be active
- `tc_max_hours_of_validity`: Maximum number of hours a tropical cyclone Sigmet can be valid
- `tc_hours_before_validity`: Maximum time between publishing a tropical cyclone Sigmet and when it can be active
- `va_max_hours_of_validity`: Maximum number of hours a volcanic ash Sigmet can be valid
- `va_hours_before_validity`: Maximum time between publishing a volcanic ash Sigmet and when it can be active
- `adjacent_firs`: list of FIRs adjacent to the FIR
- `level_min`: Object that sets the minimum level per unit as subkeys: `{"FT": 100, "FL", 50}`
- `level_max`: Object that sets the maximum level per unit as subkeys: `{"FT": 100, "FL", 50}`
- `level_rounding_FL`: Integer that sets rounding of levels in flight level units
- `level_rounding_FT`: Integer that sets rounding of levels in feet.
- `movement_min`: Object that sets the minimum moving speed level per unit as subkeys: `{"KT": 150, "KMH": 99}`
- `movement_max`: Object that sets the maximum moving speed level per unit as subkeys: `{"KT": 150, "KMH": 99}`
- `movement_rounding_kt`: Integer that sets the rounding for movement speed in knots
- `max_polygon_points`: Maximum number of polygon points that a polygon drawn by the user can have
- `units`: List of objects that contain allowed units per unit type. Each object contains two keys: `"unit_type"` and `"allowed_units"`. Allowed values for `"unit_type"` are: `"level_unit", "movement_unit", "surfacewind_unit", "cloud_level_unit"`. The value for `"allowed_units"` is a list containing one or more units such as `["FT", "FL", "KT", "KMH"]`.

#### mapPreset
The following options are configurable in the `mapPreset` field of the `SigmetConfig` class.

- `layers`: list of objects with the following keys:
    - `id` (required): identifier for layer
    - `name` (required): name of the layer
    - `type`: type of layer (`"twms"`, `"wms"`, ...)
    - `layerType`: `"baseLayer"` or `"overLayer"`
    - `format`: image format
    - `enabled`: boolean, whether the layer is shown by default
    - `service`: link to WMS server


### Sequence Numbering

if `sigmet_sequence_prefix` is set in `backendConfig.json`, the sigmet sequence numbering will follow the same obsolete pattern as airmet numbering, such that if the prefix is configured as `A`, the sequence will be `A1`, `A2`, `A3`, ... regardless of type or phenomenon. This field and the related methods from `SigmetBase` should be removed once no longer needed.

Otherwise, the sequence numbering will be specific to type and phenomenon group such as `I01`, `I02`, ... for phenomenon `SEV_ICE`, which can be configured trough `sigmet_prefixes` and `sigmet_sequence_phenomena` in `backendConfig.json` that together map phenomenon field to phenomenon group and that to correct prefix. The default values can be found in `config.py` and are based on Easy Access Rules for ATM-ANS (Regulation (EU) 2017/373). For example, if Tropical Cyclone sigmets are to be omitted from use, the default values should be replaced with otherwise identical dictionaries, but without the keys related to TC.

The improved feature has so far not been implemented for airmets, but should be trivial to do following the sigmet functionality, once relevant mappings are available.

## Publish modules

The backend comes with sample publishers as templates to help end users develop their own. The backend needs a URL to send messages to in the form of PUBLISHER_URL/FILENAME with file contents for FILENAME as the "data" field in a JSON payload. The backend expects a 200 HTTPStatus in the response, but anything else is up to whatever the provider of the publisher service needs it to do.

### Local publisher

The included publisher_local comes with an extremely simple FastAPI endpoint that stores the incoming message as a text file into local filesystem of that container, or whatever volume the user has mounted into the DESTINATION path within the container.

### S3 publisher

The opmet backend is compatible with an [s3 publisher](https://gitlab.com/opengeoweb/backend-services/publisher-backend/container_registry/5997126) that can be found in the shared [publisher backend repository](https://gitlab.com/opengeoweb/backend-services/publisher-backend). The s3 publisher comes with an extremely simple FastAPI endpoint that stores the incoming message as a text file into an Amazon S3 bucket defined by S3_BUCKET_NAME environment variable. Note that the necessary AWS credentials need to be provided to the container by whatever method is approppriate in the used deployment environment for it to work.

### SFTP publisher

Another publisher the opmet backend is compatible with is the [SFTP publisher](https://gitlab.com/opengeoweb/backend-services/publisher-backend/container_registry/5997926) that can also be found in the shared [publisher backend repository](https://gitlab.com/opengeoweb/backend-services/publisher-backend). The SFTP publisher comes with a simple FastAPI endpoint that sends the incoming messages as files to remote hosts via SFTP. The SFTP publisher can be configured with multiple remote hosts to send the files to. It is configured using `SERVERS` environment variable that contains a JSON-encoded string. A minimum configuration JSON string looks like this:
```
[{"name": "local-server", "username": "opmet", "password": "pass", "hostname": "sftp-server", "remote_dir": "/output"}]
```
You can add multiple servers to the array. The files will be sent to all of them. See the `Settings` class in `publisher_sftp/main.py` for the full configuration schema.

## Endpoints

### General endpoints defined in `main.py`

* `/`that returns a welcome message in plaintext, similar to other GeoWeb backends.
* `/version`that returns the currently deployed version of the backend.

### Healthchecks
For each container service in this backend, a healthcheck can be requested. This can be done by making a request to `{service URL}/healthcheck`. Only for the NGINX container an alternative endpoint exists: `/health_check`.


If the service is healthy, you can expect a response with status code 200 and `{'status': 'OK', 'service': '{SERVICE NAME}'}`.

If you start the backend and services according to [this section](#running-and-connecting-containers-without-docker-compose-and-without-network), you should be able to access the services with:
- For the opmet-backend container: `https://localhost:4443/healthcheck`. Response: `{"status":"OK","service":"Opmet"}`
- For the NGINX container: `https://localhost:4443/health_check`. Response: `{"status":"OK", "service":"NGINX"}`
- For the publisher: `http://0.0.0.0:8090/healthcheck`. Response: `{"status":"OK","service":"Local publisher"}` or `{"status":"OK","service":"SFTP publisher"}`
- For the avi-messageconverter: `http://localhost:8081/`. Reponse: `Greetings from Spring SigmetController!!!...` (will be updated)

### SIGMET and AIRMET specific endpoints defined in `routers/sigmet.py`and `routers/airmet.py`

* GET `/sigmetlist`and `/airmetlist` to get the list of corresponding objects. By default they call for 15 latest published items within the default time limit configured in `/configuration_files/backendConfig.json`. They can be overridden with query parameters `published_limit` and `time_limit`.
* POST `/sigmet` and `/airmet` to store a corresponding message into database. The system uses `changeStatusTo`field in the JSON payload and validation of the accompanyin SIGMET or AIRMET message to determine approppriate actions to take. If validation fails, error messages may be lengthy as the system tries to match the input into all known patterns.
* GET `/sigmet/{uuid}` and `/airmet/{uuid}` to fetch individual messages from database by their UUID. This is currently not used by the frontend, but is available should it become needed and is used for testing.
* GET `/sigmet2tac` and `/airmet2tac`to forward corresponding messages to the messageconverter container and return the response in plain text format.
* GET `/sigmet-config`and `/airmet-config` that return configuration set in `configuration_files/sigmetConfig.json` and `configuration_files/airmetConfig.json` for use in the frontend.

There is a lot of redundancy between SIGMET and AIRMET code both under `routers/` and under `models/`, which has been retained in case the needs for the two might deviate. Developers may consider using more shared code instead if their needs are expected to stay the same.

## OpenAPI schema and API reference

Generated [OpenAPI](https://github.com/OAI/OpenAPI-Specification) documentation can be browsed at `/docs`.
See http://localhost:8000/docs when running the backend locally or browse the production docs at https://cap.opengeoweb.com/docs.

The OpenAPI schema is available at http://localhost:8000/openapi.json.

Alternative ReDoc API documentation can be viewed at http://localhost:8080/redoc.

## Coding standards

### Formatting

This project follows the [Google Python Style Guide](https://github.com/google/styleguide/blob/gh-pages/pyguide.md). [YAPF](https://github.com/google/yapf) is used used to enforce this. Install all dependencies using `pip install -r requirements.txt -r requirements-dev.txt` and run YAPF by executing
```
yapf -ir opmet_backend/
```
This will format all Python files in [opmet_baclend](opmet_backend), using the configuration specified in [.style.yapf](.style.yapf).

### Organizing imports

Imports are organized using isort. Run
```
isort opmet_backend/
```

### Installing tools globally
Many python tools, including yapf and isort, can be installed globally using package managers such as apt. Run
```
sudo apt install yapf isort
```
The tools can be run with the same commands as above.

### IDE support

If you are using Visual Studio Code, consider updating [.vscode/settings.json](.vscode/settings.json) with the contents below. This will format code and organize imports on save.

```
{
    "editor.codeActionsOnSave": {
        "source.organizeImports": true
    },
    "python.formatting.provider": "yapf",
    "[python]": {
        "editor.formatOnSave": true
    }
}
```

### Running tests

Tests are implemented with [pytest](https://pytest.org). They require the services described in `docker-compose-tests.yml` to be up and running. Simply use `tests.sh` and pytest will start said services and stop them upon completion.

## Deployment

When MR gets merged, the GitLab CI/CD pipeline builds a container image based on the [Dockerfile](./Dockerfile)

- Docker image is published to the GitLab Container Registry (https://gitlab.com/opengeoweb/backend-services/cap-backend/container_registry)
- The docker image is tagged with a tag `"registry.gitlab.com/opengeoweb/backend-services/opmet-backend:main"`
- Note: the main branch based version of the Opmet backend will be deployed to the development environment later when it is available. See https://gitlab.com/opengeoweb/opengeoweb/-/issues/2417

When a new tag is created from the main branch (use semantic versioning for example v0.0.1)

- Docker image is published to GitLab Container Registry (https://gitlab.com/opengeoweb/backend-services/cap-backend/container_registry)
- The docker image is tagged with a tag `"registry.gitlab.com/opengeoweb/backend-services/opmet-backend:v0.0.1"`
- A new revision of the Elastic Container Service (ECS) task definition is created and uploaded to the ECS
- The new tagged version of the Opmet backend gets deployed to the ECS production environment (https://opmet.opengeoweb.com/)

## Database migrations

[Alembic](https://alembic.sqlalchemy.org/en/latest/) is used for database migrations. All files related to database migrations can be found in the [migrations](migrations) directory. There are multiple ways to use alembic for database migrations:

1. Alembic commands can be run using Docker:
```
docker compose -f docker-compose-alembic.yml run dev [...]
```
for example
```
docker compose -f docker-compose-alembic.yml run dev alembic upgrade head
```

2. If you want to run the application with [uvicorn](#start-the-python-fastapi-application), first set up the datase, make sure your [env](.env) file is is created and correct and then run
```
./admin.sh
# or
bash admin.sh
```

3. The final option is attaching the [`docker-compose-admin.yml`](docker-compose-admin.yml) file to a docker compose command. You can do this by adding `-f docker-compose-admin.yml` to a command (Always make sure your [env](.env) file is correct!):
```
# For "production-like" deployment
docker compose -f docker-compose.yml -f docker-compose-admin.yml up -d --build

# For dev deployment
docker compose -f docker-compose-dev.yml -f docker-compose-admin.yml up -d --build
```

For tests you need to edit the docker compose command in [tests.sh](tests.sh) so that it equals:
```
docker compose -f docker-compose-tests.yml -f docker-compose-admin.yml up --abort-on-container-exit --exit-code-from tests
```


The steps to create a new database migration are described below.

### Steps to create a new database migration
1. Make changes to your database models.
2. Run ```alembic revision --autogenerate -m "Revision description"``` to generate a new revision file.
3. Open the generated revision file.
    * Check that the variable `down_revision` is correctly set, it should be identical to the previous revision ID
4. Complete the function `upgrade()`. Here you describe the database changes with respect to the previous revision. When the autogenerate option is used, some of the changes will be generated by Alembic. The changes may not be complete and need to be manually verified.
5. You can complete the `downgrade()` function as well to revert the changes made by the `upgrade()` function if down-revision compatibility is desired.
6. Done! The backend runs  ```alembic upgrade head``` programmatically on startup, ensuring that the latest database version is used. Additionally, you can play around with ```alembic upgrade/downgrade``` to verify that the correct changes are made during the migration.


### Some more background information

Alembic can be used for database migrations and can also help you to easily switch between different revisions. Some useful commands:
  * ```alembic current``` show the revision currently in use. If the current revision is equal to the latest revision, `(head)` is printed after the revision ID.
  * ```alembic history (--verbose)``` shows the migration history. The flag ```--verbose``` can be used to display extra information.
  * ```alembic upgrade head``` will migrate to the latest created database revision.
  * ```alembic upgrade [revision ID]``` will migrate to the specified revision ID.
  * ```alembic upgrade +n``` will upgrade to `n` new versions from the current version.
  * ```alembic downgrade base``` will downgrade to the beginning first database revision.
  * ```alembic downgrade -n``` will downgrade to `n` versions from the current.
  * ```alembic revision -m "Revision description"``` This will generate a new revision file in [migrations/versions](migrations/versions/).

In the directory [migrations/versions](migrations/versions/), revision files can be found that describe the database migration:
  * Each migration has a unique Revision ID that is identified with the variable `revision`. The previous revision is identified by the variable `down_revision`. With these variables, all revision files can be linked in order to easily switch between migrations.
  * Running ```alembic revision -m "Revision description"``` generates a new revision file. This revision is the most recent one, so it can be identified as `head`.
  * In each revision file, two functions can be found: `upgrade` and `downgrade`. The `upgrade` function describes how the database changes with respect to the previous revision. The `downgrade` function reverts the changes made with `upgrade` to restore the database to the previous revision.
  * During startup of the backend, the backend runs  ```alembic upgrade head``` programmatically, ensuring that the latest database revision is used.

  For more information, see the [Alembic documentation](https://alembic.sqlalchemy.org/en/latest/).

## Running and connecting containers without docker compose

### 1 Start database server

```
PG_PORT=5432 #Make sure this doesn't conflict with other Postgres instances
docker run --rm \
    --name opmet-backend-db \
    -d \
    -e POSTGRES_USER=geoweb \
    -e POSTGRES_PASSWORD=geoweb \
    -e POSTGRES_DB=opmetbackenddb \
    -p $PG_PORT:5432 \
    postgres:13.4
```

### 2 Start opmet backend & services

First set up the publisher service:
```
docker build -t publisher publisher_local/
docker run -d -it --network host \
    publisher
```

And the avi-messageconverter:
```
docker run -d -it --network host \
    registry.gitlab.com/opengeoweb/avi-msgconverter/geoweb-knmi-avi-messageservices:v0.11.0
```

Finally build the backend:
```
docker build -t opmetbe .

docker run -d -it --network  host \
    -e SQLALCHEMY_DATABASE_URL=postgresql://geoweb:geoweb@localhost:$PG_PORT/opmetbackenddb \
    -e PUBLISHER_URL=http://publisher:8090/publish\
    -e MESSAGECONVERTER_URL=http://messageservices:8080 \
    opmetbe
```

Check if visiting http://localhost:8000/ gives the message "GeoWeb OPMET backend"

### 3 Start nginx reverse proxy

```bash
docker run -d -it --network host \
    -e "ENABLE_SSL=FALSE" \
    -e "ENABLE_DEBUG_LOG=TRUE" \
    -e "ENABLE_ACCESS_LOG=FALSE" \
    -e "NGINX_PORT_HTTP=9001" \
    -e "BACKEND_HOST=0.0.0.0:8000" \
    -e OAUTH2_USERINFO="https://gitlab.com/oauth/userinfo" \
    -v nginxcertvol:/cert \
    -v "$(pwd)"/nginx/nginx.conf:/nginx.conf \
    registry.gitlab.com/opengeoweb/backend-services/auth-backend/auth-backend:v0.2.0
```

Check if visiting http://localhost:9001/healthcheck provides a 200 with a message.

Check if visiting http://localhost:9001/ gives the message "GeoWeb OPMET backend"


## Running and connecting containers without docker compose and without network

### 1 - Database service
```
PG_PORT=5432 #Make sure this doesn't conflict with other Postgres instances
docker run --rm \
    --name opmet-backend-db \
    -d \
    -e POSTGRES_USER=geoweb \
    -e POSTGRES_PASSWORD=geoweb \
    -e POSTGRES_DB=opmetbackenddb \
    -p $PG_PORT:5432 \
    postgres:13.4
```

### 2 - Publisher service
```
docker build -t publisher publisher_local/
docker run -d -it -p 8090:8090 \
    --name publisher publisher
export PUBLISHER_URL="http://0.0.0.0:8090/publish"
```

Check if visiting http://0.0.0.0:8090/healthcheck gives the message `{"status":"OK","service":"Local publisher"}`

Note that you can build the SFTP service locally in a similar way (the s3_publisher works only when deployed on AWS). With the instructions below, retrieve the image from the [publisher repository](https://gitlab.com/opengeoweb/backend-services/publisher-backend) and build the service locally. The output can be found in the [sftp_output](/sftp_output/) directory.

```
docker run -d -it -p 8090:8090 \
    -e username="geoweb" \
    -e remove_dir="/sftp_output" \
    --name publisher_sftp \
    registry.gitlab.com/opengeoweb/backend-services/publisher-backend/publisher-sftp:v0.0.2
export PUBLISHER_URL="http://0.0.0.0:8091/publish"
```

Check that visiting http://0.0.0.0:8090/healthcheck gives the message `{"status":"OK","service":"SFTP publisher"}`

### 3 - Avi-message service
```
docker run -d -p 8081:8080 --name geoweb-knmi-avi-messageservices-local \
    registry.gitlab.com/opengeoweb/avi-msgconverter/geoweb-knmi-avi-messageservices:v0.11.0
export MESSAGECONVERTER_URL="http://localhost:8081"
```

### 4 - Opmet backend
```
docker build -t opmetbe .

docker run -d -it --network="host" \
    -e SQLALCHEMY_DATABASE_URL=postgresql://geoweb:geoweb@localhost:$PG_PORT/opmetbackenddb \
    -e PUBLISHER_URL=${PUBLISHER_URL} \
    -e MESSAGECONVERTER_URL=${MESSAGECONVERTER_URL} \
    opmetbe
```

Check if visiting http://localhost:8000/ gives the message "GeoWeb OPMET backend".

Check if visiting http://0.0.0.0:8000/healthcheck gives the message `{"status":"OK","service":"Opmet"}`

### 3 Start nginx reverse proxy

Start the reverse proxy container with the following command

```bash
docker run -d -it --network host \
    -e "ENABLE_SSL=FALSE" \
    -e "ENABLE_DEBUG_LOG=TRUE" \
    -e "ENABLE_ACCESS_LOG=FALSE" \
    -e "NGINX_PORT_HTTP=9001" \
    -e "BACKEND_HOST=0.0.0.0:8000" \
    -e OAUTH2_USERINFO="https://gitlab.com/oauth/userinfo" \
    -v nginxcertvol:/cert \
    -v "$(pwd)"/nginx/nginx.conf:/nginx.conf \
    registry.gitlab.com/opengeoweb/backend-services/auth-backend/auth-backend:v0.2.0
```

Check if visiting http://localhost:9001/healthcheck provides the message `{"status":"OK","service":"Opmet"}`.

Check if visiting http://localhost:9001/health_check provides the message `{"status":"OK", "service":"NGINX"}`.

Check if visiting http://localhost:9001/ gives the message "GeoWeb OPMET backend"
