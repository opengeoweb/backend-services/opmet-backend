"""File saver microservice for storing files into local filesystem"""

import logging
import os

from fastapi import Body, FastAPI, Request
from pydantic_settings import BaseSettings

app = FastAPI()

# Configure logging
logging.basicConfig(level=logging.DEBUG)


class Settings(BaseSettings):
    """Settings, read from ENV"""

    DESTINATION: str = os.environ.get("DESTINATION", "/app/output")


settings = Settings()


# Middleware to log incoming requests
@app.middleware("http")
async def log_requests(request: Request, call_next):
    body = await request.body()
    logging.debug(
        f"Incoming request: {request.method} {request.url} - Body: {body.decode('utf-8')}"
    )
    response = await call_next(request)
    return response


@app.post("/publish/{filename}")
async def save_file(
    filename: str,
    data: str = Body(min_length=4, embed=True),
):
    """Endpoint to take filename and string and save them into volume in the
    container"""

    with open(f"{settings.DESTINATION}/{filename}", "w", encoding="utf-8") as file:
        file.write(data)

    return {"message": f"File '{filename}' saved successfully to volume."}


@app.get("/healthcheck")
async def healthcheck():
    """Health check that can be called to see if service is accessible."""
    return {"status": "OK", "service": "Local publisher"}
