# Main two-stage dockerfile for opmet-backend
FROM python:3.11-slim-bookworm AS build

ARG VERSION=dev
ARG POETRY_DEV_GROUP=--without

RUN apt update && \
    apt install -y gcc libpq-dev && \
    rm -rf /var/lib/apt/lists/*

RUN pip install poetry==1.8.3


ENV POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_IN_PROJECT=1 \
    POETRY_CACHE_DIR=/tmp/poetry_cache \
    POETRY_VIRTUALENVS_CREATE=1 \
    APP_VERSION=$VERSION \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1

COPY pyproject.toml poetry.lock ./

RUN poetry install --no-root $POETRY_DEV_GROUP dev

ENV PATH="/.venv/bin:$PATH"

EXPOSE 8000

WORKDIR /app
COPY ./opmet_backend /app/opmet_backend
COPY ./configuration_files /app/configuration_files
COPY ./migrations /app/migrations
COPY ./alembic.ini /app/
COPY ./opmet_logging.conf /app/
COPY ./admin.sh /app/
COPY ./cli.py /app/

# Creates a non-root user with an explicit UID and adds permission to access the /app folder
# For more info, please refer to https://aka.ms/vscode-docker-python-configure-containers
RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /app
USER appuser

# Number of workers can be adjusted here if desired
CMD gunicorn --bind 0.0.0.0:${OPMET_PORT_HTTP:-8000} -k uvicorn.workers.UvicornWorker --log-config opmet_logging.conf opmet_backend.main:app
# CMD uvicorn --host 0.0.0.0 --port ${OPMET_PORT_HTTP:-8000} opmet_backend.main:app
