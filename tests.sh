#!/bin/bash
poetry install --with dev --no-root
poetry run pytest --asyncio-mode=auto
